package social.petsgram.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jsibbold.zoomage.ZoomageView;

import social.petsgram.R;

public class PreviewImageDialog extends DialogFragment implements View.OnClickListener {
    private ImageView mBtnBack;
    private ZoomageView mImgPreview;

    public static PreviewImageDialog newInstance(Bundle args){
        PreviewImageDialog previewImageDialog = new PreviewImageDialog();
        previewImageDialog.setArguments(args);
        return previewImageDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_preview_image,container,false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mBtnBack = rootView.findViewById(R.id.imgBack);
        mImgPreview = rootView.findViewById(R.id.imgPreview);
        mBtnBack.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null){
            String urlImage = getArguments().getString("URL_IMAGE","");
            Glide.with(getContext())
                    .load(urlImage)
                    .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_sticker)
                    )
                    .into(mImgPreview);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                dismiss();
                break;
        }
    }
}
