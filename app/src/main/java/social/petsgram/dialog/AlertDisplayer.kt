package social.petsgram.dialog

import android.app.AlertDialog
import android.content.Context

interface IAlertDisplayer{
    fun onOkClicked()
}
class AlertDisplayer {
    fun show(title: String, message: String, context: Context, onClick: IAlertDisplayer?) {
        val builder = AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.cancel()
                    onClick?.onOkClicked()
                }
        val ok = builder.create()
        ok.show()
    }
}