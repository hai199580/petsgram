package social.petsgram.view

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ImageView

class KeepTouchButton : ImageView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private lateinit var mCallback : IKeepTouch

    fun setListener(callback : IKeepTouch){
        mCallback = callback
    }

    private val KEEP_TOUCH_TIME = 500L
    private var mTimeDown = 0L

    private val _handler = Handler()
    private var _keepTouch: Runnable = Runnable {
        run {
            mCallback.onKeepTouchStart()
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mTimeDown = System.currentTimeMillis()
                _handler.postDelayed(_keepTouch, KEEP_TOUCH_TIME)
            }

            MotionEvent.ACTION_UP -> {
                if (System.currentTimeMillis() - mTimeDown < KEEP_TOUCH_TIME){
                    _handler.removeCallbacks(_keepTouch)
                    mCallback.onSingleClick()
                } else {
                    mCallback.onKeepTouchEnd()
                }
            }
        }
        return true
    }
}

interface IKeepTouch{
    fun onSingleClick()

    fun onKeepTouchStart()

    fun onKeepTouchEnd()
}