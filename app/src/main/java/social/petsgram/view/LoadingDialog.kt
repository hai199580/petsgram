package social.petsgram.view

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import kotlinx.android.synthetic.main.layout_alert_dialog.*
import social.petsgram.R

interface OnCancelClickListener {
    fun onClick()
}

class LoadingDialog : Dialog {
    private lateinit var activity: Activity
    private lateinit var mListener : OnCancelClickListener

    constructor(context: Context) : super(context) {
        activity = context as Activity
    }

    init {
        setContentView(R.layout.layout_alert_dialog)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        setCancelable(false)

        btnCancelDialog.setOnClickListener{
            if (mListener != null){
                mListener.onClick()
            }
        }
    }

    fun hideCancelButton(){
        btnCancelDialog.visibility = View.GONE
    }

    fun setOnCancelClickListener(callback : OnCancelClickListener){
        mListener = callback
    }

    fun setText(text: String) {
        tv_noti.text = text
    }

    fun setText(text: Int) {
        tv_noti.text = activity.resources.getText(text)
    }

    override fun show() {
        if (activity != null && activity.isDestroyed)
            if (isShowing) {
                dismiss()
            }
        super.show()
    }
}