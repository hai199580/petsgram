package social.petsgram.ui

import android.Manifest
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.View
import kotlinx.android.synthetic.main.activity_browser.*
import kotlinx.android.synthetic.main.fragment_library.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import social.petsgram.MeowApplication
import social.petsgram.R
import social.petsgram.adapter.MyPagerAdapter
import social.petsgram.base.BaseActivity
import social.petsgram.model.event.RefreshEvent
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.ui.fragment.CameraFragment
import social.petsgram.ui.fragment.LibraryFragment
import social.petsgram.ui.fragment.VideoFragment
import social.petsgram.utils.*
import social.petsgram.utils.permission.PermissionResultCallback
import social.petsgram.utils.permission.PermissionUtils
import social.petsgram.view.LoadingDialog

class ResourceBrowserActivity : BaseActivity(), View.OnClickListener {
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var mFFmpeg: FFmpegManager
    private lateinit var fragments: ArrayList<Fragment>
    private lateinit var mPermissionUtils: PermissionUtils
    private val permissions : ArrayList<String> by lazy {
        ArrayList<String>()
    }

    override fun getFragmentViewId(): Int = -1

    override fun getLayoutId(): Int = R.layout.activity_browser

    private lateinit var libraryFragment : LibraryFragment
    private lateinit var cameraFragment : CameraFragment
    private lateinit var videoFragment : VideoFragment
    override fun initView() {
        mPermissionUtils = PermissionUtils(this)
        mPermissionUtils.setPermissionCallback(permissionCallback)
        permissions.add(Manifest.permission.CAMERA)
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)

        loadingDialog = LoadingDialog(this)
        loadingDialog.hideCancelButton()
        loadingDialog.setText(R.string.resizing)

        mPermissionUtils.check_permission(permissions,1)
    }

    private fun setupViewPager(){
        fragments = ArrayList()
        libraryFragment = LibraryFragment()
        cameraFragment = CameraFragment()
        videoFragment = VideoFragment()
        fragments.add(libraryFragment)
        fragments.add(cameraFragment)
        fragments.add(videoFragment)

        val pagerAdapter = MyPagerAdapter(supportFragmentManager,fragments)
        pagerAdapter.setPagerId(R.id.vp_browser)
        vp_browser.adapter = pagerAdapter
        vp_browser.offscreenPageLimit = 3
        switchPager(0)

        vp_browser.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                switchPager(position)
            }
        })
    }

    private val ffmpegListener = object : FFmpegManagerListener {
        override fun onStart(tag: String) {

        }

        override fun onProgress(tag: String, message: String) {

        }

        override fun onSuccess(tag: String, message: String) {

        }

        override fun onFailure(tag: String, message: String) {

        }

        override fun onFinished(tag: String) {
            if (tag == mFFmpeg.TAG_RESIZE_VIDEO){
                loadingDialog.dismiss()
                val video = LocalResourceModel(data = FileUtils.getPathVideoResize(), duration = videoFragment.getImageData().duration)
                val intent = Intent(applicationContext, EditVideoActivity::class.java)
                intent.putExtra(Define.KeyItent.KEY_VIDEO, video)
                startActivity(intent)
            }
        }
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btn_library.setOnClickListener(this)
        btn_camera.setOnClickListener(this)
        btn_video.setOnClickListener(this)
        btn_back.setOnClickListener(this)
        rl_next.setOnClickListener(this)
    }

    override fun initData() {
    }

    override fun onClick(view: View) {
        when (view) {
            btn_library -> {
                switchPager(0)
            }

            btn_camera -> {
                switchPager(1)
            }

            btn_video -> {
                switchPager(2)
            }

            btn_back -> {
                finish()
            }

            rl_next -> {
                when (vp_browser.currentItem) {
                    0 -> {
                        val intent = Intent(this,EditPhotoActivity::class.java)
                        intent.putExtra(Define.KeyItent.KEY_IMAGE,libraryFragment.getImageData())
                        startActivity(intent)
                    }

                    1 -> {

                    }

                    2 -> {
                        if (videoFragment.getImageData().duration > 10000){
                            val intent = Intent(this,CutVideoActivity::class.java)
                            intent.putExtra(Define.KeyItent.KEY_VIDEO,videoFragment.getImageData())
                            startActivity(intent)
                        } else {
                            mFFmpeg = (applicationContext as MeowApplication).getFFmpegManager()
                            mFFmpeg.setFFmpegListener(ffmpegListener)
                            loadingDialog.show()
                            mFFmpeg.resizeVideoCommand(videoFragment.getImageData().data)
                        }
                    }
                }
            }
        }
    }

    private fun switchPager(page : Int){
        vp_browser.setCurrentItem(page,true)
        when(page){
            0 -> {
                rl_next.visibility = View.VISIBLE
                tv_title.text = resources.getString(R.string.library)
                btn_library.setTextColor(resources.getColor(R.color.gray_dark))
                btn_camera.setTextColor(resources.getColor(R.color.gray))
                btn_video.setTextColor(resources.getColor(R.color.gray))
            }

            1 -> {
                rl_next.visibility = View.GONE
                tv_title.text = resources.getString(R.string.camera)
                btn_library.setTextColor(resources.getColor(R.color.gray))
                btn_camera.setTextColor(resources.getColor(R.color.gray_dark))
                btn_video.setTextColor(resources.getColor(R.color.gray))
            }

            2 -> {
                rl_next.visibility = View.VISIBLE
                tv_title.text = resources.getString(R.string.video)
                btn_library.setTextColor(resources.getColor(R.color.gray))
                btn_camera.setTextColor(resources.getColor(R.color.gray))
                btn_video.setTextColor(resources.getColor(R.color.gray_dark))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: RefreshEvent) {
        libraryFragment.swipeToLoadLayout.isRefreshing = true
        libraryFragment.onRefresh()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        mPermissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }

    private val permissionCallback = object : PermissionResultCallback {
        override fun PermissionGranted(request_code: Int) {
            setupViewPager()
            copyFileOrNot()
        }

        override fun PermissionDenied(request_code: Int) {
            finish()
        }

        override fun NeverAskAgain(request_code: Int) {

        }

    }

    private fun copyFileOrNot() {
        if (!SettingManager.getInstance().getIsCopyEmoji()){
            val copyFileTask = CopyFileTask(AssetType.EMOJI)
            copyFileTask.start()
        }

        if (!SettingManager.getInstance().getIsCopyFrame()){
            val copyFileTask = CopyFileTask(AssetType.FRAME)
            copyFileTask.start()
        }

        if (!SettingManager.getInstance().getIsCopyMusic()){
            val copyFileTask = CopyFileTask(AssetType.MUSIC)
            copyFileTask.start()
        }

        if (!SettingManager.getInstance().getIsCopySticker()){
            val copyFileTask = CopyFileTask(AssetType.STICKER)
            copyFileTask.start()
        }
    }
}