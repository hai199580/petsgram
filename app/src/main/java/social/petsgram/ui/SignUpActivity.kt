package social.petsgram.ui

import android.app.Activity
import android.content.Intent
import android.view.View
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_signup.*
import social.petsgram.R
import social.petsgram.base.BaseActivity
import social.petsgram.dialog.AlertDisplayer
import social.petsgram.dialog.IAlertDisplayer
import social.petsgram.view.LoadingDialog

class SignUpActivity : BaseActivity(), View.OnClickListener {
    private lateinit var loadingDialog: LoadingDialog

    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int = R.layout.activity_signup

    override fun initView() {
        loadingDialog = LoadingDialog(this)
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btn_signUp2.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
    }

    override fun initData() {

    }

    private lateinit var alertDisplayer : AlertDisplayer
    private fun signUpAccount(){
        ParseUser.logOut()
        alertDisplayer = AlertDisplayer()
        val parseUser = ParseUser()
        parseUser.username = edt_userName.text.toString()
        parseUser.setPassword(edt_password.text.toString())
        parseUser.email = edt_email.text.toString()
        loadingDialog.show()
        parseUser.signUpInBackground { e ->
            loadingDialog.dismiss()
            if (e == null){
                alertDisplayer.show(
                        resources.getString(R.string.register_succesfuly),
                        resources.getString(R.string.please_verify),
                        this,
                        alertDisplayerListener
                )

            } else {
                alertDisplayer.show(
                        resources.getString(R.string.register_failed),
                        resources.getString(R.string.register_failed_message) + e.message,
                        this,
                        null
                )
            }
        }
    }

    private val alertDisplayerListener = object : IAlertDisplayer{
        override fun onOkClicked() {
            onSignUpSuccessful()
        }
    }

    private fun onSignUpSuccessful(){
        val intent = Intent()
        intent.putExtra("username",edt_userName.text.toString())
        intent.putExtra("password",edt_password.text.toString())
        setResult(Activity.RESULT_OK,intent)
        finish()
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.btn_signUp2 -> {
                signUpAccount()
            }
            R.id.btn_cancel -> {
                finish()
            }
        }
    }
}