package social.petsgram.ui

import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.text.TextUtils
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import kotlinx.android.synthetic.main.activity_cut_video.*
import kotlinx.android.synthetic.main.layout_control_video.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.MeowApplication
import social.petsgram.R
import social.petsgram.base.BaseActivity
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.utils.Define
import social.petsgram.utils.FFmpegManager
import social.petsgram.utils.FFmpegManagerListener
import social.petsgram.utils.FileUtils
import social.petsgram.view.LoadingDialog
import social.petsgram.view.OnCancelClickListener
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class CutVideoActivity : BaseActivity() {
    private lateinit var mFFmpeg: FFmpegManager
    private val mediaDataSourceFactory: DataSource.Factory by lazy {
        buildDataSourceFactory(false)
    }

    private val mainHandler: Handler by lazy {
        Handler()
    }
    private val player: SimpleExoPlayer by lazy {
        ExoPlayerFactory.newSimpleInstance(baseContext)
    }
    private lateinit var loadingDialog: LoadingDialog
    private val MAX_LENGTH = 10000F
    private val MIN_LENGTH = 5000F

    override fun getFragmentViewId(): Int {
        return 0
    }

    override fun getLayoutId(): Int = R.layout.activity_cut_video

    private lateinit var mVideo: LocalResourceModel
    private var isSBLeftTouched = false
    override fun initView() {
        loadingDialog = LoadingDialog(this)
        loadingDialog.setOnCancelClickListener(loadingListener)
        mFFmpeg = (applicationContext as MeowApplication).getFFmpegManager()
        mFFmpeg.setFFmpegListener(ffmpegListener)

        playerView.player = player
        playerView.useController = false
        playerView.requestFocus()

        player.playWhenReady = true

        mVideo = intent.getParcelableExtra(Define.KeyItent.KEY_VIDEO)
        //        Video Source
        val uri = Uri.fromFile(File(mVideo.data))
        //        Default Media Source
        val mediaSource = buildMediaSource(uri, "mp4")

        settingSeekbar()
        player.prepare(mediaSource)

        rl_next.setOnClickListener {
            loadingDialog.setText(R.string.cutting)
            loadingDialog.show()
            mFFmpeg.cutVideoCommand(mVideo.data, FileUtils.getPathVideoCut(),
                    convertTimeCutFormat(mLeftValue.toLong()), convertTimeCutFormat((mRightValue - mLeftValue).toLong()))
        }
    }

    override fun initPresenter() {
    }

    override fun initListener() {
    }

    override fun initData() {
    }

    private val loadingListener = object : OnCancelClickListener {
        override fun onClick() {
            mFFmpeg.killRunningProcess()
            loadingDialog.dismiss()
        }
    }

    private val ffmpegListener = object : FFmpegManagerListener {
        override fun onStart(tag: String) {

        }

        override fun onProgress(tag: String, message: String) {

        }

        override fun onSuccess(tag: String, message: String) {

        }

        override fun onFailure(tag: String, message: String) {

        }

        override fun onFinished(tag: String) {
            if (tag.equals(mFFmpeg.TAG_CUT_VIDEO)) {
                loadingDialog.setText(R.string.resizing)
                mFFmpeg.resizeVideoCommand(FileUtils.getPathVideoCut())
            } else if (tag.equals(mFFmpeg.TAG_RESIZE_VIDEO)) {
                loadingDialog.dismiss()
                val intent = Intent(baseContext, EditVideoActivity::class.java)
                val videoCut = LocalResourceModel(data = FileUtils.getPathVideoResize(),
                        duration = (mRightValue - mLeftValue).toLong())
                intent.putExtra(Define.KeyItent.KEY_VIDEO, videoCut)
                startActivity(intent)
            }
        }
    }

    private var mLeftValue: Float = 0F
    private var mRightValue: Float = MAX_LENGTH
    private fun settingSeekbar() {
        sb_exoProgress.setRange(0F, mVideo.duration.toFloat(), 1000F)
        sb_exoProgress.setValue(0F, MAX_LENGTH)
        sb_exoProgress.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
                isSBLeftTouched = isLeft
            }

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                if (rightValue - leftValue > MAX_LENGTH) {
                    if (isSBLeftTouched) {
                        val right = leftValue + MAX_LENGTH
                        sb_exoProgress.setValue(leftValue, right)
                        mLeftValue = leftValue
                        mRightValue = right
                        sb_exoProgress.setIndicatorText(convertTimeFormat(leftValue.toLong()))
                    } else {
                        val left = rightValue - MAX_LENGTH
                        sb_exoProgress.setValue(left, rightValue)
                        mLeftValue = left
                        mRightValue = rightValue
                        sb_exoProgress.setIndicatorText(convertTimeFormat(rightValue.toLong()))
                    }
                } else if (rightValue - leftValue < MIN_LENGTH) {
                    if (isSBLeftTouched) {
                        val left = rightValue - MIN_LENGTH
                        sb_exoProgress.setValue(left, rightValue)
                        mLeftValue = left
                        mRightValue = rightValue
                        sb_exoProgress.setIndicatorText(convertTimeFormat(leftValue.toLong()))
                    } else {
                        val right = leftValue + MIN_LENGTH
                        sb_exoProgress.setValue(leftValue, right)
                        mLeftValue = leftValue
                        mRightValue = right
                        sb_exoProgress.setIndicatorText(convertTimeFormat(rightValue.toLong()))
                    }
                } else {
                    if (isSBLeftTouched) {
                        sb_exoProgress.setIndicatorText(convertTimeFormat(leftValue.toLong()))
                    } else {
                        sb_exoProgress.setIndicatorText(convertTimeFormat(rightValue.toLong()))
                    }

                    mLeftValue = leftValue
                    mRightValue = rightValue
                }
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }
        })
    }

    private fun convertTimeFormat(millisecond: Long): String {
        val format = SimpleDateFormat("m:ss", Locale.US)
        return format.format(Date(millisecond))
    }

    private fun convertTimeCutFormat(millisecond: Long): String {
        val format = SimpleDateFormat("00:mm:ss", Locale.US)
        return format.format(Date(millisecond))
    }

    private fun buildMediaSource(uri: Uri, overrideExtension: String): MediaSource {
        val type = if (TextUtils.isEmpty(overrideExtension))
            Util.inferContentType(uri)
        else
            Util.inferContentType(".$overrideExtension")
        when (type) {
            C.TYPE_SS -> return SsMediaSource(uri, buildDataSourceFactory(false),
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_DASH -> return DashMediaSource(uri, buildDataSourceFactory(false),
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_HLS -> return HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null)
            C.TYPE_OTHER -> return ExtractorMediaSource(uri, mediaDataSourceFactory, DefaultExtractorsFactory(),
                    mainHandler, null)
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }

    private val BANDWIDTH_METER = DefaultBandwidthMeter()
    private fun buildDataSourceFactory(useBandwidthMeter: Boolean): DataSource.Factory {
        return buildDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    private fun buildDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): DataSource.Factory {
        return DefaultDataSourceFactory(this, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(Util.getUserAgent(this, "ExoPlayerDemo"), bandwidthMeter)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.stop()
        player.release()
    }
}