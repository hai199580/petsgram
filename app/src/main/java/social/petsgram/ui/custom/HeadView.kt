package social.petsgram.ui.custom

import android.view.View

interface HeadView {

    val view: View

    fun begin()

    fun progress(progress: Float, all: Float)

    fun finishing(progress: Float, all: Float)

    fun loading()

    fun normal()

}