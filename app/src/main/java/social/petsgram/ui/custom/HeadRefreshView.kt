package social.petsgram.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.wang.avi.AVLoadingIndicatorView
import social.petsgram.R

class HeadRefreshView : FrameLayout, HeadView {

    private var tv: TextView? = null
    private var arrow: ImageView? = null
    private var progressBar: AVLoadingIndicatorView? = null



    override val view: View
        get() = this

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        init(context)
    }

    private fun init(context: Context) {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_header, null)
        addView(view)
        tv = view.findViewById(R.id.header_tv)
        arrow = view.findViewById(R.id.header_arrow)
        progressBar = view.findViewById(R.id.avi)
    }

    override fun begin() {

    }

    override fun progress(progress: Float, all: Float) {
        val s = progress / all
        if (s >= 0.9f) {
            arrow!!.rotation = 180f
        } else {
            arrow!!.rotation = 0f
        }
        if (progress >= all - 10) {
            tv!!.text = resources.getString(R.string.cancel)
        } else {
            tv!!.text = resources.getString(R.string.app_name)
        }
    }

    override fun finishing(progress: Float, all: Float) {

    }

    override fun loading() {
        arrow!!.visibility = View.GONE
        progressBar!!.visibility = View.VISIBLE
        tv!!.text = resources.getString(R.string.cancel)
    }

    override fun normal() {
        arrow!!.visibility = View.VISIBLE
        progressBar!!.visibility = View.GONE
        tv!!.text = resources.getString(R.string.cancel)
    }
}