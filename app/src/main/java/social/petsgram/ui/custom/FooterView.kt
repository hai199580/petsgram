package social.petsgram.ui.custom

import android.view.View

interface FooterView {

    /**
     * 返回当前视图
     */
    val view: View

    /**
     * 开始下拉
     */
    fun begin()

    /**
     * 回调的精度,单位为px
     *
     * @param progress 当前高度
     * @param all      总高度   为默认高度的2倍
     */
    fun progress(progress: Float, all: Float)

    fun finishing(progress: Float, all: Float)
    /**
     * 下拉完毕
     */
    fun loading()

    /**
     * 看不见的状态
     */
    fun normal()

}