package social.petsgram.ui.custom

interface BaseRefreshListener {
    fun refresh()
    fun loadMore()
}