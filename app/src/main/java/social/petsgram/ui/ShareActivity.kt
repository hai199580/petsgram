package social.petsgram.ui

import android.content.Intent
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.parse.*
import kotlinx.android.synthetic.main.activity_share.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.R
import social.petsgram.base.BaseActivity
import social.petsgram.ui.fragment.home.HomeFragment
import social.petsgram.utils.Define
import social.petsgram.utils.ToastManager
import social.petsgram.view.LoadingDialog
import social.petsgram.view.OnCancelClickListener
import java.io.File

class ShareActivity : BaseActivity() {
    private val loadingDialog: LoadingDialog by lazy {
        LoadingDialog(this)
    }

    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int = R.layout.activity_share

    private lateinit var mPath: String
    private var mTypePost: Int = 0
    private val requestOption = RequestOptions().error(R.drawable.img_error).override(480, 480)
    override fun initView() {
        if (intent.getStringExtra(Define.KeyItent.KEY_IMAGE) != null) {
            mPath = intent.getStringExtra(Define.KeyItent.KEY_IMAGE)
            mTypePost = Define.TypePost.IMAGE
            imgPlay.visibility = View.GONE
        } else if (intent.getStringExtra(Define.KeyItent.KEY_VIDEO) != null) {
            mPath = intent.getStringExtra(Define.KeyItent.KEY_VIDEO)
            mTypePost = Define.TypePost.VIDEO
        }

        Glide.with(applicationContext)
                .load(mPath)
                .apply(requestOption)
                .into(imgThumbnail)
    }

    override fun initPresenter() {
    }

    private val cancelClickListener = object : OnCancelClickListener {
        override fun onClick() {
            loadingDialog.dismiss()
        }
    }

    override fun initListener() {
        loadingDialog.setOnCancelClickListener(cancelClickListener)

        rl_next.setOnClickListener {
            share()
        }

        btn_back.setOnClickListener{
            finish()
        }
    }

    private fun share() {
        loadingDialog.show()

        val post = ParseObject("Post")
        val parseFile = ParseFile(File(mPath))
        post.put("user", ParseUser.getCurrentUser())
        post.put("caption", edtCaption.text.toString())
        post.put("file", parseFile)
        post.put("likeCount", 0)
        post.put("commentCount", 0)
        post.put("type", mTypePost)
        post.saveInBackground(object : SaveCallback {
            override fun done(e: ParseException?) {
                if (e == null) {
                    loadingDialog.dismiss()
                    HomeFragment.sInstance = null
                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                } else {
                    loadingDialog.dismiss()
                    ToastManager.showLong(R.string.error,applicationContext)
                }
            }
        })
    }

    override fun initData() {

    }
}