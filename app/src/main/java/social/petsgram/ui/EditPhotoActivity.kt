package social.petsgram.ui

import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.cannon.photoprinter.coloreffect.sticker.icons.ImageSticker
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import kotlinx.android.synthetic.main.activity_edit_photo.*
import kotlinx.android.synthetic.main.layout_bottom_edit_photo.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import social.petsgram.R
import social.petsgram.adapter.MyPagerAdapter
import social.petsgram.base.BaseActivity
import social.petsgram.model.event.*
import social.petsgram.presenter.EditPhotoPresenter
import social.petsgram.presenter.FilterType
import social.petsgram.ui.fragment.*
import social.petsgram.utils.Define
import social.petsgram.utils.GPUImageFilterTools
import social.petsgram.view.LoadingDialog
import java.util.*


interface IEditPhoto {
    fun renderFilter(mFilterGroup: GPUImageFilterGroup)
    fun addSticker(drawable: Drawable)
    fun addFrame(drawable: Drawable)
    fun onImageSaved(imagePath: String)
}

class EditPhotoActivity : BaseActivity(), View.OnClickListener, IEditPhoto {
    private val FRAME_TAG = "FRAME_TAG"

    private lateinit var loadingDialog: LoadingDialog

    private var frameView: ImageView? = null
    override fun addFrame(drawable: Drawable) {
        frameView = ImageView(baseContext)
        frameView!!.tag = FRAME_TAG
        frameView!!.layoutParams = ViewGroup.LayoutParams(rl_gpuImage.width, rl_gpuImage.height)
        frameView!!.scaleType = ImageView.ScaleType.FIT_CENTER
        frameView!!.setImageDrawable(drawable)

        removeFrame()
        rl_gpuImage.addView(frameView)
    }

    private fun removeFrame() {
        (0 until rl_gpuImage.childCount)
                .map { rl_gpuImage.getChildAt(it) }
                .forEach { child ->
                    if (child.tag is String && child.tag == FRAME_TAG) {
                        rl_gpuImage.removeView(child)
                    }
                }
    }

    override fun addSticker(drawable: Drawable) {
        val sticker = ImageSticker(drawable, mStickerView)
        mStickerView.addSticker(sticker)
    }

    override fun renderFilter(mFilterGroup: GPUImageFilterGroup) {
        gpuImageView.filter = mFilterGroup
    }

    private lateinit var fragments: ArrayList<Fragment>

    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int = R.layout.activity_edit_photo

    override fun initView() {
        loadingDialog = LoadingDialog(this)
        mStickerView.isEnabled = true
    }

    private lateinit var mPresenter: EditPhotoPresenter
    override fun initPresenter() {
        mPresenter = EditPhotoPresenter(baseContext)
        mPresenter.setListener(this)
    }

    override fun initListener() {
        imgControls.setOnClickListener(this)
        imgCrop.setOnClickListener(this)
        imgFilter.setOnClickListener(this)
        imgFrame.setOnClickListener(this)
        imgReturn.setOnClickListener(this)
        btn_back.setOnClickListener(this)
        rl_next.setOnClickListener(this)

        loadingDialog.hideCancelButton()
    }

    private lateinit var pathImage: String
    private lateinit var filterListFragment: FilterListFragment
    private lateinit var cropFragment: CropFragment
    private lateinit var frameFragment: FrameFragment
    private lateinit var adjustmentFragment: AdjustmentFragment
    private lateinit var stickerFragment: StickerFragment

    override fun initData() {
        fragments = ArrayList()
        pathImage = intent.getStringExtra(Define.KeyItent.KEY_IMAGE)
        gpuImageView.setImage(BitmapFactory.decodeFile(pathImage))

        filterListFragment = FilterListFragment()
        val bundle = Bundle()
        bundle.putString(Define.KeyItent.KEY_IMAGE, pathImage)
        filterListFragment.arguments = bundle

        cropFragment = CropFragment()
        frameFragment = FrameFragment()
        adjustmentFragment = AdjustmentFragment()
        stickerFragment = StickerFragment()

        fragments.add(cropFragment)
        fragments.add(filterListFragment)
        fragments.add(adjustmentFragment)
        fragments.add(frameFragment)
        fragments.add(stickerFragment)

        val editPhotoAdapter = MyPagerAdapter(supportFragmentManager, fragments)
        editPhotoAdapter.setPagerId(R.id.vp_editPhoto)
        vp_editPhoto.adapter = editPhotoAdapter
        vp_editPhoto.disableScroll(true)
        vp_editPhoto.offscreenPageLimit = 5
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: FilterEvent) {
        mPresenter.changeFilter(GPUImageFilterTools.createFilterForType(baseContext, event.filter))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: BrightnessEvent) {
        mPresenter.changeOption(event.brightness, FilterType.BRIGHTNESS)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ContrastEvent) {
        mPresenter.changeOption(event.contrast, FilterType.CONTRAST)
    }

    private lateinit var filterAdjuster: GPUImageFilterTools.FilterAdjuster
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: WarmthEvent) {
        mPresenter.changeOption(event.warmth.toFloat(), FilterType.WARMTH)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SaturationEvent) {
        mPresenter.changeOption(event.saturation, FilterType.SATURATION)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: VibranceEvent) {
        mPresenter.changeOption(event.vibrance, FilterType.VIBRANCE)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: FrameEvent) {
        addFrame(drawable = event.drawable)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: RemoveFrameEvent) {
        removeFrame()
        frameView = null
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: StickerEvent) {
        addSticker(drawable = event.drawable)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: RemoveStickerEvent) {
        mStickerView.deleteCurrentSticker()
    }

    override fun onClick(view: View) {
        when (view) {
            imgCrop -> {
                switchPager(0)
            }

            imgFilter -> {
                switchPager(1)
            }

            imgControls -> {
                switchPager(2)
            }

            imgFrame -> {
                switchPager(3)
            }

            imgReturn -> {
                switchPager(4)
            }

            btn_back -> {
                finish()
            }

            rl_next -> {
                val bmpGPU = gpuImageView.gpuImage.bitmapWithFilterApplied

                loadingDialog.show()

                mPresenter.saveImage(
                        bmpGPU,
                        if (frameView != null) (frameView!!.drawable as BitmapDrawable).bitmap else null,
                        if (mStickerView.mStickers.size > 0) mStickerView.bitmapOfStickerView else null
                )
            }
        }
    }

    override fun onImageSaved(imagePath: String) {
        loadingDialog.dismiss()
        val intent = Intent(this, ShareActivity::class.java)
        intent.putExtra(Define.KeyItent.KEY_IMAGE, imagePath)

        startActivity(intent)
    }

    private fun switchPager(page: Int) {
        vp_editPhoto.setCurrentItem(page, true)
        when (page) {
            0 -> {
                highlight_1.visibility = View.VISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.INVISIBLE
                highlight_4.visibility = View.INVISIBLE
                highlight_5.visibility = View.INVISIBLE
            }

            1 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.VISIBLE
                highlight_3.visibility = View.INVISIBLE
                highlight_4.visibility = View.INVISIBLE
                highlight_5.visibility = View.INVISIBLE
            }

            2 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.VISIBLE
                highlight_4.visibility = View.INVISIBLE
                highlight_5.visibility = View.INVISIBLE
            }

            3 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.INVISIBLE
                highlight_4.visibility = View.VISIBLE
                highlight_5.visibility = View.INVISIBLE
            }

            4 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.INVISIBLE
                highlight_4.visibility = View.INVISIBLE
                highlight_5.visibility = View.VISIBLE
            }
        }
    }
}