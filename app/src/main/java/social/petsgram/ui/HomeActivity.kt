package social.petsgram.ui

import android.content.Intent
import android.support.v4.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_navigation_bar.*
import social.petsgram.R
import social.petsgram.adapter.MyPagerAdapter
import social.petsgram.base.BaseActivity
import social.petsgram.ui.fragment.home.HomeFragment
import social.petsgram.ui.fragment.home.MeFragment
import social.petsgram.ui.fragment.home.NotificationFragment
import social.petsgram.ui.fragment.home.SearchFragment

class HomeActivity : BaseActivity(), View.OnClickListener {
    private val fragments : ArrayList<Fragment> by lazy {
        ArrayList<Fragment>()
    }

    override fun initView() {
        fragments.add(HomeFragment.getInstance())
        fragments.add(SearchFragment())
        fragments.add(NotificationFragment())
        fragments.add(MeFragment())

        vp_main.adapter = MyPagerAdapter(supportFragmentManager,fragments)
        vp_main.disableScroll(true)
        vp_main.offscreenPageLimit = 4
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btnHome.setOnClickListener(this)
        btnSearch.setOnClickListener(this)
        btnAddFloat.setOnClickListener(this)
        btnNotification.setOnClickListener(this)
        btnMe.setOnClickListener(this)
    }

    override fun initData() {

    }

    override fun getFragmentViewId(): Int {
        return 0
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onClick(view: View) {
        when (view) {
            btnHome -> {
                switchPager(0)
            }

            btnSearch -> {
                switchPager(1)
            }

            btnAddFloat -> {
                startActivity(Intent(this, ResourceBrowserActivity::class.java))
            }

            btnNotification -> {
                switchPager(2)
            }

            btnMe -> {
                switchPager(3)
            }
        }
    }

    fun switchPager(page : Int){
        vp_main.setCurrentItem(page,true)
        when(page){
            0 -> {
//                btnHome.setImageResource()
//                btnSearch.setImageResource()
//                btnNotification.setImageResource()
//                btnMe.setImageResource()
            }

            1 -> {

            }

            2 -> {

            }

            3 -> {

            }
        }
    }
}