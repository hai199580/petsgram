package social.petsgram.ui

import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_forgot_password.*
import social.petsgram.R
import social.petsgram.base.BaseActivity
import social.petsgram.utils.ToastManager
import social.petsgram.view.LoadingDialog



class ForgotPasswordActivity : BaseActivity() {
    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int = R.layout.activity_forgot_password

    private val loadingDialog : LoadingDialog by lazy {
        LoadingDialog(this)
    }
    override fun initView() {

    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btnSendEmail.setOnClickListener{
            if (edtForgot.text.toString().isEmpty()){
                ToastManager.showLong(R.string.error,baseContext)
            } else {
                loadingDialog.show()
                ParseUser.requestPasswordResetInBackground(edtForgot.text.toString()) { e ->
                    loadingDialog.dismiss()
                    if (e == null) {
                        ToastManager.showLong(R.string.email_sent,baseContext)
                        finish()
                    } else {
                        ToastManager.showLong(R.string.error,baseContext)
                    }
                }
            }
        }
    }

    override fun initData() {

    }
}