package social.petsgram.ui.fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_list.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.ItemFilterAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.FilterEvent
import social.petsgram.presenter.FilterListPresenter
import social.petsgram.utils.Define
import social.petsgram.utils.GPUImageFilterTools

interface IFilterListFragment {
    fun onFilterClicked(filter: GPUImageFilterTools.FilterType)
}

class FilterListFragment : BaseFragment(), IFilterListFragment, ItemFilterAdapter.ItemFilterListener {
    private lateinit var mPresenter: FilterListPresenter
    private lateinit var itemFilterAdapter: ItemFilterAdapter

    override fun getLayoutId(): Int = R.layout.fragment_filter_list

    override fun initView() {
        rvFilterList
    }

    private lateinit var bitmap : Bitmap
    override fun initData(data: Bundle?) {
        bitmap = BitmapFactory.decodeFile(data?.getString(Define.KeyItent.KEY_IMAGE))
        itemFilterAdapter = ItemFilterAdapter(context!!,this)
        rvFilterList.adapter = itemFilterAdapter
        rvFilterList.layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false)
        mPresenter.compressBitmap(bitmap)
        mPresenter.initData(itemFilterAdapter)
    }

    override fun initPresenter() {
        mPresenter = FilterListPresenter(context)
        mPresenter.setListener(this)
    }

    override fun initListener() {

    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }

    override fun onFilterClicked(filter: GPUImageFilterTools.FilterType) {
        EventBus.getDefault().post(FilterEvent(filter))
    }

    override fun onItemClick(position: Int) {
        mPresenter.applyFilter(position)
        itemFilterAdapter.setItemSelected(position)
    }
}