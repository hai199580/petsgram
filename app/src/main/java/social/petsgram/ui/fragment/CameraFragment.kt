package social.petsgram.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import com.wonderkiln.camerakit.*
import kotlinx.android.synthetic.main.fragment_camera.*
import social.petsgram.MeowApplication
import social.petsgram.R
import social.petsgram.base.BaseFragment
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.ui.EditPhotoActivity
import social.petsgram.ui.EditVideoActivity
import social.petsgram.utils.*
import social.petsgram.view.IKeepTouch
import social.petsgram.view.LoadingDialog
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


interface ICameraFragment {

}

class CameraFragment : BaseFragment(), View.OnClickListener {
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var mFFmpeg: FFmpegManager

    override fun getLayoutId(): Int = R.layout.fragment_camera

    override fun initView() {
        loadingDialog = LoadingDialog(context!!)
        loadingDialog.hideCancelButton()

        mFFmpeg = (activity?.applicationContext as MeowApplication).getFFmpegManager()
        mFFmpeg.setFFmpegListener(ffmpegListener)

        setUpCamera()
    }

    private val ffmpegListener = object : FFmpegManagerListener {
        override fun onStart(tag: String) {

        }

        override fun onProgress(tag: String, message: String) {

        }

        override fun onSuccess(tag: String, message: String) {

        }

        override fun onFailure(tag: String, message: String) {

        }

        override fun onFinished(tag: String) {
            if (tag == mFFmpeg.TAG_RESIZE_VIDEO) {
                loadingDialog.dismiss()
                val video = LocalResourceModel(data = FileUtils.getPathVideoResize(), duration = mDuration)
                val intent = Intent(baseActivity, EditVideoActivity::class.java)
                intent.putExtra(Define.KeyItent.KEY_VIDEO, video)
                startActivity(intent)
            }
        }
    }

    override fun initData(data: Bundle?) {
    }

    override fun initPresenter() {
        btn_flash.setOnClickListener(this)
        btn_switch_camera.setOnClickListener(this)
//        btn_capture.setOnClickListener(this)
    }

    override fun initListener() {
        btn_capture.setListener(mKeepTouchListener)
    }

    private val mKeepTouchListener = object : IKeepTouch {
        override fun onSingleClick() {
            camera.captureImage()
        }

        override fun onKeepTouchStart() {
            tvTip.visibility = View.GONE
            btn_capture.setImageResource(R.drawable.ic_stop_video)
            mDuration = System.currentTimeMillis()
            camera.captureVideo()
        }

        override fun onKeepTouchEnd() {
            tvTip.visibility = View.VISIBLE
            btn_capture.setImageResource(R.drawable.ic_capture)
            mDuration = System.currentTimeMillis() - mDuration
            camera.stopVideo()
        }
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }

    override fun onResume() {
        super.onResume()
        camera.start()
    }

    override fun onPause() {
        camera.stop()
        super.onPause()
    }


    private fun setUpCamera() {
        camera.setCropOutput(true)
        camera.flash = CameraKit.Constants.FLASH_OFF
        updateFlashButton()

        camera.addCameraKitListener(cameraListener)
    }

    private lateinit var mOutputPath: String
    private var mDuration = 0L
    private val cameraListener = object : CameraKitEventListener {
        override fun onVideo(video: CameraKitVideo) {
            if (mDuration < 5000) {
                ToastManager.showLong(R.string.video_short,context!!)
            } else {
                mOutputPath = FileUtils.getOutputFile()
                FileUtils.copyFile(video.videoFile, File(mOutputPath))
                val intentScan = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                intentScan.data = Uri.fromFile(File(mOutputPath))
                activity?.sendBroadcast(intentScan)

                loadingDialog.setText(R.string.resizing)
                loadingDialog.show()

                mFFmpeg.resizeVideoCommand(mOutputPath)
            }

        }

        override fun onImage(image: CameraKitImage) {
            val savedPhoto = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                    "PETS_" + System.currentTimeMillis() + ".jpg")
            try {
                val outputStream = FileOutputStream(savedPhoto.path)
                outputStream.write(image.jpeg)
                outputStream.close()

                val intentScan = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                intentScan.data = Uri.fromFile(savedPhoto)
                activity?.sendBroadcast(intentScan)

                val intent = Intent(baseActivity, EditPhotoActivity::class.java)
                intent.putExtra(Define.KeyItent.KEY_IMAGE, savedPhoto.absolutePath)
                startActivity(intent)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        override fun onEvent(p0: CameraKitEvent?) {
        }

        override fun onError(p0: CameraKitError?) {
        }
    }

    override fun onClick(view: View) {
        when (view) {
            btn_switch_camera -> {
                camera.toggleFacing()
            }

            btn_flash -> {
                camera.toggleFlash()
                updateFlashButton()
            }

            btn_capture -> {
                camera.captureImage()
            }
        }
    }

    private fun updateFlashButton() {
        when (camera.flash) {
            CameraKit.Constants.FLASH_ON -> btn_flash.setImageResource(R.drawable.ic_flash_on)
            CameraKit.Constants.FLASH_OFF -> btn_flash.setImageResource(R.drawable.ic_flash_off)
            CameraKit.Constants.FLASH_AUTO -> btn_flash.setImageResource(R.drawable.ic_flash_auto)
            CameraKit.Constants.FLASH_TORCH -> btn_flash.setImageResource(R.drawable.ic_bell)
        }
    }
//    val savedPhoto = File(Environment.getExternalStorageDirectory(), "photo.jpg")
//    try {
//        val outputStream = FileOutputStream (savedPhoto.path)
//        outputStream.write(byteArray)
//        outputStream.close()
//    } catch (e : IOException) {
//        e.printStackTrace()
//    }
}