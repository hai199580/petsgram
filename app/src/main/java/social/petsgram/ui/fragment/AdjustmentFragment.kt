package social.petsgram.ui.fragment

import android.os.Bundle
import android.util.Log
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import kotlinx.android.synthetic.main.fragment_adjustment.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.*

class AdjustmentFragment : BaseFragment() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_adjustment
    }

    override fun initView() {
        settingBrightnessSeekbar()
        settingContrastSeekbar()
        settingWarmthSeekbar()
        settingSaturationSeekbar()
        settingVibranceSeekbar()
    }

    private fun settingBrightnessSeekbar(){
        sb_brightness.setRange(-0.6F,0.6F,0.012F)
        sb_brightness.setValue(0.0F)
        sb_brightness.setIndicatorText("0")
        sb_brightness.setOnRangeChangedListener(object  : OnRangeChangedListener{
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                sb_brightness.setIndicatorText((leftValue/ 0.012).toInt().toString())
                Log.e("---------RANGE---------",(leftValue/ 0.012).toInt().toString())
                EventBus.getDefault().post(BrightnessEvent(leftValue))
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}
        })
    }

    private fun settingContrastSeekbar(){
        sb_contrast.setRange(0.0F,2.0F,0.02F)
        sb_contrast.setValue(1.0F)
        sb_contrast.setIndicatorText("0")
        sb_contrast.setOnRangeChangedListener(object  : OnRangeChangedListener{
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                sb_contrast.setIndicatorText(((leftValue - 1)/ 0.02).toInt().toString())
                EventBus.getDefault().post(ContrastEvent(leftValue))
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}
        })
    }

    private fun settingWarmthSeekbar(){
        sb_warmth.setRange(0.0F,100.0F,1.0F)
        sb_warmth.setValue(50.0F)
        sb_warmth.setIndicatorText("0")
        sb_warmth.setOnRangeChangedListener(object  : OnRangeChangedListener{
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                sb_warmth.setIndicatorText((leftValue - 50).toInt().toString())
                EventBus.getDefault().post(WarmthEvent(leftValue.toInt()))
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}
        })
    }

    private fun settingSaturationSeekbar(){
        sb_saturation.setRange(0.0F,2.0F,0.02F)
        sb_saturation.setValue(1.0F)
        sb_saturation.setIndicatorText("0")
        sb_saturation.setOnRangeChangedListener(object  : OnRangeChangedListener{
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                sb_saturation.setIndicatorText(((leftValue - 1)/ 0.02).toInt().toString())
                EventBus.getDefault().post(SaturationEvent(leftValue))
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}
        })
    }

    private fun settingVibranceSeekbar(){
        sb_vibrance.setRange(-1.2F,1.2F,0.024F)
        sb_vibrance.setValue(0.0F)
        sb_vibrance.setIndicatorText("0")
        sb_vibrance.setOnRangeChangedListener(object  : OnRangeChangedListener{
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                sb_vibrance.setIndicatorText((leftValue/ 0.024).toInt().toString())
                EventBus.getDefault().post(VibranceEvent(leftValue))
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {}
        })
    }

    override fun initData(data: Bundle?) {
    }

    override fun initPresenter() {
    }

    override fun initListener() {
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}