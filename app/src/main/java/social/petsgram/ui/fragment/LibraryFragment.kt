package social.petsgram.ui.fragment

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.aspsine.swipetoloadlayout.OnRefreshListener
import kotlinx.android.synthetic.main.fragment_library.*
import social.petsgram.R
import social.petsgram.adapter.GridAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.presenter.LibraryPresenter

interface ILibraryFragment {
    fun onLoadDataError(errorMessage: String)
    fun onDataLoaded(list : List<LocalResourceModel>)
}

class LibraryFragment : BaseFragment(), ILibraryFragment, GridAdapter.IGridAdapter, OnRefreshListener {
    override fun onRefresh() {
        mPresenter.loadData()
    }

    override fun onItemClick(position: Int) {
        mAdapter.setItemSelected(position)
        mPresenter.setImageSelected(position)
    }

    private lateinit var mAdapter : GridAdapter
    private lateinit var mPresenter : LibraryPresenter

    override fun onDataLoaded(list: List<LocalResourceModel>) {
        mAdapter.loadData(list)
        swipeToLoadLayout.isRefreshing = false
    }

    override fun onLoadDataError(errorMessage: String) {
        swipeToLoadLayout.isRefreshing = false
    }

    fun getImageData() : String {
        return mPresenter.getImageSelected()
    }

    override fun getLayoutId(): Int = R.layout.fragment_library

    override fun initView() {
        mAdapter = GridAdapter(context!!)
        mAdapter.setListener(this)
        swipe_target.layoutManager = GridLayoutManager(context,3, GridLayoutManager.VERTICAL,false)
        swipe_target.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {
    }

    override fun initPresenter() {
        mPresenter = LibraryPresenter(context!!)
        mPresenter.setListener(this)

        swipeToLoadLayout.isRefreshing = true
        mPresenter.loadData()
    }

    override fun initListener() {
        swipeToLoadLayout.setOnRefreshListener(this)
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}