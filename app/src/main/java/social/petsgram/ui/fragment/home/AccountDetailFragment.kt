package social.petsgram.ui.fragment.home

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.R
import social.petsgram.adapter.AccountPostAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.PostModel
import social.petsgram.model.UserModel
import social.petsgram.presenter.AccountDetailPresenter
import social.petsgram.utils.Define

interface IAccountDetail {
    fun onDataLoaded(list: List<PostModel>)
    fun onUpdateFollow(isFollowed : Boolean)
    fun onUpdateFollowFailed()
}

class AccountDetailFragment : BaseFragment(), IAccountDetail{
    private lateinit var mPresenter : AccountDetailPresenter
    private lateinit var mAccount: UserModel

    private val adapterListener = object : AccountPostAdapter.IAccountPostAdapter{
        override fun onItemClick(position: Int) {
            val bundle = Bundle()
            val postModel = mPresenter.getPostByPosition(position)
            bundle.putParcelable(Define.KeyItent.KEY_POST,postModel)

            if (postModel.getPostType() == Define.TypePost.IMAGE){
                HomeFragment.getInstance().pushFragment(PostDetailFragment(),transferData = bundle)
            } else {
                HomeFragment.getInstance().pushFragment(VideoDetailFragment(),transferData = bundle)
            }
        }
    }

    private val mAdapter : AccountPostAdapter by lazy {
        AccountPostAdapter(context!!,adapterListener)
    }

    override fun onDataLoaded(list: List<PostModel>) {
        mAdapter.loadData(list)
    }

    override fun onUpdateFollow(isFollowed: Boolean) {
        btn_follow_status.isEnabled = true
        if (isFollowed) {
            btn_follow_status.background = ContextCompat.getDrawable(context!!,R.drawable.bg_followed)
            btn_follow_status.text = context?.resources?.getString(R.string.followed)
            btn_follow_status.setTextColor(ContextCompat.getColor(context!!,R.color.black))
        } else {
            btn_follow_status.background = ContextCompat.getDrawable(context!!,R.drawable.bg_follow)
            btn_follow_status.text = context?.resources?.getString(R.string.follow)
            btn_follow_status.setTextColor(ContextCompat.getColor(context!!,R.color.white))
        }
    }

    override fun onUpdateFollowFailed() {
        btn_follow_status.isEnabled = true
    }

    override fun getLayoutId(): Int = R.layout.fragment_account

    override fun initView() {
        rl_next.visibility = View.GONE
        rv_post.layoutManager = GridLayoutManager(context,3,GridLayoutManager.VERTICAL,false)
        rv_post.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {
        if (data != null){
            mAccount = UserModel(data.getParcelable(Define.KeyItent.KEY_USER))
            tv_title.text = mAccount.getUserName()
            bindAccountInfo()
            mPresenter.loadData(mAccount.getParseUser())
            mPresenter.getFollowStatus(mAccount.getParseUser())
        }
    }

    private val requestOption = RequestOptions().error(R.drawable.ic_sticker).override(128, 128).circleCrop()
    private fun bindAccountInfo(){
        Glide.with(context!!)
                .load(mAccount.getUserAvatar()?.file)
                .apply(requestOption)
                .into(imgAvatar)
        tvName.text = mAccount.getUserFullname()
        tvDes.text = mAccount.getUserDescription()
    }

    override fun initPresenter() {
        mPresenter = AccountDetailPresenter(context!!,this)
    }

    override fun initListener() {
        btn_back.setOnClickListener{
            HomeFragment.getInstance().popFragment()
        }

        btn_follow_status.setOnClickListener{
            mPresenter.requestFollow(mAccount.getParseUser())
            btn_follow_status.isEnabled = false
        }
    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }
}