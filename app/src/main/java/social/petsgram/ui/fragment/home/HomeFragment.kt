package social.petsgram.ui.fragment.home

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.aspsine.swipetoloadlayout.OnRefreshListener
import kotlinx.android.synthetic.main.fragment_library.*
import social.petsgram.R
import social.petsgram.adapter.Interator
import social.petsgram.adapter.PostAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.PostModel
import social.petsgram.presenter.HomePresenter
import social.petsgram.utils.Define
import social.petsgram.utils.ToastManager

interface IHomeFragment {
    fun onDataLoaded(listData: ArrayList<PostModel>)
    fun onLoadFailed()
    fun onUpdateLikeSuccess(position : Int)
    fun onUpdateLikeFailure(position : Int)
    fun onUpdateBookmarkSuccess(position : Int)
    fun onUpdateBookmarkFailure(position : Int)
}

class HomeFragment : BaseFragment(), OnRefreshListener, IHomeFragment {
    private lateinit var mPresenter : HomePresenter
    private lateinit var mAdapter: PostAdapter

    companion object {
        var sInstance : HomeFragment? = null
        fun getInstance() : HomeFragment{
            if (sInstance == null){
                sInstance = HomeFragment()
            }
            return sInstance as HomeFragment
        }
    }

    override fun onLoadFailed() {
        swipeToLoadLayout.isRefreshing = false
        onRefresh()
    }

    override fun onDataLoaded(listData: ArrayList<PostModel>) {
        mAdapter.loadData(listData)
        swipeToLoadLayout.isRefreshing = false
    }

    override fun onUpdateLikeSuccess(position: Int) {
        mAdapter.onUpdateLikeSuccess(position)
    }

    override fun onUpdateLikeFailure(position: Int) {
        ToastManager.showLong(R.string.error,context!!)
        mAdapter.onUpdateLikeFailure(position)
    }

    override fun onUpdateBookmarkSuccess(position: Int) {
        mAdapter.onUpdateBookmarkSuccess(position)
    }

    override fun onUpdateBookmarkFailure(position: Int) {
        ToastManager.showLong(R.string.error,context!!)
        mAdapter.onUpdateBookmarkFailure(position)
    }

    override fun onRefresh() {
        swipeToLoadLayout.isRefreshing = true
        mPresenter.loadData()
    }

    override fun initPresenter() {
        mPresenter = HomePresenter(context!!)
        mPresenter.setPresenter(this)

        swipeToLoadLayout.isRefreshing = true
        mPresenter.loadData()
    }

    override fun initListener() {
        swipeToLoadLayout.setOnRefreshListener(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    private val onItemClicked = object : Interator {
        override fun onAccountClick(position: Int) {
            val bundle = Bundle()
            bundle.putParcelable(Define.KeyItent.KEY_USER,mPresenter.getPostByPosition(position).getPostUser())
            HomeFragment.getInstance().pushFragment(AccountDetailFragment(),transferData = bundle)
        }

        override fun onLikeClick(position: Int) {
            mAdapter.onLikeClicked(position)
            mPresenter.updateLikeStatus(position)
        }

        override fun onShareClick(position: Int) {

        }

        override fun onBookmarkClick(position: Int) {
            mAdapter.onBookmarkClicked(position)
            mPresenter.updateBookmarkStatus(position)
        }

        override fun onItemClick(position: Int) {
            val bundle = Bundle()
            val postModel = mPresenter.getPostByPosition(position)
            bundle.putParcelable(Define.KeyItent.KEY_POST,postModel)

            if (postModel.getPostType() == Define.TypePost.IMAGE){
                HomeFragment.getInstance().pushFragment(PostDetailFragment(),transferData = bundle)
            } else {
                HomeFragment.getInstance().pushFragment(VideoDetailFragment(),transferData = bundle)
            }
        }
    }

    override fun initView() {
        swipe_target.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        mAdapter = PostAdapter(context!!, onItemClicked)
        swipe_target.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {

    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }
}