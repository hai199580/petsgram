package social.petsgram.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_music_video.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.MusicAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.CancelMusicPicked
import social.petsgram.model.event.MusicEvent
import social.petsgram.utils.FileUtils
import java.io.File

class MusicVideoFragment : BaseFragment(),MusicAdapter.MusicAdapterListener{
    private lateinit var mAdapter : MusicAdapter
    private val mListMusic : ArrayList<MusicEvent> by lazy {
        ArrayList<MusicEvent>()
    }

    override fun onItemClick(position: Int) {
        mAdapter.setItemSelected(position)
        EventBus.getDefault().post(mListMusic[position])
    }

    override fun onCancelMusic() {
        EventBus.getDefault().post(CancelMusicPicked())
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_music_video
    }

    override fun initView() {
        mAdapter = MusicAdapter(this, context!!)
        rv_music.adapter = mAdapter
        rv_music.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
    }

    override fun initData(data: Bundle?) {
        val listMusic = FileUtils.getListMusic()
        listMusic.forEach {
            mListMusic.add(MusicEvent(it,File(it).name))
        }
        mAdapter.loadData(mListMusic)

//        val query: ParseQuery<ParseObject> = ParseQuery("VideoMusic")
//        query.findInBackground { objects, e ->
//            if (e == null) {
//                objects.forEach {
//                    mListMusic.add(MusicEvent(it.getParseFile("music")?.file?.absolutePath,it.getString("name")))
//                }
//
//
//            }
//        }
    }

    override fun initPresenter() {
    }

    override fun initListener() {
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}