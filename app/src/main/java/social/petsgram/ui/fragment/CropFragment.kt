package social.petsgram.ui.fragment

import android.os.Bundle
import social.petsgram.R
import social.petsgram.base.BaseFragment

class CropFragment : BaseFragment() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_crop
    }

    override fun initView() {

    }

    override fun initData(data: Bundle?) {
    }

    override fun initPresenter() {
    }

    override fun initListener() {
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}