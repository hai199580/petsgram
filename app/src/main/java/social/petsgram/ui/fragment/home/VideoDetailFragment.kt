package social.petsgram.ui.fragment.home

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.parse.ParseObject
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.fragment_video_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.R
import social.petsgram.base.BaseFragment
import social.petsgram.model.PostModel
import social.petsgram.utils.Define
import java.io.File

class VideoDetailFragment : BaseFragment() {
    private var mediaDataSourceFactory: DataSource.Factory? = null
    private var mainHandler: Handler? = null
    private lateinit var player: SimpleExoPlayer

    override fun getLayoutId(): Int = R.layout.fragment_video_detail

    override fun initView() {
        rl_next.visibility = View.GONE
        tv_title.text = context?.resources?.getText(R.string.app_name)
    }

    private lateinit var mPost: PostModel
    override fun initData(data: Bundle?) {
        if (data != null) {
            mPost = data.getParcelable(Define.KeyItent.KEY_POST) as PostModel
            bindView()
            loadVideo()
        }
    }

    private fun loadVideo(){
        mainHandler = Handler()
        player = ExoPlayerFactory.newSimpleInstance(context)
        playerView.player = player
        playerView.useController = false
        playerView.requestFocus()
        mediaDataSourceFactory = buildDataSourceFactory(false)
        //        Video Source
        val uri = Uri.fromFile(File(mPost.getPostFile()?.file?.absolutePath))
        //        Default Media Source
        val mediaSource = buildMediaSource(uri, "mp4")

        player.prepare(mediaSource)
        player.playWhenReady = false
    }

    private val requestOption = RequestOptions().error(R.drawable.ic_sticker).override(128, 128).circleCrop()
    private fun bindView() {
        val user = mPost.getParseObject().fetchIfNeeded<ParseObject>().getParseObject("user")

        if (user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar") != null) {
            Glide.with(context!!)
                    .load(compressBitmap(user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar")?.file!!))
                    .apply(requestOption)
                    .into(imgAvatar)
        } else {
            Glide.with(context!!)
                    .load(user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar")?.file)
                    .apply(requestOption)
                    .into(imgAvatar)
        }

        tvName.text = user?.getString("username")

        tvCaption.text = mPost.getPostCaption()
        if (mPost.getLikeCount()!! == 0) {
            tvNumberLike.visibility = View.GONE
        } else if (mPost.getLikeCount()!! in 1..1) {
            tvNumberLike.visibility = View.VISIBLE
            tvNumberLike.text = "${mPost.getLikeCount()} like"
        } else {
            tvNumberLike.visibility = View.VISIBLE
            tvNumberLike.text = "${mPost.getLikeCount()} likes"
        }

        if (mPost.isLiked()) {
            btnLike.setImageResource(R.drawable.ic_heart)
        } else {
            btnLike.setImageResource(R.drawable.ic_heart_none)
        }

        if (mPost.isBookmarked()) {
            btnBookmark.setImageResource(R.drawable.ic_bookmark)
        } else {
            btnBookmark.setImageResource(R.drawable.ic_bookmark_none)
        }
    }

    private fun compressBitmap(file: File): Bitmap {
        return Compressor(context!!)
                .setMaxWidth(720)
                .setMaxHeight(720)
                .setQuality(70)
                .setCompressFormat(Bitmap.CompressFormat.PNG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).absolutePath)
                .compressToBitmap(file)
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btn_back.setOnClickListener(onViewClickListener)
        btnLike.setOnClickListener(onViewClickListener)
        btnComment.setOnClickListener(onViewClickListener)
        btnShare.setOnClickListener(onViewClickListener)
        btnBookmark.setOnClickListener(onViewClickListener)
        imgAvatar.setOnClickListener(onViewClickListener)
        tvName.setOnClickListener(onViewClickListener)
        btnPlay.setOnClickListener(onViewClickListener)

        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                if (playbackState == Player.STATE_ENDED) {
                    player.seekTo(0)
                    player.playWhenReady = false
                    btnPlay.visibility = View.VISIBLE
                }
            }
        })
    }

    private val onViewClickListener = object : View.OnClickListener {
        override fun onClick(view: View) {
            when(view){
                btn_back -> {
                    HomeFragment.getInstance().popFragment()
                }

                btnLike -> {

                }

                btnComment -> {

                }

                btnShare -> {

                }

                btnBookmark -> {

                }

                imgAvatar,
                tvName -> {
                    val bundle = Bundle()
                    bundle.putParcelable(Define.KeyItent.KEY_USER,mPost.getPostUser())
                    HomeFragment.getInstance().pushFragment(AccountDetailFragment(),transferData = bundle)
                }

                btnPlay -> {
                    player.playWhenReady = true
                    btnPlay.visibility = View.INVISIBLE
                }
            }
        }
    }

    override fun onAttachView() {

    }

    override fun onDetachView() {
        player.stop()
    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.stop()
        player.release()
    }

    private val BANDWIDTH_METER = DefaultBandwidthMeter()
    private fun buildDataSourceFactory(useBandwidthMeter: Boolean): DataSource.Factory {
        return buildDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    private fun buildDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): DataSource.Factory {
        return DefaultDataSourceFactory(context, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(Util.getUserAgent(context, "ExoPlayerDemo"), bandwidthMeter)
    }

    private fun buildMediaSource(uri: Uri, overrideExtension: String): MediaSource {
        val type = if (TextUtils.isEmpty(overrideExtension))
            Util.inferContentType(uri)
        else
            Util.inferContentType(".$overrideExtension")
        return when (type) {
            C.TYPE_SS -> SsMediaSource(uri, buildDataSourceFactory(false),
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_DASH -> DashMediaSource(uri, buildDataSourceFactory(false),
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_HLS -> HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null)
            C.TYPE_OTHER -> ExtractorMediaSource(uri, mediaDataSourceFactory, DefaultExtractorsFactory(),
                    mainHandler, null)
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }
}