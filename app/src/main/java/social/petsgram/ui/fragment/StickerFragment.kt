package social.petsgram.ui.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_frame.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.DrawableAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.RemoveStickerEvent
import social.petsgram.model.event.StickerEvent
import social.petsgram.presenter.StickerPresenter

interface IStickerFragment {
    fun onDataLoaded(list: List<Drawable>)
}

class StickerFragment : BaseFragment(), IStickerFragment, DrawableAdapter.DrawableAdapterListener {
    private lateinit var mAdapter: DrawableAdapter
    private lateinit var mPresenter: StickerPresenter

    override fun onDataLoaded(list: List<Drawable>) {
        mAdapter.loadData(list)
    }

    override fun onItemClick(drawable: Drawable) {
        EventBus.getDefault().post(StickerEvent(drawable))
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_frame
    }

    override fun initView() {
        mAdapter = DrawableAdapter(this)
        rv_frames.layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false)
        rv_frames.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {

    }

    override fun initPresenter() {
        mPresenter = StickerPresenter(context!!)
        mPresenter.setPresenter(this)
        mPresenter.loadData()
    }

    override fun initListener() {
        btn_remove.setOnClickListener{
            EventBus.getDefault().post(RemoveStickerEvent())
        }
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}