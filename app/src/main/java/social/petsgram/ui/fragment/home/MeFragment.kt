package social.petsgram.ui.fragment.home

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_me.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.R
import social.petsgram.adapter.AccountPostAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.PostModel
import social.petsgram.model.UserModel
import social.petsgram.presenter.MePresenter
import social.petsgram.ui.HomeActivity
import social.petsgram.utils.Define

interface IMeFragment {
    fun onDataLoaded(list: List<PostModel>)
}

class MeFragment : BaseFragment(), View.OnClickListener, IMeFragment {
    private lateinit var mPresenter : MePresenter

    override fun onDataLoaded(list: List<PostModel>) {
        mAdapter.loadData(list)
    }

    private val adapterListener = object : AccountPostAdapter.IAccountPostAdapter{
        override fun onItemClick(position: Int) {
            val bundle = Bundle()
            val postModel = mPresenter.getPostByPosition(position)
            bundle.putParcelable(Define.KeyItent.KEY_POST,postModel)

            (activity as HomeActivity).switchPager(0)
            if (postModel.getPostType() == Define.TypePost.IMAGE){
                HomeFragment.getInstance().pushFragment(PostDetailFragment(),transferData = bundle)
            } else {
                HomeFragment.getInstance().pushFragment(VideoDetailFragment(),transferData = bundle)
            }
        }
    }

    private val mAdapter : AccountPostAdapter by lazy {
        AccountPostAdapter(context!!,adapterListener)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_me
    }

    private lateinit var mUser : UserModel
    override fun initView() {
        rl_next.visibility = View.INVISIBLE
        btn_back.visibility = View.INVISIBLE

        rv_post.layoutManager = GridLayoutManager(context,3,GridLayoutManager.VERTICAL,false)
        rv_post.adapter = mAdapter
    }

    private val requestOption = RequestOptions().error(R.drawable.ic_sticker).override(128, 128).circleCrop()
    private fun bindAccountInfo(){
        Glide.with(context!!)
                .load(mUser.getUserAvatar()?.file)
                .apply(requestOption)
                .into(imgAvatar)
        tvName.text = mUser.getUserFullname()
        tvDes.text = mUser.getUserDescription()
    }

    override fun initData(data: Bundle?) {
        mUser = UserModel(ParseUser.getCurrentUser())
        tv_title.text = mUser.getUserName()
        bindAccountInfo()
        mPresenter.loadData(mUser.getParseUser())
    }

    override fun initPresenter() {
        mPresenter = MePresenter(context!!,this)
    }

    override fun initListener() {

    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }

    override fun onClick(view: View) {
        when(view){

        }
    }
}