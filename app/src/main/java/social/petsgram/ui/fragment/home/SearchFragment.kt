package social.petsgram.ui.fragment.home

import android.os.Bundle
import social.petsgram.R
import social.petsgram.base.BaseFragment

class SearchFragment : BaseFragment() {
    override fun initPresenter() {

    }

    override fun initListener() {

    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun initView() {

    }

    override fun initData(data: Bundle?) {

    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }
}