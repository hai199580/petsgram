package social.petsgram.ui.fragment.home

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.parse.ParseObject
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.fragment_post_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import social.petsgram.R
import social.petsgram.base.BaseFragment
import social.petsgram.dialog.PreviewImageDialog
import social.petsgram.model.PostModel
import social.petsgram.utils.Define
import java.io.File

class PostDetailFragment : BaseFragment() {

    override fun getLayoutId(): Int = R.layout.fragment_post_detail

    override fun initView() {
        rl_next.visibility = View.GONE
        tv_title.text = context?.resources?.getText(R.string.app_name)
    }

    private lateinit var mPost: PostModel
    override fun initData(data: Bundle?) {
        if (data != null) {
            mPost = data.getParcelable(Define.KeyItent.KEY_POST) as PostModel
            bindView()
        }
    }

    private val requestOption = RequestOptions().error(R.drawable.ic_sticker).override(128, 128).circleCrop()
    private val requestOption1 = RequestOptions().error(R.drawable.img_error).override(480, 480)
    private fun bindView() {
        val user = mPost.getParseObject().fetchIfNeeded<ParseObject>().getParseObject("user")

        if (user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar") != null) {
            Glide.with(context!!)
                    .load(compressBitmap(user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar")?.file!!))
                    .apply(requestOption)
                    .into(imgAvatar)
        } else {
            Glide.with(context!!)
                    .load(user?.fetchIfNeeded<ParseObject>()?.getParseFile("avatar")?.file)
                    .apply(requestOption)
                    .into(imgAvatar)
        }

        tvName.text = user?.getString("username")

        Glide.with(context!!)
                .load(compressBitmap(mPost.getPostFile()?.file!!))
                .apply(requestOption1)
                .into(imgSource)

        tvCaption.text = mPost.getPostCaption()
        if (mPost.getLikeCount()!! == 0) {
            tvNumberLike.visibility = View.GONE
        } else if (mPost.getLikeCount()!! in 1..1) {
            tvNumberLike.visibility = View.VISIBLE
            tvNumberLike.text = "${mPost.getLikeCount()} like"
        } else {
            tvNumberLike.visibility = View.VISIBLE
            tvNumberLike.text = "${mPost.getLikeCount()} likes"
        }

        if (mPost.isLiked()) {
            btnLike.setImageResource(R.drawable.ic_heart)
        } else {
            btnLike.setImageResource(R.drawable.ic_heart_none)
        }

        if (mPost.isBookmarked()) {
            btnBookmark.setImageResource(R.drawable.ic_bookmark)
        } else {
            btnBookmark.setImageResource(R.drawable.ic_bookmark_none)
        }
    }

    private fun compressBitmap(file: File): Bitmap {
        return Compressor(context!!)
                .setMaxWidth(720)
                .setMaxHeight(720)
                .setQuality(70)
                .setCompressFormat(Bitmap.CompressFormat.PNG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).absolutePath)
                .compressToBitmap(file)
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btn_back.setOnClickListener(onViewClickListener)
        btnLike.setOnClickListener(onViewClickListener)
        btnComment.setOnClickListener(onViewClickListener)
        btnShare.setOnClickListener(onViewClickListener)
        btnBookmark.setOnClickListener(onViewClickListener)
        imgAvatar.setOnClickListener(onViewClickListener)
        tvName.setOnClickListener(onViewClickListener)
        imgSource.setOnClickListener(onViewClickListener)
    }

    private val onViewClickListener = object : View.OnClickListener {
        override fun onClick(view: View) {
            when(view){
                btn_back -> {
                    HomeFragment.getInstance().popFragment()
                }

                btnLike -> {

                }

                btnComment -> {

                }

                btnShare -> {

                }

                btnBookmark -> {

                }

                imgSource -> {
                    val args = Bundle()
                    args.putString("URL_IMAGE", mPost.getPostFile()?.file?.absolutePath)
                    val previewImageDialog = PreviewImageDialog.newInstance(args)
                    showDialogFragment(childFragmentManager, previewImageDialog)
                }

                imgAvatar,
                tvName -> {
                    val bundle = Bundle()
                    bundle.putParcelable(Define.KeyItent.KEY_USER,mPost.getPostUser())
                    HomeFragment.getInstance().pushFragment(AccountDetailFragment(),transferData = bundle)
                }
            }
        }
    }

    override fun onAttachView() {

    }

    override fun onDetachView() {

    }

    private fun showDialogFragment(fragmentManager: FragmentManager, dialogFragment: DialogFragment?) {
        if (dialogFragment != null) {
            if (dialogFragment.isAdded) {
                dialogFragment.dismiss()
            }
            val transaction = fragmentManager.beginTransaction().add(dialogFragment, dialogFragment.javaClass.simpleName)
            transaction.setCustomAnimations(R.anim.anim_zoom_in, R.anim.anim_zoom_out, R.anim.anim_zoom_in, R.anim.anim_zoom_out)
            transaction.commitAllowingStateLoss()
        }
    }
}