package social.petsgram.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_video.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.VideoFilterAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.VideoFIlterEvent
import social.petsgram.utils.VideoFilterType


class FilterVideoFragment : BaseFragment(){
    override fun getLayoutId(): Int {
        return R.layout.fragment_filter_video
    }

    private lateinit var mFilterList : Array<VideoFilterType>

    private val filterItemClick = object : VideoFilterAdapter.Interator {
        override fun onItemClick(position: Int) {
            EventBus.getDefault().post(VideoFIlterEvent(mFilterList[position]))
        }
    }

    override fun initView() {
        mFilterList = VideoFilterType.createFilterList()
        val adapter = VideoFilterAdapter(mFilterList,filterItemClick)
        rv_videoFilter.layoutManager = LinearLayoutManager(context!!,LinearLayoutManager.VERTICAL,false)
        rv_videoFilter.adapter = adapter
    }

    override fun initData(data: Bundle?) {
    }

    override fun initPresenter() {
    }

    override fun initListener() {
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}