package social.petsgram.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_frame.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.EmojiAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.EmojiEvent
import social.petsgram.model.event.RemoveEmojiEvent
import social.petsgram.presenter.EmojiPresenter

interface IEmojiFragment {
    fun onDataLoaded(list: List<EmojiEvent>)
}

class EmojiFragment  : BaseFragment(), IEmojiFragment, EmojiAdapter.EmojiAdapterListener {
    private lateinit var mAdapter: EmojiAdapter
    private lateinit var mPresenter: EmojiPresenter

    override fun onDataLoaded(list: List<EmojiEvent>) {
        mAdapter.loadData(list)
    }

    override fun onItemClick(emojiEvent: EmojiEvent) {
        EventBus.getDefault().post(emojiEvent)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_frame
    }

    override fun initView() {
        mAdapter = EmojiAdapter(context!!,this)
        rv_frames.layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false)
        rv_frames.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {

    }

    override fun initPresenter() {
        mPresenter = EmojiPresenter(context!!)
        mPresenter.setPresenter(this)
        mPresenter.loadData()
    }

    override fun initListener() {
        btn_remove.setOnClickListener{
            EventBus.getDefault().post(RemoveEmojiEvent())
        }
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}