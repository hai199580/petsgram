package social.petsgram.ui.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_frame.*
import org.greenrobot.eventbus.EventBus
import social.petsgram.R
import social.petsgram.adapter.DrawableAdapter
import social.petsgram.base.BaseFragment
import social.petsgram.model.event.FrameEvent
import social.petsgram.model.event.RemoveFrameEvent
import social.petsgram.presenter.FramePresenter

interface IFrameFragment {
    fun onDataLoaded(list: List<Drawable>)
}

class FrameFragment : BaseFragment(), IFrameFragment, DrawableAdapter.DrawableAdapterListener {
    private lateinit var mAdapter: DrawableAdapter
    private lateinit var mPresenter: FramePresenter

    override fun onDataLoaded(list: List<Drawable>) {
        mAdapter.loadData(list)
    }

    override fun onItemClick(drawable: Drawable) {
        EventBus.getDefault().post(FrameEvent(drawable))
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_frame
    }

    override fun initView() {
        mAdapter = DrawableAdapter(this)
        rv_frames.layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.HORIZONTAL, false)
        rv_frames.adapter = mAdapter
    }

    override fun initData(data: Bundle?) {

    }

    override fun initPresenter() {
        mPresenter = FramePresenter(context!!)
        mPresenter.setPresenter(this)
        mPresenter.loadData()
    }

    override fun initListener() {
        btn_remove.setOnClickListener{
            EventBus.getDefault().post(RemoveFrameEvent())
        }
    }

    override fun onAttachView() {
    }

    override fun onDetachView() {
    }
}