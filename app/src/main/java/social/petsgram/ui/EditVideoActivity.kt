package social.petsgram.ui

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.cannon.photoprinter.coloreffect.sticker.icons.ImageSticker
import com.daasuu.gpuv.composer.GPUMp4Composer
import com.daasuu.gpuv.egl.filter.GlFilter
import com.daasuu.gpuv.player.GPUPlayerView
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_edit_video.*
import kotlinx.android.synthetic.main.layout_bottom_edit_video.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import social.petsgram.MeowApplication
import social.petsgram.R
import social.petsgram.adapter.MyPagerAdapter
import social.petsgram.base.BaseActivity
import social.petsgram.model.event.*
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.ui.fragment.EmojiFragment
import social.petsgram.ui.fragment.FilterVideoFragment
import social.petsgram.ui.fragment.MusicVideoFragment
import social.petsgram.utils.*
import social.petsgram.view.LoadingDialog
import social.petsgram.view.OnCancelClickListener
import java.io.File

interface IEditVideo {
    fun addEmoji(drawable: Drawable)
    fun addFilter()
    fun onVideoProcessed()
}

class EditVideoActivity : BaseActivity(), View.OnClickListener, OnCancelClickListener {
    private lateinit var mFFmpeg: FFmpegManager
    private var mediaDataSourceFactory: DataSource.Factory? = null
    private var mainHandler: Handler? = null
    private lateinit var player: SimpleExoPlayer
    private var mMusicPicked: MusicEvent? = null
    private var mFilterPicked : VideoFIlterEvent? = null

    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int = R.layout.activity_edit_video

    private val loadingDialog: LoadingDialog by lazy {
        LoadingDialog(this)
    }
    private lateinit var mVideo: LocalResourceModel
    private lateinit var gpuPlayerView : GPUPlayerView
    override fun initView() {
        setupViewPager()
        stickerView.isEnabled = true
        mFFmpeg = (applicationContext as MeowApplication).getFFmpegManager()
        mFFmpeg.setFFmpegListener(ffmpegListener)
        mainHandler = Handler()
        player = ExoPlayerFactory.newSimpleInstance(baseContext)
//        playerView.player = player
//        playerView.useController = false
//        playerView.requestFocus()
        mediaDataSourceFactory = buildDataSourceFactory(false)
        mVideo = intent.getParcelableExtra(Define.KeyItent.KEY_VIDEO)
        //        Video Source
        val uri = Uri.fromFile(File(mVideo.data))
        //        Default Media Source
        val mediaSource = buildMediaSource(uri, "mp4")

        player.prepare(mediaSource)
        player.playWhenReady = true

        gpuPlayerView = GPUPlayerView (this)
        // set SimpleExoPlayer
        gpuPlayerView.setSimpleExoPlayer(player)
        gpuPlayerView.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        rl_videoView.addView(gpuPlayerView)
        gpuPlayerView.onResume()
    }

    private val fragments: ArrayList<Fragment> by lazy {
        ArrayList<Fragment>()
    }

    private fun setupViewPager() {
        fragments.add(FilterVideoFragment())
        fragments.add(EmojiFragment())
        fragments.add(MusicVideoFragment())

        val editPhotoAdapter = MyPagerAdapter(supportFragmentManager, fragments)
        editPhotoAdapter.setPagerId(R.id.vp_editVideo)
        vp_editVideo.adapter = editPhotoAdapter
        vp_editVideo.disableScroll(true)
        vp_editVideo.offscreenPageLimit = 3
    }

    override fun onClick() {
        loadingDialog.dismiss()
        mFFmpeg.killRunningProcess()
    }

    private val ffmpegListener = object : FFmpegManagerListener {
        override fun onStart(tag: String) {

        }

        override fun onProgress(tag: String, message: String) {

        }

        override fun onSuccess(tag: String, message: String) {

        }

        override fun onFailure(tag: String, message: String) {

        }

        override fun onFinished(tag: String) {
            if (tag == mFFmpeg.TAG_EMOJI_VIDEO) {
                if (mMusicPicked != null) {
                    mOutputPath = FileUtils.getOutputFile()
                    mFFmpeg.mergeAudio(FileUtils.getPathVideoEmoji(), mMusicPicked!!.data!!, mOutputPath)
                } else {
                    mOutputPath = FileUtils.getOutputFile()
                    FileUtils.copyFile(File(FileUtils.getPathVideoEmoji()), File(mOutputPath))
                    loadingDialog.dismiss()
                    goShare()
                }
            }

            if (tag == mFFmpeg.TAG_MERGE_AUDIO) {
                loadingDialog.dismiss()
                goShare()
            }
        }
    }

    override fun initPresenter() {
        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                if (playbackState == Player.STATE_ENDED) {
                    player.seekTo(0)
                    player.playWhenReady = true
                }
            }
        })
    }

    override fun initListener() {
        loadingDialog.setOnCancelClickListener(this)
        rl_next.setOnClickListener(this)
        btn_back.setOnClickListener(this)
        imgFilter.setOnClickListener(this)
        imgEmoji.setOnClickListener(this)
        imgMusic.setOnClickListener(this)
    }

    override fun initData() {

    }

    private fun buildMediaSource(uri: Uri, overrideExtension: String): MediaSource {
        val type = if (TextUtils.isEmpty(overrideExtension))
            Util.inferContentType(uri)
        else
            Util.inferContentType(".$overrideExtension")
        return when (type) {
            C.TYPE_SS -> SsMediaSource(uri, buildDataSourceFactory(false),
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_DASH -> DashMediaSource(uri, buildDataSourceFactory(false),
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null)
            C.TYPE_HLS -> HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null)
            C.TYPE_OTHER -> ExtractorMediaSource(uri, mediaDataSourceFactory, DefaultExtractorsFactory(),
                    mainHandler, null)
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }

    private val BANDWIDTH_METER = DefaultBandwidthMeter()
    private fun buildDataSourceFactory(useBandwidthMeter: Boolean): DataSource.Factory {
        return buildDataSourceFactory(if (useBandwidthMeter) BANDWIDTH_METER else null)
    }

    private fun buildDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): DataSource.Factory {
        return DefaultDataSourceFactory(this, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(Util.getUserAgent(this, "ExoPlayerDemo"), bandwidthMeter)
    }

    override fun onClick(view: View) {
        when (view) {
            btn_back -> {
                mFFmpeg.killRunningProcess()
                finish()
            }

            rl_next -> {
                loadingDialog.show()
                loadingDialog.setText(R.string.processing)
                if (mFilterPicked != null){
                    applyVideoFilter(VideoFilterType.createGlFilter(mFilterPicked!!.filter,baseContext))
                } else {
                    if (stickerView.mStickers.size > 0) {
                        mFFmpeg.addEmoji(mVideo, stickerView)
                    } else if (mMusicPicked != null) {
                        mOutputPath = FileUtils.getOutputFile()
                        mFFmpeg.mergeAudio(mVideo.data, mMusicPicked!!.data!!, mOutputPath)
                    } else {
                        mOutputPath = FileUtils.getOutputFile()
                        FileUtils.copyFile(File(FileUtils.getPathVideoResize()), File(mOutputPath))
                        loadingDialog.dismiss()
                        goShare()
                    }
                }
            }

            imgFilter -> {
                switchPager(0)
            }

            imgEmoji -> {
                switchPager(1)
            }

            imgMusic -> {
                switchPager(2)
            }
        }
    }

    private lateinit var mOutputPath: String
    private fun goShare() {
        val scanner = MediaScannerWrapper(baseContext, mOutputPath, null)
        scanner.scan()

//        val intentScan = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
//        intentScan.data = Uri.fromFile(File(mOutputPath))
//        sendBroadcast(intent)

        val intent = Intent(this, ShareActivity::class.java)
        intent.putExtra(Define.KeyItent.KEY_VIDEO, mOutputPath)

        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        player.stop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mFFmpeg.killRunningProcess()
        player.stop()
        player.release()
    }

    private fun addSticker(emojiEvent: EmojiEvent) {
        val sticker = ImageSticker(emojiEvent.drawable, stickerView)
        sticker.stickerName = emojiEvent.name
        sticker.stickerPath = emojiEvent.path
        sticker.isCanRotate = false
        stickerView.addSticker(sticker)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EmojiEvent) {
        addSticker(event)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: RemoveEmojiEvent) {
        stickerView.deleteCurrentSticker()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MusicEvent) {
        mMusicPicked = event
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CancelMusicPicked) {
        mMusicPicked = null
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: VideoFIlterEvent) {
        mFilterPicked = event
        gpuPlayerView.setGlFilter(VideoFilterType.createGlFilter(event.filter,baseContext))
    }

    private fun applyVideoFilter(glFilter: GlFilter){
        GPUMp4Composer(FileUtils.getPathVideoResize(), FileUtils.getPathVideoFilter())
            .size(1080, 1080)
            .filter(glFilter)
            .listener(object : GPUMp4Composer.Listener {
                override fun onFailed(exception: Exception?) {
                    Log.d("PETSGRAM", exception.toString())
                    loadingDialog.dismiss()
                }

                override fun onProgress(progress: Double) {
                    Log.d("PETSGRAM", progress.toString())
                }

                override fun onCanceled() {
                    Log.d("PETSGRAM", "CANCELED")
                    loadingDialog.dismiss()
                }

                override fun onCompleted() {
                    Log.d("PETSGRAM", "DONEEEEEEEEEEEEEEEEEEEEEE")
                    if (stickerView.mStickers.size > 0) {
                        val videoModel = LocalResourceModel(data = FileUtils.getPathVideoFilter(), duration = mVideo.duration)
                        mFFmpeg.addEmoji(videoModel, stickerView)
                    } else if (mMusicPicked != null) {
                        mOutputPath = FileUtils.getOutputFile()
                        mFFmpeg.mergeAudio(FileUtils.getPathVideoFilter(), mMusicPicked!!.data!!, mOutputPath)
                    } else {
                        mOutputPath = FileUtils.getOutputFile()
                        FileUtils.copyFile(File(FileUtils.getPathVideoFilter()), File(mOutputPath))
                        loadingDialog.dismiss()
                        goShare()
                    }
                }
            })
            .start()
    }

    private fun switchPager(page: Int) {
        vp_editVideo.setCurrentItem(page, true)
        when (page) {
            0 -> {
                highlight_1.visibility = View.VISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.INVISIBLE
            }

            1 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.VISIBLE
                highlight_3.visibility = View.INVISIBLE
            }

            2 -> {
                highlight_1.visibility = View.INVISIBLE
                highlight_2.visibility = View.INVISIBLE
                highlight_3.visibility = View.VISIBLE
            }
        }
    }
}