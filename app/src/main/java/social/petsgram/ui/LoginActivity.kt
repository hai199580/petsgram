package social.petsgram.ui

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.parse.ParseInstallation
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_login.*
import social.petsgram.R
import social.petsgram.base.BaseActivity
import social.petsgram.dialog.AlertDisplayer
import social.petsgram.dialog.IAlertDisplayer
import social.petsgram.utils.ToastManager
import social.petsgram.view.LoadingDialog

class LoginActivity : BaseActivity(), View.OnClickListener {
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var alertDisplayer: AlertDisplayer

    override fun getFragmentViewId(): Int = 0

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun initView() {
        if (ParseUser.getCurrentUser() != null){
            startActivity(Intent(baseContext, HomeActivity::class.java))
        }

        alertDisplayer = AlertDisplayer()
    }

    override fun initPresenter() {

    }

    override fun initListener() {
        btn_signIn.setOnClickListener(this)
        btn_signUp.setOnClickListener(this)
        tvForgotPassword.setOnClickListener(this)
    }

    override fun initData() {

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_signIn -> {
                if (validate()) {
                    loginWithUsernameAndPass(edt_userName.text.toString(), edt_password.text.toString())
                }
            }

            R.id.btn_signUp -> {
                startActivityForResult(Intent(this, SignUpActivity::class.java), 1)
            }

            R.id.tvForgotPassword -> {
                startActivity(Intent(this,ForgotPasswordActivity::class.java))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            edt_userName.setText(data.getStringExtra("username"))
            edt_password.setText(data.getStringExtra("password"))
        }
    }

    private fun validate(): Boolean {
        if (TextUtils.isEmpty(edt_userName.text)) {
            ToastManager.showLong(R.string.username_empty, applicationContext)
            return false
        }
        if (TextUtils.isEmpty(edt_password.text)) {
            ToastManager.showLong(R.string.password_empty, applicationContext)
            return false
        }
        return true
    }

    private val alertDisplayerListener = object : IAlertDisplayer {
        override fun onOkClicked() {
            applicationContext.startActivity(Intent(baseContext, HomeActivity::class.java))
        }
    }

    private lateinit var currentInstallation: ParseInstallation
    private fun loginWithUsernameAndPass(userName: String, password: String) {
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, getString(R.string.missing_field), Toast.LENGTH_LONG).show()
            return
        }
        loadingDialog = LoadingDialog(this)
        loadingDialog.show()
        ParseUser.logInInBackground(userName, password) { user, e ->
            if (user != null) {
                if (user.getBoolean("emailVerified")) {
                    alertDisplayer.show("Login Sucessful",
                            "Welcome, " + user.username + "!",
                            this,
                            alertDisplayerListener)

                    currentInstallation = ParseInstallation.getCurrentInstallation()
                    currentInstallation.put("user", user)
                    currentInstallation.saveInBackground()
                } else {
                    ParseUser.logOut()
                    alertDisplayer.show("Login Fail",
                            "Please Verify Your Email first",
                            this,
                            null)
                }

                loadingDialog.dismiss()
            } else {
                Toast.makeText(this@LoginActivity, getString(R.string.wrong), Toast.LENGTH_LONG).show()
                loadingDialog.dismiss()
            }
        }
    }
}
