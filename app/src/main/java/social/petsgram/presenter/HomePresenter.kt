package social.petsgram.presenter

import android.content.Context
import com.parse.*
import social.petsgram.model.PostModel
import social.petsgram.ui.fragment.home.IHomeFragment

class HomePresenter(private val mContext: Context) {
    private lateinit var mCallback: IHomeFragment
    private val mListPost: ArrayList<PostModel> by lazy {
        ArrayList<PostModel>()
    }

    fun setPresenter(callback: IHomeFragment) {
        mCallback = callback
    }

    fun loadData() {
        val query = ParseQuery<ParseObject>("Post")
        query.setLimit(15).orderByDescending("createdAt").findInBackground(object : FindCallback<ParseObject> {
            override fun done(posts: MutableList<ParseObject>, e: ParseException?) {
                if (e == null) {
                    mListPost.clear()
                    posts.forEachIndexed { index, post ->
                        val postModel = PostModel(post)
                        mListPost.add(postModel)
                        isLike(index)
                        isBookmarked(index)
                    }

                    mCallback.onDataLoaded(mListPost)
                }
            }
        })
    }

    fun updateLikeStatus(position: Int) {
        if (mListPost[position].isLiked()) {
            val parseLike = ParseObject("Like")
            parseLike.put("post", mListPost[position].getParseObject())
            parseLike.put("user", ParseUser.getCurrentUser())
            parseLike.put("postOwnerId", mListPost[position].getPostUser()!!.objectId)
            parseLike.saveInBackground(object : SaveCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {
                        mListPost[position].getParseObject().fetchIfNeededInBackground<ParseObject>()
                        mCallback.onUpdateLikeSuccess(position)
                    } else {
                        mCallback.onUpdateLikeFailure(position)
                        //Failure
                    }
                }
            })
        } else {
            val query = ParseQuery<ParseObject>("Like")
            query.whereEqualTo("user", ParseUser.getCurrentUser())
                    .whereEqualTo("post", mListPost[position].getParseObject())
                    .getFirstInBackground { _object, e ->
                        if (e == null) {
                            _object.deleteInBackground(object : DeleteCallback {
                                override fun done(e: ParseException?) {
                                    if (e == null) {
                                        mCallback.onUpdateLikeSuccess(position)
                                    } else {
                                        mCallback.onUpdateLikeFailure(position)
                                        //Failure
                                    }
                                }
                            })
                        } else {
                            mCallback.onUpdateLikeFailure(position)
                            //Failure
                        }
                    }
        }
    }

    fun updateBookmarkStatus(position: Int) {
        if (mListPost[position].isBookmarked()) {
            val parseLike = ParseObject("Bookmark")
            parseLike.put("post", mListPost[position].getParseObject())
            parseLike.put("user", ParseUser.getCurrentUser())
            parseLike.saveInBackground(object : SaveCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {
                        mListPost[position].getParseObject().fetchIfNeededInBackground<ParseObject>()
                        mCallback.onUpdateBookmarkSuccess(position)
                    } else {
                        mCallback.onUpdateBookmarkFailure(position)
                        //Failure
                    }
                }
            })
        } else {
            val query = ParseQuery<ParseObject>("Bookmark")
            query.whereEqualTo("user", ParseUser.getCurrentUser())
                    .whereEqualTo("post", mListPost[position].getParseObject())
                    .getFirstInBackground { _object, e ->
                        if (e == null) {
                            _object.deleteInBackground(object : DeleteCallback {
                                override fun done(e: ParseException?) {
                                    if (e == null) {
                                        mCallback.onUpdateBookmarkSuccess(position)
                                    } else {
                                        mCallback.onUpdateBookmarkFailure(position)
                                        //Failure
                                    }
                                }
                            })
                        } else {
                            mCallback.onUpdateBookmarkFailure(position)
                            //Failure
                        }
                    }
        }
    }

    private fun isLike(position: Int) {
        val query = ParseQuery<ParseObject>("Like")
        query.whereEqualTo("user", ParseUser.getCurrentUser())
                .whereEqualTo("post", mListPost[position].getParseObject())
                .countInBackground { count, e ->
                    if (e == null) {
                        if (count > 0) {
                            mListPost[position].setIsLiked(true)
                            mCallback.onUpdateLikeSuccess(position)
                        }
                    }
                }
    }

    private fun isBookmarked(position: Int) {
        val query = ParseQuery<ParseObject>("Bookmark")
        query.whereEqualTo("user", ParseUser.getCurrentUser())
                .whereEqualTo("post", mListPost[position].getParseObject())
                .countInBackground { count, e ->
                    if (e == null) {
                        if (count > 0) {
                            mListPost[position].setIsBookmarked(true)
                            mCallback.onUpdateBookmarkSuccess(position)
                        }
                    }
                }
    }

    fun getPostByPosition(position: Int): PostModel {
        return mListPost[position]
    }
}