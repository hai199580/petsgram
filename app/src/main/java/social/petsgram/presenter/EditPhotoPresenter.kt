package social.petsgram.presenter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import jp.co.cyberagent.android.gpuimage.filter.*
import social.petsgram.ui.IEditPhoto
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class EditPhotoPresenter(private val mContext: Context) {
    private lateinit var mCallback: IEditPhoto
    private var mFilterGroup: GPUImageFilterGroup = GPUImageFilterGroup()
    private var mFilterList: LinkedList<GPUImageFilter> = LinkedList()
    fun setListener(callback: IEditPhoto) {
        mCallback = callback
    }

    init {
        mFilterList.add(FilterType.FILTER.value, GPUImageBrightnessFilter())
        mFilterList.add(FilterType.BRIGHTNESS.value, GPUImageBrightnessFilter())
        mFilterList.add(FilterType.CONTRAST.value, GPUImageBrightnessFilter())
        mFilterList.add(FilterType.WARMTH.value, GPUImageBrightnessFilter())
        mFilterList.add(FilterType.SATURATION.value, GPUImageBrightnessFilter())
        mFilterList.add(FilterType.VIBRANCE.value, GPUImageBrightnessFilter())
    }

    fun changeFilter(filter: GPUImageFilter) {
        mFilterList[FilterType.FILTER.value] = filter

        requestRender()
    }

    fun saveImage(bmpBack: Bitmap, bmpFrame: Bitmap? = null, bmpSticker: Bitmap? = null) {
        SaveImageTask().execute(bmpBack,bmpFrame,bmpSticker)
    }

    private fun saveImageInBackgound(bmpBack: Bitmap, bmpFrame: Bitmap? = null, bmpSticker: Bitmap? = null) : String {
        val result = Bitmap.createBitmap(bmpBack.width, bmpBack.height, bmpBack.config)
        val canvas = Canvas(result)
        canvas.drawBitmap(bmpBack, 0f, 0f, null)

        if (bmpSticker != null){
            val bmSticker = resizeBitmap(bmpSticker, bmpBack.width, bmpBack.height)
            canvas.drawBitmap(bmSticker, 0f, 0f, null)
        }

        if (bmpFrame != null){
            val bmFrame = resizeBitmap(bmpFrame, bmpBack.width, bmpBack.height)
            canvas.drawBitmap(bmFrame, 0F, 0F, null)
        }

        val savedPhoto = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                "MEOW_" + System.currentTimeMillis() + ".jpg")
        try {
            val outputStream = FileOutputStream(savedPhoto.path)
            result.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            outputStream.flush()
            outputStream.close()

            val intent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            intent.data = Uri.fromFile(savedPhoto)
            mContext.sendBroadcast(intent)

            return savedPhoto.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
            return savedPhoto.absolutePath
        }
    }

    inner class SaveImageTask : AsyncTask<Bitmap, Unit, String>() {
        override fun doInBackground(vararg bitmaps: Bitmap?): String {
            return saveImageInBackgound(bitmaps[0]!!,bitmaps[1],bitmaps[2])
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            mCallback.onImageSaved(result)
        }
    }

    fun changeOption(value: Float, type: FilterType) {
        var filter = GPUImageFilter()
        when (type) {
            FilterType.BRIGHTNESS -> {
                filter = GPUImageBrightnessFilter(value)
            }

            FilterType.CONTRAST -> {
                filter = GPUImageContrastFilter(value)
            }

            FilterType.WARMTH -> {
                val warmFilter = GPUImageWhiteBalanceFilter()
                warmFilter.setTemperature(range(value.toInt(), 2000.0f, 10000.0f))
                mFilterList[type.value] = warmFilter
                requestRender()
                return
            }

            FilterType.SATURATION -> {
                filter = GPUImageSaturationFilter(value)
            }

            FilterType.VIBRANCE -> {
                filter = GPUImageVibranceFilter(value)
            }
            else -> {
            }
        }

        mFilterList[type.value] = filter
        requestRender()
    }

    private fun requestRender() {
        mFilterGroup = GPUImageFilterGroup(mFilterList)
        mCallback.renderFilter(mFilterGroup)
    }

    private fun range(percentage: Int, start: Float, end: Float): Float {
        return (end - start) * percentage / 100.0f + start
    }

    private fun range(percentage: Int, start: Int, end: Int): Int {
        return (end - start) * percentage / 100 + start
    }

    private fun resizeBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap {
        val scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)

        val scaleX = newWidth.toFloat() / bitmap.width
        val scaleY = newHeight.toFloat() / bitmap.height
        val pivotX = 0f
        val pivotY = 0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY)

        val canvas = Canvas(scaledBitmap)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bitmap, 0F, 0F, Paint(Paint.FILTER_BITMAP_FLAG))

        return scaledBitmap
    }
}

enum class FilterType(val value: Int) {
    FILTER(0), BRIGHTNESS(1), CONTRAST(2), WARMTH(3), SATURATION(4), VIBRANCE(5)
}