package social.petsgram.presenter

import android.content.Context
import android.graphics.drawable.Drawable
import social.petsgram.ui.fragment.IStickerFragment
import social.petsgram.utils.FileUtils

class StickerPresenter(private val mContext : Context) {
    private lateinit var mCallback : IStickerFragment

    fun setPresenter(callback : IStickerFragment){
        mCallback = callback
    }

    fun loadData(){
        val listDrawable = ArrayList<Drawable>()
        val listFrame = FileUtils.getListSticker()
        listFrame.forEach {
            listDrawable.add(Drawable.createFromPath(it))
        }

        mCallback.onDataLoaded(listDrawable)

//        val query : ParseQuery<ParseObject> = ParseQuery("Sticker")
//        query.findInBackground(object : FindCallback<ParseObject> {
//            override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
//                if (e == null){
//
//                    objects.forEach {
//                        val file = it.getParseFile("sticker")
//                        val byteArray = file!!.data
//                        val image = BitmapDrawable(mContext.resources, BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size))
//                        listDrawable.add(image)
//                    }
//
//                    mCallback.onDataLoaded(listDrawable)
//                }
//            }
//        })
    }
}