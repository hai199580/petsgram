package social.petsgram.presenter

import android.content.Context
import com.parse.ParseObject
import com.parse.ParseQuery
import com.parse.ParseUser
import social.petsgram.model.PostModel
import social.petsgram.ui.fragment.home.IMeFragment

class MePresenter(val mContext: Context, val mCallback: IMeFragment) {
    private var isFollowed = false

    private val mListPost: ArrayList<PostModel> by lazy {
        ArrayList<PostModel>()
    }

    fun loadData(account: ParseUser) {
        val query = ParseQuery<ParseObject>("Post")
        query.whereEqualTo("user", account).orderByAscending("createAt").findInBackground { posts, e ->
            if (e == null) {
                mListPost.clear()
                for (post in posts) {
                    val postModel = PostModel(post)
                    mListPost.add(postModel)
                }
                mCallback.onDataLoaded(mListPost)
            }
        }
    }

    fun getPostByPosition(position: Int): PostModel {
        return mListPost[position]
    }
}