package social.petsgram.presenter

import android.content.Context
import android.graphics.drawable.Drawable
import social.petsgram.ui.fragment.IFrameFragment
import social.petsgram.utils.FileUtils

class FramePresenter(private val mContext : Context) {
    private lateinit var mCallback : IFrameFragment

    fun setPresenter(callback : IFrameFragment){
        mCallback = callback
    }

    fun loadData(){
        val listDrawable = ArrayList<Drawable>()
        val listFrame = FileUtils.getListFrame()
        listFrame.forEach {
            listDrawable.add(Drawable.createFromPath(it))
        }

        mCallback.onDataLoaded(listDrawable)

//        val query : ParseQuery<ParseObject> = ParseQuery("Frame")
//        query.findInBackground(object : FindCallback<ParseObject> {
//            override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
//                if (e == null){
//                    val listDrawable = ArrayList<Drawable>()
//                    objects.forEach {
//                        val file = it.getParseFile("frame")
//                        val byteArray = file!!.data
//                        val image = BitmapDrawable(mContext.resources, BitmapFactory.decodeFile())
//                        listDrawable.add(image)
//                    }
//
//
//                }
//            }
//        })
    }
}