package social.petsgram.presenter

import android.content.Context
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.ui.fragment.IVideoFragment
import social.petsgram.utils.LocalGetVideoTask

class VideoPresenter(private val mContext: Context) : LocalGetVideoTask.CallBack {
    private lateinit var mCallback: IVideoFragment
    private lateinit var mList: List<LocalResourceModel>
    private lateinit var mImageSelected : LocalResourceModel

    fun setListener(callback: IVideoFragment) {
        mCallback = callback
    }

    fun loadData() {
        LocalGetVideoTask(this).execute(mContext)
    }

    override fun onDataLoaded(result: List<LocalResourceModel>) {
        mList = result
        mCallback.onDataLoaded(result)
    }

    override fun onError(errorMessage: String) {
        mCallback.onLoadDataError(errorMessage)
    }

    fun setImageSelected(position : Int){
        mImageSelected = mList[position]
    }

    fun getImageSelected(): LocalResourceModel {
        return mImageSelected
    }
}