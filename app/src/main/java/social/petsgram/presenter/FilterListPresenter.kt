package social.petsgram.presenter

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import id.zelory.compressor.Compressor
import jp.co.cyberagent.android.gpuimage.GPUImage
import social.petsgram.adapter.ItemFilterAdapter
import social.petsgram.model.MyFilterModel
import social.petsgram.ui.fragment.IFilterListFragment
import social.petsgram.utils.GPUImageFilterTools
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class FilterListPresenter(private val mContext : Context?) {
    private lateinit var mCallback : IFilterListFragment
    private lateinit var mListFilter : List<GPUImageFilterTools.CustomFilter>
    private lateinit var mListData : ArrayList<MyFilterModel>
    private lateinit var mAdapter : ItemFilterAdapter

    fun setListener(callback : IFilterListFragment){
        mCallback = callback
    }

    fun initData(adapter: ItemFilterAdapter){
        mListFilter = GPUImageFilterTools.getListFilter()
        mListData = ArrayList()
        mAdapter = adapter

        for (filter in mListFilter) {
            val gpuImage = GPUImage(mContext)
            gpuImage.setImage(compressedImage)
            gpuImage.setFilter(GPUImageFilterTools.createFilterForType(mContext!!,filter.filterType))
            val resultBitmap = gpuImage.bitmapWithFilterApplied

            mListData.add(MyFilterModel(filter.name, resultBitmap))
        }

        mAdapter.initData(mListData)
    }

    fun applyFilter(position : Int){
        mCallback.onFilterClicked(mListFilter[position].filterType)
    }

    private lateinit var compressedImage : Bitmap
    fun compressBitmap(bitmap : Bitmap){
        val imageName = "image"
        val pathRoot = File(mContext?.filesDir, "meowvideo/image")
        if (!pathRoot.exists()) pathRoot.mkdirs()
        val imageFile = File(pathRoot, imageName)
        val fOut: FileOutputStream
        try {
            fOut = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, fOut)
            try {
                fOut.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        compressedImage = Compressor(mContext)
                .setMaxWidth(128)
                .setMaxHeight(128)
                .setQuality(40)
                .setCompressFormat(Bitmap.CompressFormat.PNG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).absolutePath)
                .compressToBitmap(imageFile)
    }
}