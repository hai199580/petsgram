package social.petsgram.presenter

import android.content.Context
import android.graphics.drawable.Drawable
import social.petsgram.model.event.EmojiEvent
import social.petsgram.ui.fragment.IEmojiFragment
import social.petsgram.utils.FileUtils

class EmojiPresenter(private val mContext : Context) {
    private lateinit var mCallback : IEmojiFragment

    fun setPresenter(callback : IEmojiFragment){
        mCallback = callback
    }

    fun loadData(){
        val listDrawable = ArrayList<EmojiEvent>()
        val listFrame = FileUtils.getListEmoji()
        listFrame.forEach {
            listDrawable.add(EmojiEvent(Drawable.createFromPath(it),path = it))
        }

        mCallback.onDataLoaded(listDrawable)

//        val query : ParseQuery<ParseObject> = ParseQuery("VideoSticker")
//        query.findInBackground(object : FindCallback<ParseObject> {
//            override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
//                if (e == null){
//                    val listDrawable = ArrayList<EmojiEvent>()
//                    objects.forEach {
//                        val file = it.getParseFile("sticker")
//                        val byteArray = file!!.data
//                        val image = BitmapDrawable(mContext.resources, BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size))
//                        listDrawable.add(EmojiEvent(image,it.objectId,file.file.absolutePath))
//                    }
//
//                    mCallback.onDataLoaded(listDrawable)
//                }
//            }
//        })
    }
}