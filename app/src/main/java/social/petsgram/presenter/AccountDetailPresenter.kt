package social.petsgram.presenter

import android.content.Context
import com.parse.*
import social.petsgram.model.PostModel
import social.petsgram.ui.fragment.home.IAccountDetail

class AccountDetailPresenter(val mContext: Context, val mCallback: IAccountDetail) {
    private var isFollowed = false

    private val mListPost: ArrayList<PostModel> by lazy {
        ArrayList<PostModel>()
    }

    fun loadData(account: ParseUser) {
        val query = ParseQuery<ParseObject>("Post")
        query.whereEqualTo("user", account).orderByAscending("createAt").findInBackground { posts, e ->
            if (e == null) {
                mListPost.clear()
                for (post in posts) {
                    val postModel = PostModel(post)
                    mListPost.add(postModel)
                }
                mCallback.onDataLoaded(mListPost)
            }
        }
    }

    fun requestFollow(owner: ParseUser) {
        if (!isFollowed) {
            val parseLike = ParseObject("Follow")
            parseLike.put("owner", owner)
            parseLike.put("follower", ParseUser.getCurrentUser())
            parseLike.saveInBackground(object : SaveCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {
                        isFollowed = !isFollowed
                        mCallback.onUpdateFollow(isFollowed)
                    } else {
                        mCallback.onUpdateFollowFailed()
                        //Failure
                    }
                }
            })
        } else {
            val query = ParseQuery<ParseObject>("Follow")
            query.whereEqualTo("follower", ParseUser.getCurrentUser())
                    .whereEqualTo("owner", owner)
                    .getFirstInBackground { _object, e ->
                        if (e == null) {
                            _object.deleteInBackground(object : DeleteCallback {
                                override fun done(e: ParseException?) {
                                    if (e == null) {
                                        isFollowed = !isFollowed
                                        mCallback.onUpdateFollow(isFollowed)
                                    } else {
                                        mCallback.onUpdateFollowFailed()
                                        //Failure
                                    }
                                }
                            })
                        } else {
                            mCallback.onUpdateFollowFailed()
                            //Failure
                        }
                    }
        }
    }

    fun getFollowStatus(owner: ParseUser) {
        val query = ParseQuery<ParseObject>("Follow")
        query.whereEqualTo("owner", owner)
                .whereEqualTo("follower", ParseUser.getCurrentUser())
                .countInBackground { count, e ->
                    if (e == null) {
                        if (count > 0) {
                            isFollowed = true
                            mCallback.onUpdateFollow(isFollowed)
                        }
                    }
                }
    }

    fun getPostByPosition(position: Int): PostModel {
        return mListPost[position]
    }
}