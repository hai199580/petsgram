package social.petsgram.presenter

import android.content.Context
import social.petsgram.model.local.LocalResourceModel
import social.petsgram.ui.fragment.ILibraryFragment
import social.petsgram.utils.LocalGetImageTask

class LibraryPresenter(private val mContext: Context) : LocalGetImageTask.CallBack {
    private lateinit var mCallback: ILibraryFragment
    private lateinit var mList: List<LocalResourceModel>
    private lateinit var mImageSelected : LocalResourceModel

    fun setListener(callback: ILibraryFragment) {
        mCallback = callback
    }

    fun loadData() {
        LocalGetImageTask(this).execute(mContext)
    }

    override fun onDataLoaded(result: List<LocalResourceModel>) {
        mList = result
        mCallback.onDataLoaded(result)
    }

    override fun onError(errorMessage: String) {
        mCallback.onLoadDataError(errorMessage)
    }

    fun setImageSelected(position : Int){
        mImageSelected = mList[position]
    }

    fun getImageSelected(): String {
        return mImageSelected.data
    }
}