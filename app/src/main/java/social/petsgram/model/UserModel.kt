package social.petsgram.model

import com.parse.GetCallback
import com.parse.ParseException
import com.parse.ParseFile
import com.parse.ParseUser
import java.util.*

class UserModel() {
    private lateinit var mParseUser: ParseUser

    constructor(parseUser: ParseUser) : this() {
        mParseUser = parseUser
        mParseUser.fetchInBackground(object : GetCallback<ParseUser> {
            override fun done(user: ParseUser, e: ParseException?) {
                mParseUser = user
            }
        })
    }

    fun getParseUser() : ParseUser {
        return  mParseUser
    }

    fun getUserID(): String {
        return mParseUser.objectId
    }

    fun getUserName(): String {
        return mParseUser.username
    }

    fun getUserEmail(): String {
        return mParseUser.email
    }

    @Throws(ParseException::class)
    fun getUserBirthday(): Date? {
        return mParseUser.fetchIfNeeded().getDate("birthday")
    }

    @Throws(ParseException::class)
    fun getUserAvatar(): ParseFile? {
        return mParseUser.fetchIfNeeded().getParseFile("avatar")
    }

    @Throws(ParseException::class)
    fun getUserFullname() : String? {
        return mParseUser.fetchIfNeeded().getString("fullName")
    }

    @Throws(ParseException::class)
    fun getUserDescription() : String? {
        return mParseUser.fetchIfNeeded().getString("description")
    }

    @Throws(ParseException::class)
    fun getUserGender(): String? {
        return mParseUser.fetchIfNeeded().getString("gender")
    }

    fun getDateCreated(): Date {
        return mParseUser.createdAt
    }

    fun getDateUpdated(): Date {
        return mParseUser.updatedAt
    }
}