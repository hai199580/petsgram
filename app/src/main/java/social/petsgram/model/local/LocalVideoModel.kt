package social.petsgram.model.local

data class LocalVideoModel(
        var videoPath : String,
        var videoDescription : String = "",
        var videoDuration : Long,
        var isSelected : Boolean = false
)