package social.petsgram.model.local

import android.os.Parcel
import android.os.Parcelable

data class LocalResourceModel(var id: String = "",
                              var name: String = "",
                              var albumId: String = "",
                              var albumName: String = "",
                              var data: String = "",
                              var thumbnail: String = "",
                              var dateCreated: Long = 0L,
                              var timeCreated: Long = 0L,
                              var duration: Long = 0L,
                              var type: LocalResource = LocalResource.IMAGE,
                              var isSelected: Boolean = false) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readLong(),
            parcel.readLong(),
            type = if (parcel.readInt() == 0) LocalResource.IMAGE else LocalResource.VIDEO,
            isSelected = parcel.readInt() != 0
    )

    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeString(id)
        p0.writeString(name)
        p0.writeString(albumId)
        p0.writeString(albumName)
        p0.writeString(data)
        p0.writeString(thumbnail)
        p0.writeLong(dateCreated)
        p0.writeLong(timeCreated)
        p0.writeLong(duration)
        val typeAsInt = if (type == LocalResource.IMAGE) 0 else 1
        p0.writeInt(typeAsInt)
        p0.writeInt(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<LocalResourceModel> {
        override fun createFromParcel(parcel: Parcel): LocalResourceModel {
            return LocalResourceModel(parcel)
        }

        override fun newArray(size: Int): Array<LocalResourceModel?> {
            return arrayOfNulls(size)
        }
    }
}

enum class LocalResource {
    IMAGE, VIDEO
}