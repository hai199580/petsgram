package social.petsgram.model

import android.graphics.Bitmap

data class MyFilterModel(var name : String, var bitmap: Bitmap, var isSelected: Boolean = false)