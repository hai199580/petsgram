package social.petsgram.model.event

data class ContrastEvent(val contrast : Float)