package social.petsgram.model.event

data class SaturationEvent(val saturation : Float)