package social.petsgram.model.event

data class VibranceEvent(val vibrance : Float)