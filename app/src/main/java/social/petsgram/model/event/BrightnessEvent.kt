package social.petsgram.model.event

data class BrightnessEvent(val brightness : Float)