package social.petsgram.model.event

import android.graphics.drawable.Drawable

data class StickerEvent(val drawable: Drawable)

class RemoveStickerEvent()