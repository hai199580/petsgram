package social.petsgram.model.event

import android.graphics.drawable.Drawable

data class FrameEvent(val drawable: Drawable)

class RemoveFrameEvent()