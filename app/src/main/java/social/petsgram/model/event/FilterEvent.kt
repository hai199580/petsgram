package social.petsgram.model.event

import social.petsgram.utils.GPUImageFilterTools

data class FilterEvent(val filter:GPUImageFilterTools.FilterType)