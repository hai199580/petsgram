package social.petsgram.model.event

import social.petsgram.utils.VideoFilterType

data class VideoFIlterEvent(val filter : VideoFilterType)