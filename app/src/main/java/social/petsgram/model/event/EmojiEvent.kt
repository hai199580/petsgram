package social.petsgram.model.event

import android.graphics.drawable.Drawable

data class EmojiEvent(val drawable : Drawable, val name : String = "", val path : String)

class RemoveEmojiEvent()