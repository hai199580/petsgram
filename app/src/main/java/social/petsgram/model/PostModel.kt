package social.petsgram.model

import android.os.Parcel
import android.os.Parcelable
import com.parse.*
import java.util.*

class PostModel() : Parcelable {
    private lateinit var mParseObject : ParseObject
    private var isLiked : Boolean = false
    private var isBookmarked : Boolean = false
    private var mUser : ParseUser? = null

    constructor(parcel: Parcel) : this() {
        mParseObject = parcel.readParcelable(ParseObject::class.java.classLoader)
        isLiked = parcel.readByte() != 0.toByte()
        isBookmarked = parcel.readByte() != 0.toByte()
        mUser = parcel.readParcelable(ParseUser::class.java.classLoader)
    }


    fun isBookmarked() : Boolean {
        return isBookmarked
    }

    fun setIsBookmarked(isBookmarked : Boolean){
        this.isBookmarked = isBookmarked
    }

    fun isLiked() : Boolean {
        return isLiked
    }

    fun setIsLiked(isLike : Boolean){
        this.isLiked = isLike
    }

    constructor(parseObject: ParseObject) : this() {
        mParseObject = parseObject
    }

    fun getParseObject() : ParseObject {
        return mParseObject
    }

    fun getPostID() : String {
        return mParseObject.objectId
    }

    fun getPostUser() : ParseUser? {
        mUser = mParseObject.getParseUser("user")
        mUser?.fetchInBackground(object : GetCallback<ParseUser> {
            override fun done(user: ParseUser, e: ParseException?) {
                mUser = user
            }
        })

        return mUser
    }

    fun getPostCaption() : String? {
        return mParseObject.getString("caption")
    }

    fun getPostFile() : ParseFile? {
        return mParseObject.getParseFile("file")
    }

    fun getLikeCount() : Int? {
        return mParseObject.fetch<ParseObject>().getInt("likeCount")
    }

    fun getCommentCount() : Int? {
        return mParseObject.getInt("commentCount")
    }

    fun getPostType() : Int? {
        return mParseObject.getInt("type")
    }

    fun getDateCreated(): Date {
        return mParseObject.createdAt
    }

    fun getDateUpdated(): Date {
        return mParseObject.updatedAt
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(mParseObject, flags)
        parcel.writeByte(if (isLiked) 1 else 0)
        parcel.writeByte(if (isBookmarked) 1 else 0)
        parcel.writeParcelable(mUser, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostModel> {
        override fun createFromParcel(parcel: Parcel): PostModel {
            return PostModel(parcel)
        }

        override fun newArray(size: Int): Array<PostModel?> {
            return arrayOfNulls(size)
        }
    }
}