package social.petsgram

import android.app.Application
import com.parse.Parse
import com.parse.ParseObject
import social.petsgram.model.CommentModel
import social.petsgram.utils.FFmpegManager
import social.petsgram.utils.SettingManager

class MeowApplication : Application() {
    private lateinit var mFFmpegManager: FFmpegManager

    override fun onCreate() {
        super.onCreate()
        mFFmpegManager = FFmpegManager.getInstance(this)

        SettingManager.initialize(this)

        ParseObject.registerSubclass(CommentModel::class.java)
        Parse.initialize(Parse.Configuration.Builder(this)
                .applicationId("XLW8pbMwB7owyqAxXQnKWNqMhChIjcS0Z0WSl6Bv")
                // if defined
                .clientKey("cxNV2IXT78D63fovv22dQGa5kAmYgp7SJU1MGifR")
                .server("https://parseapi.back4app.com/")
                .enableLocalDataStore()
                .build()
        )
    }

    fun getFFmpegManager(): FFmpegManager {
        return mFFmpegManager
    }
}