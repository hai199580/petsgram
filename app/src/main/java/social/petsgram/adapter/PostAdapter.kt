package social.petsgram.adapter

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.item_post.view.*
import social.petsgram.R
import social.petsgram.model.PostModel
import social.petsgram.utils.Define
import java.io.File


interface Interator {
    fun onAccountClick(position: Int)
    fun onItemClick(position: Int)
    fun onLikeClick(position: Int)
    fun onShareClick(position: Int)
    fun onBookmarkClick(position: Int)
}

class PostAdapter(val mContext: Context, val mCallback: Interator) : RecyclerView.Adapter<PostAdapter._ViewHolder>() {
    private lateinit var mListPost: ArrayList<PostModel>

    init {
        mListPost = ArrayList()
    }

    fun loadData(listPost: ArrayList<PostModel>) {
        mListPost = listPost
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostAdapter._ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return _ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListPost.size
    }

    override fun onBindViewHolder(holder: _ViewHolder, position: Int) {
        holder.bindData(mListPost[position])
    }

    override fun onBindViewHolder(holder: _ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (!payloads.isEmpty()) {
            when (payloads[0]) {
                PayLoad.LIKE_CHANGE -> {
                    holder.bindLikeStatus(mListPost[position])
                    return
                }

                PayLoad.LIKE_SUCCESS -> {
                    holder.enableLike()
                    return
                }

                PayLoad.LIKE_FAILED -> {
                    holder.onLikeFailure(mListPost[position].isLiked())
                    return
                }

                PayLoad.BOOKMARK_CHANGE -> {
                    holder.bindBookmarkStatus(mListPost[position].isBookmarked())
                    return
                }

                PayLoad.BOOKMARK_SUCCESS -> {
                    holder.enableBookmark()
                    return
                }

                PayLoad.BOOKMARK_FAILED -> {
                    holder.onBookmarkFailure(mListPost[position].isBookmarked())
                    return
                }
            }
        }

        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemViewType(position: Int): Int {
        return if (mListPost[position].getPostType() == Define.TypePost.IMAGE) {
            Define.TypePost.IMAGE
        } else {
            Define.TypePost.VIDEO
        }
    }

    fun onLikeClicked(position: Int) {
        mListPost[position].setIsLiked(!mListPost[position].isLiked())
        notifyItemChanged(position, PayLoad.LIKE_CHANGE)
    }

    fun onUpdateLikeSuccess(position: Int) {
        notifyItemChanged(position, PayLoad.LIKE_SUCCESS)
    }

    fun onUpdateLikeFailure(position: Int) {
        mListPost[position].setIsLiked(!mListPost[position].isLiked())
        notifyItemChanged(position, PayLoad.LIKE_FAILED)
    }

    fun onBookmarkClicked(position: Int) {
        mListPost[position].setIsBookmarked(!mListPost[position].isBookmarked())
        notifyItemChanged(position, PayLoad.BOOKMARK_CHANGE)
    }

    fun onUpdateBookmarkSuccess(position: Int) {
        notifyItemChanged(position, PayLoad.BOOKMARK_SUCCESS)
    }

    fun onUpdateBookmarkFailure(position: Int) {
        mListPost[position].setIsBookmarked(!mListPost[position].isBookmarked())
        notifyItemChanged(position, PayLoad.BOOKMARK_FAILED)
    }

    inner class _ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.imgAvatar.setOnClickListener {
                mCallback.onAccountClick(adapterPosition)
            }

            itemView.tvName.setOnClickListener {
                mCallback.onAccountClick(adapterPosition)
            }

            itemView.imgSource.setOnClickListener {
                mCallback.onItemClick(adapterPosition)
            }

            itemView.btnLike.setOnClickListener {
                mCallback.onLikeClick(adapterPosition)
            }

            itemView.btnComment.setOnClickListener {
                mCallback.onItemClick(adapterPosition)
            }

            itemView.btnShare.setOnClickListener {
                mCallback.onShareClick(adapterPosition)
            }

            itemView.btnBookmark.setOnClickListener {
                mCallback.onBookmarkClick(adapterPosition)
            }
        }

        private val requestOption = RequestOptions().error(R.drawable.ic_sticker).override(80, 80).circleCrop()
        private val requestOption1 = RequestOptions().error(R.drawable.img_error).override(240, 240).placeholder(R.drawable.ic_sticker)
        fun bindData(postModel: PostModel) {
            with(itemView) {
                Glide.with(mContext)
                        .load(postModel.getPostUser()?.fetchIfNeeded()?.getParseFile("avatar")?.file)
                        .apply(requestOption)
                        .into(imgAvatar)

                tvName.text = postModel.getPostUser()?.username

                if (postModel.getPostType() == Define.TypePost.IMAGE) {
                    img_play.visibility = View.GONE
                } else {
                    img_play.visibility = View.VISIBLE
                }
                Glide.with(mContext)
                        .load(postModel.getPostFile()?.file)
                        .apply(requestOption1)
                        .into(imgSource)

                tvCaption.text = postModel.getPostCaption()
                if (postModel.getLikeCount()!! == 0) {
                    tvNumberLike.visibility = View.GONE
                } else if (postModel.getLikeCount()!! in 1..1) {
                    tvNumberLike.visibility = View.VISIBLE
                    tvNumberLike.text = "${postModel.getLikeCount()} like"
                } else {
                    tvNumberLike.visibility = View.VISIBLE
                    tvNumberLike.text = "${postModel.getLikeCount()} likes"
                }

                if (postModel.isLiked()) {
                    btnLike.setImageResource(R.drawable.ic_heart)
                } else {
                    btnLike.setImageResource(R.drawable.ic_heart_none)
                }

                if (postModel.isBookmarked()) {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark)
                } else {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_none)
                }
            }
        }

        fun enableLike() {
            itemView.btnLike.isEnabled = true
        }

        fun onLikeFailure(isLike: Boolean) {
            with(itemView) {
                if (isLike) {
                    btnLike.setImageResource(R.drawable.ic_heart)
                } else {
                    btnLike.setImageResource(R.drawable.ic_heart_none)
                }
                btnLike.isEnabled = true
            }
        }

        fun bindLikeStatus(postModel: PostModel) {
            with(itemView) {
                if (postModel.isLiked()) {
                    btnLike.setImageResource(R.drawable.ic_heart)
                } else {
                    btnLike.setImageResource(R.drawable.ic_heart_none)
                }
                btnLike.isEnabled = false
            }
        }

        fun enableBookmark() {
            itemView.btnBookmark.isEnabled = true
        }

        fun onBookmarkFailure(isBookmarked: Boolean) {
            with(itemView) {
                if (isBookmarked) {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark)
                } else {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_none)
                }
                btnBookmark.isEnabled = true
            }
        }

        fun bindBookmarkStatus(isBookmarked: Boolean) {
            with(itemView) {
                if (isBookmarked) {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark)
                } else {
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_none)
                }
                btnBookmark.isEnabled = false
            }
        }

        private fun compressBitmap(file: File): Bitmap {
            return Compressor(mContext)
                    .setMaxWidth(128)
                    .setMaxHeight(128)
                    .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToBitmap(file)
        }
    }
}

enum class PayLoad {
    LIKE_CHANGE,
    LIKE_SUCCESS,
    LIKE_FAILED,
    BOOKMARK_CHANGE,
    BOOKMARK_SUCCESS,
    BOOKMARK_FAILED
}