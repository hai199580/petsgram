package social.petsgram.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.grid_item.view.*
import social.petsgram.R
import social.petsgram.model.local.LocalResource
import social.petsgram.model.local.LocalResourceModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GridAdapter(val mContext: Context) : RecyclerView.Adapter<GridAdapter.ViewHolder>() {
    private lateinit var itemClickListener: IGridAdapter
    private lateinit var mList: List<LocalResourceModel>

    init {
        mList = ArrayList()
    }

    fun loadData(list : List<LocalResourceModel>){
        mList = list
        notifyDataSetChanged()
    }

    private var selectedItemPosition = -1
    private var previousItem = -1

    fun setListener(itemClickListener: IGridAdapter) {
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.grid_item, parent, false)
        return ViewHolder(v, this::onItemClicked)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun setItemSelected(position: Int) {
        if (selectedItemPosition != -1 && selectedItemPosition == previousItem)
            return

        if (position == selectedItemPosition) return
        previousItem = selectedItemPosition
        selectedItemPosition = position
        if (previousItem != -1) {
            mList[previousItem].isSelected = false
            notifyItemChanged(previousItem)
        }
        mList[selectedItemPosition].isSelected = true
        notifyItemChanged(selectedItemPosition)
    }

    private fun convertTimeFormat(millisecond: Long): String {
        val format = SimpleDateFormat("m.ss", Locale.US)
        return format.format(Date(millisecond))
    }

    private fun onItemClicked(position: Int) {
        itemClickListener.onItemClick(position)
    }

    inner class ViewHolder(itemView: View, callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }

        private val requestOption = RequestOptions().error(R.drawable.img_error).override(128, 128)
        fun bindData(model: LocalResourceModel) {
            with(itemView) {
                Glide.with(mContext)
                        .load(model.data)
                        .apply(requestOption)
                        .into(thumbnail)

                if (model.type == LocalResource.IMAGE) {
                    tv_duration.text = ""
                } else {
                    tv_duration.text = convertTimeFormat(model.duration)
                }

                if (model.isSelected) {
                    mask.visibility = View.VISIBLE
                } else {
                    mask.visibility = View.INVISIBLE
                }
            }
        }
    }

    interface IGridAdapter {
        fun onItemClick(position: Int)
    }
}
