package social.petsgram.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_music.view.*
import social.petsgram.R
import social.petsgram.model.event.MusicEvent

class MusicAdapter(private val mCallback : MusicAdapter.MusicAdapterListener, private val mContext : Context) : RecyclerView.Adapter<MusicAdapter.MusicHolder>() {
    private var mListMusic: List<MusicEvent>

    init {
        mListMusic = ArrayList()
    }

    fun loadData(list: List<MusicEvent>) {
        mListMusic = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_music, parent, false)
        return MusicHolder(view, this::onItemClicked)
    }

    private fun onItemClicked(position: Int) {
        mCallback.onItemClick(position)
    }

    override fun getItemCount(): Int {
        return mListMusic.size
    }

    override fun onBindViewHolder(holder: MusicHolder, position: Int) {
        holder.bindData(mListMusic[position])
    }

    private var selectedItemPosition = -1
    private var previousItem = -1

    fun setItemSelected(position: Int) {
        if (selectedItemPosition != -1 && selectedItemPosition == previousItem)
            return
        if (position == selectedItemPosition) {
            selectedItemPosition = -1
            previousItem = -1
            mCallback.onCancelMusic()
            return
        }

        previousItem = selectedItemPosition
        selectedItemPosition = position
        if (previousItem != -1) {
            mListMusic[previousItem].isSelected = false
            notifyItemChanged(previousItem)
        }
        mListMusic[selectedItemPosition].isSelected = true
        notifyItemChanged(selectedItemPosition)
    }


    inner class MusicHolder(itemView: View, private val callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }

        fun bindData(data : MusicEvent) {
            with(itemView) {
                tv_name.text = data.name

                if (data.isSelected){
                    tv_name.setBackgroundColor(ContextCompat.getColor(mContext,R.color.pink_pastel))
                } else {
                    tv_name.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white))
                }
            }
        }
    }

    interface MusicAdapterListener {
        fun onItemClick(position : Int)
        fun onCancelMusic()
    }
}