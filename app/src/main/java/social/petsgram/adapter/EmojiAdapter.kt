package social.petsgram.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_drawable.view.*
import social.petsgram.R
import social.petsgram.model.event.EmojiEvent

class EmojiAdapter(val mContext : Context, private val mCallback : EmojiAdapterListener) : RecyclerView.Adapter<EmojiAdapter.EmojiHolder>() {
    private var mListDrawable: List<EmojiEvent>

    init {
        mListDrawable = ArrayList()
    }

    fun loadData(list: List<EmojiEvent>) {
        mListDrawable = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmojiHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_drawable, parent, false)
        return EmojiHolder(view, this::onItemClicked)
    }

    private fun onItemClicked(position: Int) {
        mCallback.onItemClick(mListDrawable[position])
    }

    override fun getItemCount(): Int {
        return mListDrawable.size
    }

    override fun onBindViewHolder(holder: EmojiHolder, position: Int) {
        holder.bindData(mListDrawable[position].path)
    }

    inner class EmojiHolder(itemView: View, private val callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }
        private val requestOption = RequestOptions().error(R.drawable.img_error).override(480, 480)
        fun bindData(path: String) {
            with(itemView) {
                Glide.with(mContext)
                        .load(path)
                        .apply(requestOption)
                        .into(img_drawable)
            }
        }
    }

    interface EmojiAdapterListener {
        fun onItemClick(emoji : EmojiEvent)
    }
}