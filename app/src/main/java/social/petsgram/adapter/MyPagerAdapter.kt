package social.petsgram.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class MyPagerAdapter(private val mFragmentManager: FragmentManager, private val fragments: List<Fragment>)
    : FragmentPagerAdapter(mFragmentManager) {

    override fun getCount(): Int {
        return fragments.size
    }

    private var mPagerId : Int = 0
    fun setPagerId(id : Int){
        mPagerId = id
    }

    private fun getPagerId() : Int {
        return mPagerId
    }

    override fun getItem(position: Int): Fragment {
        // Check if this Fragment already exists.
        val name = makeFragmentName(mPagerId, position)
        var f: Fragment? = mFragmentManager.findFragmentByTag(name)
        if (f == null)
            f = fragments[position]
        return f
    }

    private fun makeFragmentName(viewId: Int, index: Int): String {
        return "android:switcher:$viewId:$index"
    }
}