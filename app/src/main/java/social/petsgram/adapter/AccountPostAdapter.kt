package social.petsgram.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_account_post.view.*
import social.petsgram.R
import social.petsgram.model.PostModel
import social.petsgram.utils.Define

class AccountPostAdapter(val mContext: Context, val mCallback : IAccountPostAdapter) : RecyclerView.Adapter<AccountPostAdapter.ViewHolder>() {
    private var mList: List<PostModel>

    init {
        mList = ArrayList()
    }

    fun loadData(list : List<PostModel>){
        mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_account_post, parent, false)
        return ViewHolder(v, this::onItemClicked)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    private fun onItemClicked(position: Int) {
        mCallback.onItemClick(position)
    }

    inner class ViewHolder(itemView: View, callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }

        private val requestOption = RequestOptions().error(R.drawable.img_error).override(128, 128)
        fun bindData(model: PostModel) {
            with(itemView) {
                Glide.with(mContext)
                        .load(model.getPostFile()?.file?.absolutePath)
                        .apply(requestOption)
                        .into(thumbnail)

                if (model.getPostType() == Define.TypePost.IMAGE) {
                    img_play.visibility = View.GONE
                } else {
                    img_play.visibility = View.VISIBLE
                }
            }
        }
    }

    interface IAccountPostAdapter {
        fun onItemClick(position: Int)
    }
}
