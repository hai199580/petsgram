package social.petsgram.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_video_filter.view.*
import social.petsgram.R
import social.petsgram.utils.VideoFilterType


class VideoFilterAdapter(val mList : Array<VideoFilterType>, val mCallback : Interator) : RecyclerView.Adapter<VideoFilterAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_video_filter,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(mList[position].name)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener{
                mCallback.onItemClick(adapterPosition)
            }
        }

        fun bindData(name : String){
            itemView.tv_item_video_filter.text = name
        }
    }

    interface Interator {
        fun onItemClick(position: Int)
    }
}
