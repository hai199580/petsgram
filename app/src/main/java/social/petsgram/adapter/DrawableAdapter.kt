package social.petsgram.adapter

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_drawable.view.*
import social.petsgram.R

class DrawableAdapter(private val mCallback : DrawableAdapterListener) : RecyclerView.Adapter<DrawableAdapter.DrawableHolder>() {
    private var mListDrawable: List<Drawable>

    init {
        mListDrawable = ArrayList()
    }

    fun loadData(list: List<Drawable>) {
        mListDrawable = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawableHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_drawable, parent, false)
        return DrawableHolder(view, this::onItemClicked)
    }

    private fun onItemClicked(position: Int) {
        mCallback.onItemClick(mListDrawable[position])
    }

    override fun getItemCount(): Int {
        return mListDrawable.size
    }

    override fun onBindViewHolder(holder: DrawableHolder, position: Int) {
        holder.bindData(mListDrawable[position])
    }

    inner class DrawableHolder(itemView: View, private val callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }

        fun bindData(drawable: Drawable) {
            with(itemView) {
                img_drawable.setImageDrawable(drawable)
            }
        }
    }

    interface DrawableAdapterListener {
        fun onItemClick(drawable: Drawable)
    }
}