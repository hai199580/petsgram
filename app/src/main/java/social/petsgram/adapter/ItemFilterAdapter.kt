package social.petsgram.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_image_preview.view.*
import social.petsgram.R
import social.petsgram.model.MyFilterModel

class ItemFilterAdapter() : RecyclerView.Adapter<ItemFilterAdapter.ItemFilterHolder>() {
    private lateinit var mCallback: ItemFilterListener
    private lateinit var mListFilter: List<MyFilterModel>
    private lateinit var mContext : Context


    constructor(context : Context,callback: ItemFilterListener) : this() {
        mCallback = callback
        mContext = context
        mListFilter = ArrayList()
    }

    fun initData(filters : List<MyFilterModel>){
        mListFilter = filters
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemFilterHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image_preview, parent, false)
        return ItemFilterHolder(view, this::onFilterClick)
    }

    override fun getItemCount(): Int {
        return mListFilter.count()
    }

    override fun onBindViewHolder(holder: ItemFilterHolder, position: Int) {
        holder.bindData(mListFilter[position])
    }

    private fun onFilterClick(position: Int) {
        mCallback.onItemClick(position)
    }

    inner class ItemFilterHolder(itemView: View, private val callback: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.invoke(adapterPosition)
            }
        }

        fun bindData(data: MyFilterModel) {
            with(itemView) {
                itemGPUImage.setImageBitmap(data.bitmap)
                tvFilterName.text = data.name

                if (data.isSelected) {
                    tvFilterName.setTextColor(ContextCompat.getColor(mContext,R.color.white))
                    tvFilterName.setBackgroundResource(R.drawable.bg_item_text_filter_selected)
                } else {
                    tvFilterName.setTextColor(ContextCompat.getColor(mContext,R.color.pink_pastel))
                    tvFilterName.setBackgroundResource(R.drawable.bg_item_text_filter)
                }
            }
        }
    }

    private var selectedItemPosition = -1
    private var previousItem = -1
    fun setItemSelected(position: Int) {
        if (selectedItemPosition != -1 && selectedItemPosition == previousItem)
            return

        if (position == selectedItemPosition) return
        previousItem = selectedItemPosition
        selectedItemPosition = position
        if (previousItem != -1) {
            mListFilter[previousItem].isSelected = false
            notifyItemChanged(previousItem)
        }
        mListFilter[selectedItemPosition].isSelected = true
        notifyItemChanged(selectedItemPosition)
    }

    interface ItemFilterListener {
        fun onItemClick(position: Int)
    }
}