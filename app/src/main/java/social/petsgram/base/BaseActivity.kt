package social.petsgram.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity


abstract class BaseActivity : AppCompatActivity(), BaseTransitionFragment {

    companion object {
        private const val MIN_BACK_STACK_ANIMATION = 4
        private const val MIN_NON_BACK_STACK_ANIMATION = 4
        const val KEY_TRANSITION_FRAGMENT = "KEY_TRANSITION_FRAGMENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initView()
        initPresenter()
        initListener()
        initData()
    }

    override fun onBackPressed() {
//        val size = supportFragmentManager.fragments.size
//        if (size > 0) {
//            val fragment = supportFragmentManager.fragments[0]
//            if (fragment is DriverFragment) {
//                val currentVisibilityFragment = (fragment as DriverFragment).getCurrentVisibilityFragment()
//                if (currentVisibilityFragment != null) {
//                    currentVisibilityFragment!!.popFragment()
//                }
//            } else if (fragment is CustomerFragment) {
//                val currentVisibilityFragment = (fragment as CustomerFragment).getCurrentVisibilityFragment()
//                if (currentVisibilityFragment != null) {
//                    currentVisibilityFragment!!.popFragment()
//                }
//            } else if (fragment is SaleFragment) {
//                val currentVisibilityFragment = (fragment as SaleFragment).getCurrentVisibilityFragment()
//                if (currentVisibilityFragment != null) {
//                    currentVisibilityFragment!!.popFragment()
//                }
//            }
//        }
    }

        override fun popBackStack() {
            if (supportFragmentManager.backStackEntryCount > 0) {
                supportFragmentManager.popBackStack()
            } else {
                finish()
            }
        }

        override fun pushFragment(fragment: Fragment, withAnimation: Boolean, animations: IntArray?, data: Bundle?) {
            showFragment(fragment, true, withAnimation, animations, data)
        }

        override fun replaceFragment(fragment: Fragment, withAnimation: Boolean, animations: IntArray?, data: Bundle?) {
            showFragment(fragment, false, withAnimation, animations, data)
        }


        private fun showFragment(fragment: Fragment, hasAddBackStack: Boolean?, withAnimation: Boolean?, animations: IntArray?, transferData: Bundle?) {
            if (transferData != null) {
                val bundle = Bundle()
                bundle.putAll(transferData)
                fragment.arguments = bundle
            }

            val transaction: FragmentTransaction

            if (hasAddBackStack!!) {
                transaction = supportFragmentManager.beginTransaction()
                        .add(getFragmentViewId(), fragment)
            } else {
                transaction = supportFragmentManager.beginTransaction()
                        .replace(getFragmentViewId(), fragment)
            }

            if (hasAddBackStack) {
                transaction.addToBackStack(null)
            }

            if (withAnimation!! && animations != null) {
                if (hasAddBackStack) {
                    if (animations.size >= MIN_BACK_STACK_ANIMATION)
                        transaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3])
                } else {
                    if (animations.size >= MIN_NON_BACK_STACK_ANIMATION)
                        transaction.setCustomAnimations(animations[0], animations[1])
                }
            }

            transaction.commit()
            fragmentManager.executePendingTransactions()
        }

        abstract fun getFragmentViewId(): Int

        abstract fun getLayoutId(): Int

        abstract fun initView()

        abstract fun initPresenter()

        abstract fun initListener()

        abstract fun initData()
    }


