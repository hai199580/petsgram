package social.petsgram.base

import android.os.Bundle
import android.support.v4.app.Fragment

interface BaseTransitionFragment {
    fun popBackStack()

    fun pushFragment(fragment: Fragment, withAnimation: Boolean = false, animations: IntArray? = null, data : Bundle? = null)

    fun replaceFragment(fragment: Fragment, withAnimation: Boolean = false, animations: IntArray? = null, data : Bundle? = null)
}