package social.petsgram.base

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import social.petsgram.R

abstract class BaseFragment : Fragment() {

    val baseActivity: BaseActivity? get() = activity as BaseActivity

    lateinit var toolbar: Toolbar
        private set

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        onAttachView()
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBaseView()
        initView()
        initPresenter()
        initData(arguments)
        initListener()
    }

    override fun onDestroyView() {
        onDetachView()
        super.onDestroyView()
    }

    fun popFragment() {
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
        } else {
            baseActivity?.finish()
        }
    }

    fun popAllFragment() {
        val fm = childFragmentManager
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            fm.popBackStack()
        }
    }

    fun replaceFragment(fragment: Fragment, withAnimation: Boolean?, animations: Array<Int>, transferData: Bundle) {
        showFragment(fragment, false, withAnimation, animations, transferData)
    }

    fun pushFragment(fragment: Fragment, withAnimation: Boolean? = null, animations: Array<Int>? = null, transferData: Bundle) {
        showFragment(fragment, true, withAnimation, animations, transferData)
    }

    private fun showFragment(fragment: Fragment, hasAddBackStack: Boolean, withAnimation: Boolean?, animations: Array<Int>?, transferData: Bundle?) {
        if (transferData != null) {
            val bundle = Bundle()
            bundle.putAll(transferData)
            fragment.arguments = bundle
        }

        val transaction: FragmentTransaction

        if (hasAddBackStack) {
            transaction = childFragmentManager.beginTransaction()
                    .add(getFragmentViewId(), fragment)
            transaction.addToBackStack(null)
        } else {
            transaction = childFragmentManager.beginTransaction()
                    .replace(getFragmentViewId(), fragment)
        }

        if (animations != null) {
            if (hasAddBackStack) {
                val MIN_BACK_STACK_ANIMATION = 4
                if (animations.size >= MIN_BACK_STACK_ANIMATION)
                    transaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3])
            } else {
                val MIN_NON_BACK_STACK_ANIMATION = 4
                if (animations.size >= MIN_NON_BACK_STACK_ANIMATION)
                    transaction.setCustomAnimations(animations[0], animations[1])
            }
        }

        transaction.commit()
    }

    fun getFragmentViewId(): Int {
        return R.id.flHomeContainer
    }

    private fun initBaseView() {

    }

    private fun setupToolbar(): Boolean {
        val title = getTitleScreen()
        when (title) {
            is String -> toolbar.title = title
            is Int -> toolbar.setTitle(title)
            null -> toolbar.title = ""
        }

        val icon = getNavigationIcon()
        if (icon != null) {
            toolbar.setNavigationIcon(icon)
        }

        val menu = getToolbarMenu()
        if (menu != null) {
            toolbar.inflateMenu(menu)
            toolbar.setOnMenuItemClickListener { menuItem ->
                onMenuItemClicked(menuItem)
                return@setOnMenuItemClickListener true
            }
        }

        toolbar.setNavigationOnClickListener { onNavigationIconClicked() }

        toolbar.setBackgroundColor(getToolbarBackground())
        return isShowToolbar()
    }

    fun isEnableBackPress(): Boolean? {
        return true
    }

    open fun isShowToolbar(): Boolean = true

    open fun onMenuItemClicked(item: MenuItem) {}

    open fun onNavigationIconClicked() {}

    open fun getTitleScreen(): Any? = null

    open fun getNavigationIcon(): Int? = null

    open fun getToolbarMenu(): Int? = null

    open fun getToolbarBackground(): Int = Color.WHITE

    abstract fun getLayoutId(): Int

    abstract fun initView()

    abstract fun initData(data: Bundle?)

    abstract fun initPresenter()

    abstract fun initListener()

    abstract fun onAttachView()

    abstract fun onDetachView()
}