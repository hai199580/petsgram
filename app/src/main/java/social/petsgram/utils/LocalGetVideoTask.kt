package social.petsgram.utils

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import social.petsgram.model.local.LocalResource
import social.petsgram.model.local.LocalResourceModel
import java.io.File

internal class LocalGetVideoTask(private val mCallback: LocalGetVideoTask.CallBack) : AsyncTask<Context, Unit, List<LocalResourceModel>>() {
    private var mErrorMessage: String? = null

    interface CallBack {
        fun onDataLoaded(result: List<LocalResourceModel>)
        fun onError(errorMessage: String)
    }

    override fun onPostExecute(result: List<LocalResourceModel>) {
        super.onPostExecute(result)
        when {
            mErrorMessage != null -> mCallback.onError(mErrorMessage.toString())
            else -> mCallback.onDataLoaded(result)
        }
    }

    override fun doInBackground(vararg params: Context): List<LocalResourceModel> {
        val listVideo: ArrayList<LocalResourceModel> = ArrayList()
        val context = params[0]
        val uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.Video.VideoColumns.DATA, MediaStore.Video.VideoColumns.DESCRIPTION, MediaStore.Video.VideoColumns.DATE_TAKEN, MediaStore.Video.VideoColumns.DURATION)
        val orderBy = MediaStore.Video.VideoColumns.DATE_TAKEN + " DESC"
        val c = context.contentResolver.query(uri, projection, null, null, orderBy)
        if (c != null) {
            while (c.moveToNext()) {
                val videoPath = c.getString(c.getColumnIndex(MediaStore.Video.VideoColumns.DATA))

                var duration = c.getLong(c.getColumnIndex(MediaStore.Video.VideoColumns.DURATION))

                if (duration == 0L) {
                    duration = getVideoDuration(context, Uri.fromFile(File(videoPath!!)))
                }

                if (!File(videoPath).exists()) {
                    continue
                }

                listVideo.add(LocalResourceModel(data = videoPath, duration = duration, type = LocalResource.VIDEO))
            }
            c.close()
        }
        return listVideo
    }


    private fun getVideoDuration(context: Context, uri: Uri?): Long {
        if (uri == null) {
            return 0
        }

        val duration: String?
        val retriever = MediaMetadataRetriever()
        try {
            retriever.setDataSource(context, uri)
        } catch (e: Exception) {
            return 0
        }

        duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        retriever.release()
        return if (duration == null) {
            0
        } else {
            try {
                duration.toLong()
            } catch (e: NumberFormatException) {
                0L
            }
        }
    }
}