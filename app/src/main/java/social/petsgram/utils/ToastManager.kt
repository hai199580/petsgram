package social.petsgram.utils

import android.content.Context
import android.widget.Toast

class ToastManager {
    companion object {
        fun showLong(text : String, context: Context){
            Toast.makeText(context,text,Toast.LENGTH_LONG).show()
        }

        fun showLong(text : Int, context: Context){
            Toast.makeText(context,text,Toast.LENGTH_LONG).show()
        }

        fun showShort(text : String, context: Context){
            Toast.makeText(context,text,Toast.LENGTH_SHORT).show()
        }

        fun showShort(text : Int, context: Context){
            Toast.makeText(context,text,Toast.LENGTH_SHORT).show()
        }
    }
}