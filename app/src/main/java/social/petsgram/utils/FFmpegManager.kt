package social.petsgram.utils

import android.content.Context
import android.util.Log
import com.cannon.photoprinter.coloreffect.sticker.views.StickerView
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import social.petsgram.model.local.LocalResourceModel

class FFmpegManager(private val mContext: Context) {
    val TAG_MERGE_AUDIO = "TAG_MERGE_AUDIO"
    val TAG_CUT_VIDEO = "TAG_CUT_VIDEO"
    val TAG_RESIZE_VIDEO = "TAG_RESIZE_VIDEO"
    val TAG_EMOJI_VIDEO = "TAG_EMOJI_VIDEO"

    companion object {
        fun getInstance(context: Context): FFmpegManager {
            return FFmpegManager(context)
        }
    }

    private lateinit var mFFmpeg: FFmpeg

    init {
        loadFFmpeg()
    }

    private fun loadFFmpeg() {
        mFFmpeg = FFmpeg.getInstance(mContext)

        try {
            mFFmpeg.loadBinary(object : FFmpegLoadBinaryResponseHandler {
                override fun onFinish() {}

                override fun onSuccess() {}

                override fun onFailure() {}

                override fun onStart() {}
            })
        } catch (e: FFmpegNotSupportedException) {
            e.printStackTrace()
        }
    }

    fun killRunningProcess() {
        mFFmpeg.killRunningProcesses()
    }

    fun addEmoji(inputVideo: LocalResourceModel, stickerView : StickerView) {
        val videoRecoding = ArrayList<String>()
        videoRecoding.add(Define.FFmpegCommand.OVERWRITE_FLAG)
        videoRecoding.add(Define.FFmpegCommand.INPUT_FILE_FLAG)
        videoRecoding.add(inputVideo.data)

        val strFilter = StringBuilder()
        strFilter.append("[0]scale=1080:1080[v02];")
        var strVideoMap = ""
        stickerView.mStickers.forEachIndexed { index, sticker ->
            videoRecoding.add("-ignore_loop")
            videoRecoding.add("0")
            videoRecoding.add(Define.FFmpegCommand.INPUT_FILE_FLAG)
            videoRecoding.add(sticker.stickerPath)

            var width = MatrixUtils.getXScaled(sticker.width.toFloat(), sticker.matrix)
            width *= (1080 / stickerView.width.toFloat())
            var height = MatrixUtils.getXScaled(sticker.height.toFloat(), sticker.matrix)
            height *= (1080 / stickerView.height.toFloat())
            var stickerX = MatrixUtils.getXTransform(1F, sticker.matrix)
            stickerX *= (1080 / stickerView.width.toFloat())
            var stickerY = MatrixUtils.getYTransform(1F, sticker.matrix)
            stickerY *= (1080 / stickerView.height.toFloat())

            val emIndex = index + 1
            val str: String
            if (index == stickerView.mStickers.size - 1) {
                str = "[$emIndex]scale=$width:$height[v${emIndex}1];[v${emIndex - 1}2][v${emIndex}1]overlay=x=$stickerX:y=$stickerY[v${emIndex}2]"
                strVideoMap = "[v${emIndex}2]"
            } else {
                str = "[$emIndex]scale=$width:$height[v${emIndex}1];[v${emIndex - 1}2][v${emIndex}1]overlay=x=$stickerX:y=$stickerY[v${emIndex}2];"
            }
            strFilter.append(str)
        }

        videoRecoding.add(Define.FFmpegCommand.STRICT_FLAG)
        videoRecoding.add(Define.FFmpegCommand.EXPERIMENTAL_FLAG)
        videoRecoding.add("-shortest")
        videoRecoding.add(Define.FFmpegCommand.FIlTER_COMPLEX_FLAG)
        videoRecoding.add(strFilter.toString())
        videoRecoding.add(Define.FFmpegCommand.VIDEO_MAP)
        videoRecoding.add(strVideoMap)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_TIME_PROCESS_FLAG)
        videoRecoding.add((inputVideo.duration.toFloat()/1000F).toString())
        videoRecoding.add(Define.FFmpegCommand.AUDIO_ENCODE_DISABLE_FLAG)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_FRAME_FLAG)
        videoRecoding.add(Define.FFmpegCommand.FRAME_RATE_DEFAULT)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_THREAD_FLAG)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_THREAD)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_V_CODEC_FLAG)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_V_CODEC_LIBX264)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_PRESET)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_PRESET_ULTRA)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_CRF_FLAG)
        videoRecoding.add(Define.FFmpegCommand.VIDEO_CRF)
        videoRecoding.add(FileUtils.getPathVideoEmoji())

        videoProcessing(videoRecoding.toTypedArray(), TAG_EMOJI_VIDEO)
    }

    fun mergeAudio(inputVideo: String, audioPath: String, outputPath : String) {
        val commands = arrayOf(
                Define.FFmpegCommand.OVERWRITE_FLAG,
                Define.FFmpegCommand.INPUT_FILE_FLAG, inputVideo,
                Define.FFmpegCommand.INPUT_FILE_FLAG, audioPath,
                Define.FFmpegCommand.VIDEO_MAP, "0:v:0",
                Define.FFmpegCommand.VIDEO_MAP, "1:a:0",
                "-c:v", "copy",
                "-ac", "2",
                "-c:a", "copy",
                Define.FFmpegCommand.STRICT_FLAG,
                Define.FFmpegCommand.EXPERIMENTAL_FLAG, "-shortest",
                Define.FFmpegCommand.VIDEO_PRESET,
                Define.FFmpegCommand.VIDEO_PRESET_ULTRA,
                outputPath)

        videoProcessing(commands, TAG_MERGE_AUDIO)
    }

    /**
     * @return scale x:y -> size of video output
     * force_original_aspect_ratio -> optional
     * pad x:y-> padding video left:right
     * color -> padding color
     * -preset ultrafast -> build mode
     */
    fun resizeVideoCommand(input: String) {
        val output = FileUtils.getPathVideoResize()
//        val commands = ("-y -i " + input + " -vf scale=" + "'min(ih,iw)'" + ":" + "'min(ih,iw)'"
//                + ",crop=" + "'min(ih,iw)'" + ":" + "'min(ih,iw)'" + " -vcodec libx264 -preset ultrafast -strict -2 -c:a copy " + output).split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val commands = ("-y -i " + input + " -vf " +
                "crop=" + "'min(ih,iw)'" + ":" + "'min(ih,iw)'" +
                " -vcodec libx264 -preset ultrafast -strict -2 -c:a copy " + output).split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        videoProcessing(commands, TAG_RESIZE_VIDEO)
    }

    fun cutVideoCommand(input: String, output: String, start: String, duration: String) {
        val commands = arrayOf("-y", "-i", input, "-ss", start, "-vcodec", "libx264", "-preset", "ultrafast", "-t", duration, output)
        videoProcessing(commands, TAG_CUT_VIDEO)
    }

    private lateinit var mCallback: FFmpegManagerListener

    fun setFFmpegListener(callback: FFmpegManagerListener) {
        mCallback = callback
    }

    private fun videoProcessing(commands: Array<String>, tag: String) {
        try {
            mFFmpeg.execute(commands, object : ExecuteBinaryResponseHandler() {
                override fun onStart() {
                    super.onStart()
                    mCallback.onStart(tag)
                }

                override fun onProgress(message: String) {
                    super.onProgress(message)
                    mCallback.onProgress(tag, message)
                    Log.d("PETSGRAM", "tag = $tag ---------- message = $message")
                }

                override fun onSuccess(message: String) {
                    super.onSuccess(message)
                    mCallback.onSuccess(tag, message)
                }

                override fun onFailure(message: String) {
                    super.onFailure(message)
                    mCallback.onFailure(tag, message)
                }

                override fun onFinish() {
                    super.onFinish()
                    mCallback.onFinished(tag)
                }
            })

        } catch (e: FFmpegCommandAlreadyRunningException) {
            e.printStackTrace()
        }
    }
}

interface FFmpegManagerListener {
    fun onStart(tag: String)
    fun onProgress(tag: String, message: String)
    fun onSuccess(tag: String, message: String)
    fun onFailure(tag: String, message: String)
    fun onFinished(tag: String)
}