package social.petsgram.utils

import android.content.Context
import android.os.AsyncTask
import android.provider.MediaStore
import android.text.TextUtils
import social.petsgram.model.local.LocalResourceModel
import java.io.File

internal class LocalGetImageTask(private val mCallback: LocalGetImageTask.CallBack) : AsyncTask<Context, Unit, List<LocalResourceModel>>() {

    private var mErrorMessage: String? = null

    interface CallBack {
        fun onDataLoaded(result: List<LocalResourceModel>)
        fun onError(errorMessage: String)
    }

    override fun onPostExecute(result: List<LocalResourceModel>) {
        super.onPostExecute(result)
        when {
            mErrorMessage != null -> mCallback.onError(mErrorMessage.toString())
            else -> mCallback.onDataLoaded(result)
        }
    }

    override fun doInBackground(vararg params: Context?) : List<LocalResourceModel> {
        val listImage : ArrayList<LocalResourceModel> = ArrayList()
        val context = params[0]
        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        val projection = arrayOf(MediaStore.MediaColumns._ID,
                MediaStore.MediaColumns.TITLE,
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.DATE_TAKEN)
        val orderBy = MediaStore.Images.Media.DATE_TAKEN + " DESC"
        val cursor = context?.contentResolver?.query(uri, projection, null, null, orderBy)
        if (cursor != null) {

            while (cursor.moveToNext()) {
                val socialId = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID))
                val name = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.TITLE))
                val albumId = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_ID))
                val albumName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
                val data = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                var thumbnail = data
                val thumbnailCursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(
                        context.contentResolver, socialId.toLong(), MediaStore.Images.Thumbnails.MINI_KIND, null)
                if (thumbnailCursor != null && thumbnailCursor.count > 0) {
                    thumbnailCursor.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA)
                    if (columnIndex > 0) {
                        val thumb = thumbnailCursor.getString(columnIndex)
                        if (!TextUtils.isEmpty(thumb) && File(thumb).canRead()) {
                            thumbnail = thumb
                        }
                    }
                    thumbnailCursor.close()
                }
                val createdTime = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN))

                val localImageModel = LocalResourceModel(socialId,name,albumId,albumName,data,thumbnail,
                        CalendarUtils.getDate(createdTime),createdTime)
                listImage.add(localImageModel)
            }
            cursor.close()
        }
        return listImage
    }
}