package social.petsgram.utils.permission;

public interface PermissionResultCallback {
    void PermissionGranted(int request_code);
    void PermissionDenied(int request_code);
    void NeverAskAgain(int request_code);
}
