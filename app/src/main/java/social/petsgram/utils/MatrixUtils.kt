package social.petsgram.utils

import android.graphics.Matrix


class MatrixUtils {
    companion object {
        private val INDEX_SCALE_X = 0
        private val INDEX_TRANSFORM_X = 2
        private val INDEX_SCALE_Y = 4
        private val INDEX_TRANSFORM_Y = 5

        fun getXScaled(w: Float, matrix: Matrix): Float {
            val arrF = floatArrayOf(0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F)
            matrix.getValues(arrF)
            return w * arrF[INDEX_SCALE_X]
        }

        fun getYScaled(h: Float, matrix: Matrix): Float {
            val arrF = floatArrayOf(0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F)
            matrix.getValues(arrF)
            return h * arrF[INDEX_SCALE_Y]
        }

        fun getXTransform(X: Float, matrix: Matrix): Float {
            val arrF = floatArrayOf(0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F)
            matrix.getValues(arrF)
            return X * arrF[INDEX_TRANSFORM_X]
        }

        fun getYTransform(Y: Float, matrix: Matrix): Float {
            val arrF = floatArrayOf(0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F)
            matrix.getValues(arrF)
            return Y * arrF[INDEX_TRANSFORM_Y]
        }
    }
}