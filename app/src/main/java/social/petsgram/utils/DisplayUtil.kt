package social.petsgram.utils

import android.content.Context

class DisplayUtil {
    companion object {
        fun dp2Px(context: Context, dp: Int): Int {
            val scale = context.resources.displayMetrics.density
            return (dp * scale + 0.5f).toInt()
        }

        fun px2Dp(context: Context, px: Int): Int {
            val scale = context.resources.displayMetrics.density
            return (px / scale + 0.5f).toInt()
        }

        fun sp2px(context: Context, spValue: Int): Int {
            val fontScale = context.resources.displayMetrics.scaledDensity
            return (spValue * fontScale + 0.5f).toInt()
        }
    }
}