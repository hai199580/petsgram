package social.petsgram.utils

import com.parse.FindCallback
import com.parse.ParseException
import com.parse.ParseObject
import com.parse.ParseQuery
import java.io.File

class CopyFileTask(val mType : AssetType) : Thread() {
    override fun run() {
        when (mType) {
            AssetType.EMOJI -> {
                val query: ParseQuery<ParseObject> = ParseQuery("VideoSticker")
                query.findInBackground(object : FindCallback<ParseObject> {
                    override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
                        if (e == null) {
                            objects.forEach {
                                val file = it.getParseFile("sticker")
                                FileUtils.copyFile(file!!.file, File(FileUtils.getFolderEmoji()))
                            }

                            SettingManager.getInstance().saveIsCopyEmoji(true)
                        }
                    }
                })
            }

            AssetType.FRAME -> {
                val query: ParseQuery<ParseObject> = ParseQuery("Frame")
                query.findInBackground(object : FindCallback<ParseObject> {
                    override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
                        if (e == null) {
                            objects.forEach {
                                val file = it.getParseFile("frame")
                                FileUtils.copyFile(file!!.file, File(FileUtils.getFolderFrame()))
                            }

                            SettingManager.getInstance().saveIsCopyFrame(true)
                        }
                    }
                })
            }

            AssetType.STICKER -> {
                val query: ParseQuery<ParseObject> = ParseQuery("Sticker")
                query.findInBackground(object : FindCallback<ParseObject> {
                    override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
                        if (e == null) {
                            objects.forEach {
                                val file = it.getParseFile("sticker")
                                FileUtils.copyFile(file!!.file, File(FileUtils.getFolderSticker()))
                            }

                            SettingManager.getInstance().saveIsCopySticker(true)
                        }
                    }
                })
            }

            AssetType.MUSIC -> {
                val query: ParseQuery<ParseObject> = ParseQuery("VideoMusic")
                query.findInBackground(object : FindCallback<ParseObject> {
                    override fun done(objects: MutableList<ParseObject>, e: ParseException?) {
                        if (e == null) {
                            objects.forEach {
                                val file = it.getParseFile("music")
                                FileUtils.copyFile(file!!.file, File(FileUtils.getFolderMusic()))
                            }

                            SettingManager.getInstance().saveIsCopyMusic(true)
                        }
                    }
                })
            }
        }
    }
}

enum class AssetType {
    EMOJI, FRAME, STICKER, MUSIC
}