package social.petsgram.utils

import android.content.Context

class SettingManager {


    companion object {
        private val PREF_KEY_FRAME = "frame"
        private val PREF_KEY_STICKER = "sticker"
        private val PREF_KEY_MUSIC = "music"
        private val PREF_KEY_EMOJI = "emoji"
        private var instance: SettingManager? = null

        fun initialize(context: Context) {
            if (instance == null) {
                instance = SettingManager(context)
            } else {
                throw IllegalStateException("Something wrong")
            }
        }

        fun getInstance(): SettingManager {
            if (instance == null) {
                throw IllegalStateException("You have to initialize it before")
            }
            return instance as SettingManager
        }
    }

    private var mPreferences: SharedPreferenceUtils

    constructor(context: Context) {
        SharedPreferenceUtils.initialize(context, Context.MODE_PRIVATE)
        mPreferences = SharedPreferenceUtils.getInstance()
    }

    fun saveIsCopyFrame(boolean: Boolean) {
        mPreferences.saveBoolean(PREF_KEY_FRAME, boolean)
    }

    fun saveIsCopySticker(boolean: Boolean) {
        mPreferences.saveBoolean(PREF_KEY_STICKER, boolean)
    }

    fun saveIsCopyEmoji(boolean: Boolean) {
        mPreferences.saveBoolean(PREF_KEY_EMOJI, boolean)
    }

    fun saveIsCopyMusic(boolean: Boolean) {
        mPreferences.saveBoolean(PREF_KEY_MUSIC, boolean)
    }

    fun getIsCopyFrame(): Boolean {
        return mPreferences.getBoolean(PREF_KEY_FRAME, false)
    }

    fun getIsCopySticker(): Boolean {
        return mPreferences.getBoolean(PREF_KEY_STICKER, false)
    }

    fun getIsCopyEmoji(): Boolean {
        return mPreferences.getBoolean(PREF_KEY_EMOJI, false)
    }

    fun getIsCopyMusic(): Boolean {
        return mPreferences.getBoolean(PREF_KEY_MUSIC, false)
    }
}