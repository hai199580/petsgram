package social.petsgram.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPreferenceUtils {
    companion object {
        private var sInstance: SharedPreferenceUtils? = null
        private var sAppSharedPreferences: SharedPreferences? = null

        fun initialize(context: Context, mode: Int) {
            if (sAppSharedPreferences == null) {
                val name = context.applicationContext.packageName
                sAppSharedPreferences = context.applicationContext
                        .getSharedPreferences(name, mode)
            }
        }

        fun getInstance(): SharedPreferenceUtils {
            if (sInstance == null) {
                sInstance = SharedPreferenceUtils()
            }
            return sInstance as SharedPreferenceUtils
        }
    }

    /**
     * Retrieve a string value from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue.
     * Throws ClassCastException if there is a preference with this name
     * that is not a String.
     */
    fun getString(key: String, defValue: String?): String? {
        return sAppSharedPreferences!!.getString(key, defValue)
    }

    /**
     * Retrieve a integer value from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue.
     * Throws ClassCastException if there is a preference with this name
     * that is not an int.
     */
    fun getInt(key: String, defValue: Int): Int {
        return sAppSharedPreferences!!.getInt(key, defValue)
    }

    /**
     * Retrieve a float value from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue.
     * Throws ClassCastException if there is a preference with this name
     * that is not a float.
     */
    fun getFloat(key: String, defValue: Float): Float {
        return sAppSharedPreferences!!.getFloat(key, defValue)
    }

    /**
     * Retrieve a long value from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue.
     * Throws ClassCastException if there is a preference with this name
     * that is not a long.
     */
    fun getLong(key: String, defValue: Long): Long {
        return sAppSharedPreferences!!.getLong(key, defValue)
    }

    /**
     * Retrieve a boolean value from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference value if it exists, or defValue.
     * Throws ClassCastException if there is a preference with this name
     * that is not a boolean.
     */
    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return sAppSharedPreferences!!.getBoolean(key, defValue)
    }

    /**
     * Retrieve a set of String values from the preferences.
     *
     * @param key      The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return Returns the preference values if they exist, or defValues.
     * Throws ClassCastException if there is a preference with this name
     * that is not a Set.
     */
    fun getStringSet(key: String, defValue: Set<String>): Set<String>? {
        return sAppSharedPreferences!!.getStringSet(key, defValue)
    }

    /**
     * Set a String value in the preferences editor, to be written back once commit() or apply() are called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value String: The new value for the preference.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveString(key: String, value: String): Boolean {
        return sAppSharedPreferences!!.edit().putString(key, value).commit()
    }

    /**
     * Set an int value in the preferences editor, to be written back once commit() or apply() are called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value int: The new value for the preference.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveInt(key: String, value: Int): Boolean {
        return sAppSharedPreferences!!.edit().putInt(key, value).commit()
    }

    /**
     * Set a float value in the preferences editor, to be written back once commit() or apply() are called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value float: The new value for the preference.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveFloat(key: String, value: Float): Boolean {
        return sAppSharedPreferences!!.edit().putFloat(key, value).commit()
    }

    /**
     * Set a long value in the preferences editor, to be written back once commit() or apply() are called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value long: The new value for the preference.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveLong(key: String, value: Long): Boolean {
        return sAppSharedPreferences!!.edit().putLong(key, value).commit()
    }

    /**
     * Set a boolean value in the preferences editor, to be written back once commit() or apply() are called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value boolean: The new value for the preference.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveBoolean(key: String, value: Boolean): Boolean {
        return sAppSharedPreferences!!.edit().putBoolean(key, value).commit()
    }

    /**
     * Set a set of String values in the preferences editor, to be written back once commit() or apply() is called.
     *
     * @param key   String: The name of the preference to modify.
     * @param value Set: The set of new values for the preference. Passing null for this argument is equivalent to
     * calling remove(String) with this KEY.
     * @return Returns true if the new values were successfully written to persistent storage.
     */
    fun saveStringSet(key: String, value: Set<String>): Boolean {
        return sAppSharedPreferences!!.edit().putStringSet(key, value).commit()
    }

    /**
     * Clear all data in app sharedPreferences
     *
     * @return Return true if clear data successfully
     */
    fun clear(): Boolean {
        return sAppSharedPreferences!!.edit().clear().commit()
    }

    /**
     * Mark in the editor that a preference value should be removed, which
     * will be done in the actual preferences once commit is
     * called.
     *
     *
     *
     * Note that when committing back to the preferences, all removals
     * are done first, regardless of whether you called remove before
     * or after put methods on this editor.
     *
     * @param key The name of the preference to remove.
     * @return Returns a reference to the same Editor object, so you can
     * chain put calls together.
     */
    fun remove(key: String): Boolean {
        return sAppSharedPreferences!!.edit().remove(key).commit()
    }
}