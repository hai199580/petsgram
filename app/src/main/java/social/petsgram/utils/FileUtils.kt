package social.petsgram.utils

import android.os.Environment
import android.util.Log
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FileUtils {
    companion object {
        val FOLDER_ASSETS = ".assetsData"
        val FOLDER_APP = "Petsgram"
        val FOLDER_VIDEO = "Video"
        val FOLDER_VIDEO_PROCESS = ".video"
        val FORDER_ASSETS_STICKER = ".sticker"
        val FORDER_ASSETS_MUSIC = ".music"
        val FORDER_ASSETS_FRAME = ".frame"
        val FORDER_ASSETS_EMOJI = ".emoji"

        val VIDEO_CUT_NAME = "VD_cut.mp4"
        val VIDEO_RESIZED_NAME = "VD_resize.mp4"
        val VIDEO_FILTER_NAME = "VD_filter.mp4"
        val VIDEO_EMOJI_NAME = "VD_emoji.mp4"


        fun downloadSticker(id: String, sticker: ByteArray) {
            val stickerPath = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_STICKER
            val dir = File(stickerPath)
            if (!dir.exists()) {
                dir.mkdirs()
            }

            val stickerFile = File(stickerPath, "$id.gif")

            try {
                val outputStream = FileOutputStream(stickerFile.path)
                outputStream.write(sticker)
                outputStream.flush()
                outputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        fun downloadMusic(id: String, music: File) {
            val musicPath = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_MUSIC
            val dir = File(musicPath)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            val stickerFile = File(musicPath, "$id.mp3")

            try {
                val outputStream = FileOutputStream(stickerFile.path)
                outputStream.write(music.readBytes())
                outputStream.flush()
                outputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
//            stickerFile.createNewFile()
//            try {
//                copyAssets(music, stickerFile)
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
        }

        fun getFolderVideoProcess():String{
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FOLDER_VIDEO_PROCESS

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()

            return result
        }

        fun getPathVideoCapture() : String {
            val folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath +
                    "/" + FileUtils.FOLDER_VIDEO

            val dir = File(folder)
            if (!dir.exists()) dir.mkdirs()

            val result = File(folder, VIDEO_CUT_NAME).absolutePath
            return result
        }

        fun getPathVideoCut() : String {
            val folder = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FOLDER_VIDEO_PROCESS

            val dir = File(folder)
            if (!dir.exists()) dir.mkdirs()

            val result = File(folder, VIDEO_CUT_NAME).absolutePath
            return result
        }

        fun getPathVideoResize() : String {
            val folder = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FOLDER_VIDEO_PROCESS

            val dir = File(folder)
            if (!dir.exists()) dir.mkdirs()

            val result = File(folder, VIDEO_RESIZED_NAME).absolutePath
            return result
        }

        fun getPathVideoFilter() : String {
            val folder = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FOLDER_VIDEO_PROCESS

            val dir = File(folder)
            if (!dir.exists()) dir.mkdirs()

            val result = File(folder, VIDEO_FILTER_NAME).absolutePath
            return result
        }

        fun getPathVideoEmoji() : String {
            val folder = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FOLDER_VIDEO_PROCESS

            val dir = File(folder)
            if (!dir.exists()) dir.mkdirs()

            val result = File(folder, VIDEO_EMOJI_NAME).absolutePath
            return result
        }

        fun getFolderEmoji():String{
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_EMOJI

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val outputFile = File(result, "PETS_EMOJI_$timeStamp.gif").absolutePath

            return outputFile
        }

        fun getFolderFrame():String{
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_FRAME

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val outputFile = File(result, "PETS_FRAME_$timeStamp.png").absolutePath

            return outputFile
        }

        fun getFolderSticker():String{
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_STICKER

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val outputFile = File(result, "PETS_STICKER_$timeStamp.png").absolutePath

            return outputFile
        }

        fun getFolderMusic():String{
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_MUSIC

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val outputFile = File(result, "PETS_MUSIC_$timeStamp.mp3").absolutePath

            return outputFile
        }

        fun getListMusic() : ArrayList<String> {
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_MUSIC

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()
            val files = dir.listFiles()

            val output = ArrayList<String>()

            files.forEach {
                output.add(it.absolutePath)
            }
            return output
        }

        fun getListEmoji() : ArrayList<String> {
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_EMOJI

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()
            val files = dir.listFiles()

            val output = ArrayList<String>()

            files.forEach {
                output.add(it.absolutePath)
            }
            return output
        }

        fun getListSticker() : ArrayList<String> {
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_STICKER

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()
            val files = dir.listFiles()

            val output = ArrayList<String>()

            files.forEach {
                output.add(it.absolutePath)
            }
            return output
        }

        fun getListFrame() : ArrayList<String> {
            val result = Environment.getExternalStorageDirectory().path + "/" +
                    FOLDER_APP + "/" + FOLDER_ASSETS + "/" + FORDER_ASSETS_FRAME

            val dir = File(result)
            if (!dir.exists()) dir.mkdirs()
            val files = dir.listFiles()

            val output = ArrayList<String>()

            files.forEach {
                output.add(it.absolutePath)
            }
            return output
        }

        fun getOutputFile() : String {
            val videoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath + "/" + FileUtils.FOLDER_VIDEO
            val dir = File(videoPath)
            if (!dir.exists()) {
                dir.mkdirs()
            }
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val outputFile = File(videoPath, "PETS_VID_$timeStamp.mp4").absolutePath
            return outputFile
        }

        fun copyFile(source: File, des: File) {
            var inn: InputStream? = null
            var out: OutputStream? = null
            try {
                inn = FileInputStream(source)
                out = FileOutputStream(des)
                copyFile(inn!!, out)
                inn!!.close()
                inn = null
                out.flush()
                out.close()
                out = null
            } catch (e: IOException) {
                Log.e("tag", "Failed to copy asset file: ${source.name}", e)
            }
        }

        private fun copyFile(inn: InputStream, out: OutputStream) {
            val buffer = ByteArray(1024)
            var read = inn.read(buffer)
            while (read  != -1) {
                out.write(buffer, 0, read)
                read = inn.read(buffer)
            }
        }
    }
}