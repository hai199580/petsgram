package social.petsgram.utils

class Define {
    class FFmpegCommand {
        companion object {
            /**
             * commands use input processing video
             */
            val OVERWRITE_FLAG = "-y"
            val INPUT_FILE_FLAG = "-i"
            val KEY_CURRENT = "agree_term_condition";

            // ffmpeg commands
            val STRICT_FLAG = "-strict"
            val EXPERIMENTAL_FLAG_2 = "-2"
            val EXPERIMENTAL_FLAG = "experimental"
            /**
             * commands use filter graph
             */
            val FIlTER_COMPLEX_FLAG = "-filter_complex"
            val VIDEO_FILTER_FLAG = "-vf"
            val FILTER_FLAG = "-f"
            /**
             * commands encoder video and audio
             */
            val VIDEO_V_CODEC_FLAG = "-codec:v"
            val VIDEO_V_CODEC_LIBX264 = "libx264"
            val VIDEO_A_CODEC_FLAG = "-acodec"
            val VIDEO_A_CODEC = "aac"
            val COPY_FLAG = "-c"
            val COPY = "copy"
            val AUDIO_ENCODE_DISABLE_FLAG = "-an"
            /**
             * commands use set output video audio and video when processing finish
             */
            val VIDEO_PRESET = "-preset"
            val VIDEO_PRESET_ULTRA = "ultrafast"
            val VIDEO_SIZE_FLAG = "-s"
            val FRAME_RATE_FLAG = "-framerate"
            val FRAME_RATE_DEFAULT = "24"
            val VIDEO_FRAME_FLAG = "-r"
            val TUNE_FLAG = "-tune"
            val FAST_DECODE = "fastdecode"
            val ZERO_LATENCY = "zerolatency"
            val VIDEO_CRF_FLAG = "-crf"
            val VIDEO_CRF = "25"
            val VIDEO_TIME_PROCESS_FLAG = "-t"
            val VIDEO_FIX_FMT_FLAG = "-pix_fmt"
            val VIDEO_FIX_FMT = "yuv420p"
            /**
             * commands flag use set offset start load input
             */
            val ITS_OFFSET_FLAG = "-itsoffset"
            val IST_OFFSET = "5"
            /**
             * Commands set thread use processing
             */
            val VIDEO_THREAD_FLAG = "-threads"
            val VIDEO_THREAD = "2"
            /**
             * Commands flag loop video
             */
            val VIDEO_LOOP_FLAG = "-loop"
            val VIDEO_LOOP = "1"
            val VIDEO_MAP = "-map"
            /**
             * Commands merge video
             */
            val CONCAT_FLAG = "concat"
            val SAFE_FLAG = "-safe"
            val SAFE = "0"
        }
    }

    class KeyItent {
        companion object {
            val KEY_IMAGE = "KEY_IMAGE"
            val KEY_VIDEO = "KEY_VIDEO"
            val KEY_POST = "KEY_POST"
            val KEY_USER = "KEY_USER"
        }
    }

    class TypePost {
        companion object {
            val IMAGE = 0
            val VIDEO = 1
        }
    }
}