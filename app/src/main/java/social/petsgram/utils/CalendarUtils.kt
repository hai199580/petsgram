package social.petsgram.utils

import java.text.SimpleDateFormat
import java.util.*

object CalendarUtils {

    private val EEE_DAY_OF_WEEK_FORMAT = SimpleDateFormat("EEE", Locale.getDefault())

    fun getDayOfWeekThreeChars(date: Date, locale: Locale): String {
        return EEE_DAY_OF_WEEK_FORMAT.format(date).toUpperCase(locale)
    }

    fun rewindToBeginningOfMonth(date: Date): Date {
        val cal = buildFromDate(rewindToBeginningOfDay(date))
        cal.set(Calendar.DAY_OF_MONTH, 1)
        return cal.time
    }

    private fun rewindToBeginningOfDay(date: Date): Date {
        val cal = buildFromDate(date)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun getTomorrow(from: Date): Date {
        val c = buildFromDate(from)
        c.add(Calendar.DATE, 1)  // number of days to add
        return c.time
    }

    fun getYesterday(from: Date): Date {
        val c = buildFromDate(from)
        c.add(Calendar.DATE, -1)  // number of days to add
        return c.time
    }

    private fun buildFromDate(from: Date): Calendar {
        val c = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault())
        c.time = from
        return c
    }

    fun getDate(from: Long): Long {
        val c = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault())
        c.timeInMillis = from
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)
        return c.timeInMillis
    }
}