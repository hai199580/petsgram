package social.petsgram.utils

import android.content.Context
import com.daasuu.gpuv.egl.filter.*


enum class VideoFilterType {
    DEFAULT,
    BILATERAL_BLUR,
    BOX_BLUR,
    BRIGHTNESS,
    BULGE_DISTORTION,
    CGA_COLORSPACE,
    CONTRAST,
    CROSSHATCH,
    EXPOSURE,
    FILTER_GROUP_SAMPLE,
    GAMMA,
    GAUSSIAN_FILTER,
    GRAY_SCALE,
    HALFTONE,
    HAZE,
    HIGHLIGHT_SHADOW,
    HUE,
    INVERT,
    LOOK_UP_TABLE_SAMPLE,
    LUMINANCE,
    LUMINANCE_THRESHOLD,
    MONOCHROME,
    OPACITY,
    OVERLAY,
    PIXELATION,
    POSTERIZE,
    RGB,
    SATURATION,
    SEPIA,
    SHARP,
    SOLARIZE,
    SPHERE_REFRACTION,
    SWIRL,
    TONE_CURVE_SAMPLE,
    TONE,
    VIBRANCE,
    VIGNETTE,
    WATERMARK,
    WEAK_PIXEL,
    WHITE_BALANCE,
    ZOOM_BLUR,
    BITMAP_OVERLAY_SAMPLE;


    companion object {


        fun createFilterList(): Array<VideoFilterType> {
            return VideoFilterType.values()
        }

        fun createGlFilter(filterType: VideoFilterType, context: Context): GlFilter {
            when (filterType) {
                DEFAULT -> return GlFilter()
                BILATERAL_BLUR -> return GlBilateralFilter()
                BOX_BLUR -> return GlBoxBlurFilter()
                BRIGHTNESS -> {
                    val glBrightnessFilter = GlBrightnessFilter()
                    glBrightnessFilter.setBrightness(0.2f)
                    return glBrightnessFilter
                }
                BULGE_DISTORTION -> return GlBulgeDistortionFilter()
                CGA_COLORSPACE -> return GlCGAColorspaceFilter()
                CONTRAST -> {
                    val glContrastFilter = GlContrastFilter()
                    glContrastFilter.setContrast(2.5f)
                    return glContrastFilter
                }
                CROSSHATCH -> return GlCrosshatchFilter()
                EXPOSURE -> return GlExposureFilter()
                FILTER_GROUP_SAMPLE -> return GlFilterGroup(GlSepiaFilter(), GlVignetteFilter())
                GAMMA -> {
                    val glGammaFilter = GlGammaFilter()
                    glGammaFilter.setGamma(2f)
                    return glGammaFilter
                }
                GAUSSIAN_FILTER -> return GlGaussianBlurFilter()
                GRAY_SCALE -> return GlGrayScaleFilter()
                HALFTONE -> return GlHalftoneFilter()
                HAZE -> {
                    val glHazeFilter = GlHazeFilter()
                    glHazeFilter.slope = -0.5f
                    return glHazeFilter
                }
                HIGHLIGHT_SHADOW -> return GlHighlightShadowFilter()
                HUE -> return GlHueFilter()
                INVERT -> return GlInvertFilter()
                LUMINANCE -> return GlLuminanceFilter()
                LUMINANCE_THRESHOLD -> return GlLuminanceThresholdFilter()
                MONOCHROME -> return GlMonochromeFilter()
                OPACITY -> return GlOpacityFilter()
                PIXELATION -> return GlPixelationFilter()
                POSTERIZE -> return GlPosterizeFilter()
                RGB -> {
                    val glRGBFilter = GlRGBFilter()
                    glRGBFilter.setRed(0f)
                    return glRGBFilter
                }
                SATURATION -> return GlSaturationFilter()
                SEPIA -> return GlSepiaFilter()
                SHARP -> {
                    val glSharpenFilter = GlSharpenFilter()
                    glSharpenFilter.sharpness = 4f
                    return glSharpenFilter
                }
                SOLARIZE -> return GlSolarizeFilter()
                SPHERE_REFRACTION -> return GlSphereRefractionFilter()
                SWIRL -> return GlSwirlFilter()
                TONE -> return GlToneFilter()
                VIBRANCE -> {
                    val glVibranceFilter = GlVibranceFilter()
                    glVibranceFilter.setVibrance(3f)
                    return glVibranceFilter
                }
                VIGNETTE -> return GlVignetteFilter()
                WEAK_PIXEL -> return GlWeakPixelInclusionFilter()
                WHITE_BALANCE -> {
                    val glWhiteBalanceFilter = GlWhiteBalanceFilter()
                    glWhiteBalanceFilter.setTemperature(2400f)
                    glWhiteBalanceFilter.setTint(2f)
                    return glWhiteBalanceFilter
                }
                ZOOM_BLUR -> return GlZoomBlurFilter()
                else -> return GlFilter()
            }
        }
    }
}