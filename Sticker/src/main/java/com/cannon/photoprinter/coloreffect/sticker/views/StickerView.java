package com.cannon.photoprinter.coloreffect.sticker.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import com.cannon.photoprinter.coloreffect.sticker.icons.Sticker;
import com.cannon.photoprinter.coloreffect.sticker.icons.TextSticker;
import com.cannon.photoprinter.coloreffect.sticker.icons.ZoomStickerIcon;
import com.cannon.photoprinter.coloreffect.sticker.utils.StickerUtil;

/**
 * Sticker View
 */
public class StickerView extends BaseGroupStickerView {

    /**
     * Action mode which handle event from User.
     */
    public enum ActionMode {
        NONE,
        DRAG,
        ZOOM_WITH_TWO_FINGER,
        ICON,
        CLICK
    }

    public static final int FLIP_HORIZONTALLY = 1;

    public static final int FLIP_VERTICALLY = 1 << 1;

    private static final int DEFAULT_MIN_CLICK_DELAY_TIME = 200;

    private final Matrix downMatrix = new Matrix();

    private final Matrix moveMatrix = new Matrix();

    private final PointF currentCenterPoint = new PointF();

    private PointF mMiddlePoint = new PointF();

    private final int touchSlop;

    private float downX;

    private float downY;

    private float oldDistance = 0f;

    private float oldRotation = 0f;

    private ActionMode currentMode = ActionMode.NONE;

    private boolean constrained;

    private long lastClickTime = 0;

    private int minClickDelayTime = DEFAULT_MIN_CLICK_DELAY_TIME;

    /**
     * Constructor.
     *
     * @param context
     */
    public StickerView(Context context) {
        this(context, null);
    }

    /**
     * Constructor.
     *
     * @param context
     * @param attrs
     */
    public StickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Constructor.
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public StickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isIsLockIcon()) return super.onInterceptTouchEvent(ev);

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = ev.getX();
                downY = ev.getY();

                return findCurrentIconTouched(downX, downY) != null || findHandlingSticker(downX, downY) != null;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isIsLockIcon()) {
            return super.onTouchEvent(event);
        }

        int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                boolean isHandled = handleTouchDownEvent(event);
                return isHandled;
            case MotionEvent.ACTION_MOVE:
                handleMoveEvent(event);
                invalidate();
                return true;
            case MotionEvent.ACTION_UP:
                handleTouchUpEvent(event);
                return true;
            case MotionEvent.ACTION_POINTER_DOWN:
                handlePointerDownEvent(event);
                return true;
            case MotionEvent.ACTION_POINTER_UP:
                handlePointerUpEvent(event);
                return true;
            default:
                return true;
        }
    }

    private void handlePointerUpEvent(MotionEvent event) {
        if (currentMode == ActionMode.ZOOM_WITH_TWO_FINGER && mCurrentSticker != null) {
            if (mStickerOperationListener != null) {
                mStickerOperationListener.onStickerZoomFinished(mCurrentSticker);
            }
        }
        currentMode = ActionMode.NONE;
    }

    private void handlePointerDownEvent(MotionEvent event) {
        oldDistance = StickerUtil.calculateDistanceBetweenTwoFingers(event);
        oldRotation = StickerUtil.calculateRotationBetweenTwoFingers(event);

        mMiddlePoint = calculateMiddlePointInTwoFingers(event);

        if (mCurrentSticker != null && isInStickerArea(mCurrentSticker, event.getX(1),
                event.getY(1)) && findCurrentIconTouched(downX, downY) == null) {
            currentMode = ActionMode.ZOOM_WITH_TWO_FINGER;
        }
    }

    private boolean handleTouchDownEvent(MotionEvent event) {
        currentMode = ActionMode.DRAG;

        downX = event.getX();
        downY = event.getY();

        mMiddlePoint = calculateMiddlePointInSticker();
        oldDistance = StickerUtil.calculateDistanceBetweenTwoPoint(mMiddlePoint.x, mMiddlePoint.y, downX, downY);
        oldRotation = StickerUtil.calculateRotationBetweenTwoPoints(mMiddlePoint.x, mMiddlePoint.y, downX, downY);

        mCurrentIcon = findCurrentIconTouched(downX, downY);
        if (mCurrentIcon != null) {
            currentMode = ActionMode.ICON;
            mCurrentIcon.accept(event);
        } else {
            mCurrentSticker = findHandlingSticker(downX, downY);
        }

        if (mCurrentSticker != null) {
            downMatrix.set(mCurrentSticker.getMatrix());
            if (mBringCurrentStickerToFront) {
                mStickers.remove(mCurrentSticker);
                mStickers.add(mCurrentSticker);
            }
        }

        invalidate();
        if (mCurrentIcon == null && mCurrentSticker == null) {
            return false;
        }
        return true;
    }

    private void handleTouchUpEvent(MotionEvent event) {
        long currentTime = SystemClock.uptimeMillis();

        if (currentMode == ActionMode.ICON && mCurrentIcon != null && mCurrentSticker != null) {
            mCurrentIcon.accept(event);
        }

        if (currentMode == ActionMode.DRAG
                && Math.abs(event.getX() - downX) < touchSlop
                && Math.abs(event.getY() - downY) < touchSlop
                && mCurrentSticker != null) {
            currentMode = ActionMode.CLICK;
            if (mStickerOperationListener != null) {
                mStickerOperationListener.onStickerClicked(mCurrentSticker);
            }
            if (currentTime - lastClickTime < minClickDelayTime) {
                if (mStickerOperationListener != null) {
                    mStickerOperationListener.onStickerDoubleTapped(mCurrentSticker);
                }
            }
        }

        if (currentMode == ActionMode.DRAG && mCurrentSticker != null) {
            if (mStickerOperationListener != null) {
                mStickerOperationListener.onStickerDragFinished(mCurrentSticker);
            }
        }

        currentMode = ActionMode.NONE;
        lastClickTime = currentTime;
    }

    private void handleMoveEvent(MotionEvent event) {
        if (ActionMode.NONE.equals(currentMode) || ActionMode.CLICK.equals(currentMode)) {
            return;
        } else if (ActionMode.DRAG.equals(currentMode)) {
            if (mCurrentSticker != null) {
                moveMatrix.set(downMatrix);
                moveMatrix.postTranslate(event.getX() - downX, event.getY() - downY);
                mCurrentSticker.setMatrix(moveMatrix);
                if (constrained) {
                    constrainSticker(mCurrentSticker);
                }
            }
        } else if (ActionMode.ZOOM_WITH_TWO_FINGER.equals(currentMode)) {
            if (mCurrentSticker != null) {
                float newDistance = StickerUtil.calculateDistanceBetweenTwoFingers(event);
                float newRotation = StickerUtil.calculateRotationBetweenTwoFingers(event);

                moveMatrix.set(downMatrix);
                moveMatrix.postScale(newDistance / oldDistance, newDistance / oldDistance, mMiddlePoint.x,
                        mMiddlePoint.y);
                if (mCurrentSticker.isCanRotate()) {
                    moveMatrix.postRotate(newRotation - oldRotation, mMiddlePoint.x, mMiddlePoint.y);
                }
                mCurrentSticker.setMatrix(moveMatrix);
            }
        } else if (ActionMode.ICON.equals(currentMode)) {
            if (mCurrentSticker != null && mCurrentIcon != null) {
                mCurrentIcon.accept(event);
            }
        }
    }

    private void constrainSticker(Sticker sticker) {
        float moveX = 0;
        float moveY = 0;
        int width = getWidth();
        int height = getHeight();
        sticker.fillMappedCenterPointByStickerMatrix(currentCenterPoint);
        if (currentCenterPoint.x < 0) {
            moveX = -currentCenterPoint.x;
        }

        if (currentCenterPoint.x > width) {
            moveX = width - currentCenterPoint.x;
        }

        if (currentCenterPoint.y < 0) {
            moveY = -currentCenterPoint.y;
        }

        if (currentCenterPoint.y > height) {
            moveY = height - currentCenterPoint.y;
        }

        sticker.getMatrix().postTranslate(moveX, moveY);
    }

    private PointF calculateMiddlePointInTwoFingers(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            mMiddlePoint.set(0, 0);
            return mMiddlePoint;
        }
        float x = (event.getX(0) + event.getX(1)) / 2;
        float y = (event.getY(0) + event.getY(1)) / 2;
        mMiddlePoint.set(x, y);
        return mMiddlePoint;
    }

    private PointF calculateMiddlePointInSticker() {
        if (mCurrentSticker == null) {
            mMiddlePoint.set(0, 0);
            return mMiddlePoint;
        }
        mCurrentSticker.fillMappedCenterPointByStickerMatrix(mMiddlePoint);
        return mMiddlePoint;
    }

    /**
     * Set flag to constrain this sticker.
     *
     * @param constrained
     * @return
     */
    public StickerView setConstrained(boolean constrained) {
        this.constrained = constrained;
        postInvalidate();
        return this;
    }

    private void zoomAndRotateSticker(Sticker sticker, MotionEvent event) {
        if (sticker != null) {
            float newDistance = StickerUtil.calculateDistanceBetweenTwoPoint(mMiddlePoint.x, mMiddlePoint.y, event.getX(), event.getY());
            float newRotation = StickerUtil.calculateRotationBetweenTwoPoints(mMiddlePoint.x, mMiddlePoint.y, event.getX(), event.getY());

            moveMatrix.set(downMatrix);
            moveMatrix.postScale(newDistance / oldDistance, newDistance / oldDistance, mMiddlePoint.x,
                    mMiddlePoint.y);
            if (mCurrentSticker.isCanRotate()){
                moveMatrix.postRotate(newRotation - oldRotation, mMiddlePoint.x, mMiddlePoint.y);
            }
            mCurrentSticker.setMatrix(moveMatrix);
        }
    }

    public void flipCurrentSticker(int direction) {
        flip(mCurrentSticker, direction);
    }

    private void flip(Sticker sticker, int direction) {
        if (sticker != null) {
            sticker.fillOriginalCenterPointOfStickerWithoutStickerMatrix(mMiddlePoint);
            if ((direction & FLIP_HORIZONTALLY) > 0) {
                sticker.getMatrix().preScale(-1, 1, mMiddlePoint.x, mMiddlePoint.y);
                sticker.setFlippedHorizontally(!sticker.isFlippedHorizontally());
            }
            if ((direction & FLIP_VERTICALLY) > 0) {
                sticker.getMatrix().preScale(1, -1, mMiddlePoint.x, mMiddlePoint.y);
                sticker.setFlippedVertically(!sticker.isFlippedVertically());
            }

            if (mStickerOperationListener != null) {
                mStickerOperationListener.onStickerFlipped(sticker);
            }

            invalidate();
        }
    }

    public Bitmap getBitmapOfStickerView() throws OutOfMemoryError {
        // ignore border.
        mCurrentSticker = null;
        invalidate();
        // get bitmap.
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        return bitmap;
    }

    /**
     * Bring selected sticker to front of other stickers
     */
    public void bringStickerToFront() {
        if (mCurrentSticker != null) {
            mStickers.remove(mCurrentSticker);
            mStickers.add(mCurrentSticker);
            invalidate();
        }
    }

    @Override
    public void visit(ZoomStickerIcon icon, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                zoomAndRotateSticker(mCurrentSticker, event);
                break;
            case MotionEvent.ACTION_UP:
                OnStickerOperationListener listener = getStickerOperationListener();
                if (listener != null) {
                    listener.onStickerZoomFinished(mCurrentSticker);
                }
                break;
        }
    }

    @Override
    public void visit(TextSticker icon, MotionEvent event) {
        // Do nothing.
    }
}
