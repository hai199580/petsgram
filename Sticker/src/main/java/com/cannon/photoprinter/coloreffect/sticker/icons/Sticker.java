package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

import static java.lang.Math.round;


/**
 * This class is base class of all stickers.
 * It handle common logic.
 */
public abstract class Sticker implements StickerAcceptor {

    public static final int CENTER = 1;

    public static final int TOP = 1 << 1;

    public static final int LEFT = 1 << 2;

    public static final int RIGHT = 1 << 3;

    public static final int BOTTOM = 1 << 4;

    private final Matrix mMatrix = new Matrix();

    private Matrix mOriginalMatrix = new Matrix();

    public void setmOriginalMatrix(Matrix mOriginalMatrix) {
        this.mOriginalMatrix = mOriginalMatrix;
    }

    public Matrix getmOriginalMatrix() {
        return mOriginalMatrix;
    }

    private final StickerVisitor mVisitor;

    private final Drawable mDrawable;

    private final Rect mRealBounds;

    private boolean isFlippedHorizontally;

    private boolean isFlippedVertically;

    private boolean isCanRotate = true;

    private String stickerName;

    private String stickerPath;

    public String getStickerName() {
        return stickerName;
    }

    public void setStickerName(String stickerName) {
        this.stickerName = stickerName;
    }

    public String getStickerPath() {
        return stickerPath;
    }

    public void setStickerPath(String stickerPath) {
        this.stickerPath = stickerPath;
    }

    /**
     * Constructor.
     * @param visitor
     * @param drawable
     */
    public Sticker(StickerVisitor visitor, Drawable drawable) {
        if (visitor == null) {
            throw new IllegalArgumentException("The parameters is invalid");
        }
        mVisitor = visitor;
        mDrawable = drawable;
        mRealBounds = new Rect(0, 0, getWidth(), getHeight());
    }

    public Sticker(StickerVisitor visitor, Drawable drawable, int width, int height) {
        if (visitor == null) {
            throw new IllegalArgumentException("The parameters is invalid");
        }
        mVisitor = visitor;
        mDrawable = drawable;
        mRealBounds = new Rect(0, 0, width, height);
    }

    /**
     * Return {@link StickerVisitor}
     * @return
     */
    public StickerVisitor getVisitor() {
        return mVisitor;
    }

    /**
     * Get {@link Drawable}.
     * @return
     */
    public Drawable getDrawable() {
        return mDrawable;
    }

    /**
     * Check either flip sticker horizontally or NOT.
     * @return
     */
    public boolean isFlippedHorizontally() {
        return isFlippedHorizontally;
    }

    /**
     * Set to flip sticker horizontally or NOT.
     * @param flippedHorizontally
     */
    public void setFlippedHorizontally(boolean flippedHorizontally) {
        isFlippedHorizontally = flippedHorizontally;
    }

    /**
     * Check either flip sticker vertically or NOT.
     * @return
     */
    public boolean isFlippedVertically() {
        return isFlippedVertically;
    }

    /**
     * set to flip sticker vertically or NOT.
     * @param flippedVertically
     */
    public void setFlippedVertically(boolean flippedVertically) {
        isFlippedVertically = flippedVertically;
    }

    /**
     * Get matrix of this sticker which handle translate/rotate... sticker.
     * @return
     */
    public Matrix getMatrix() {
        return mMatrix;
    }

    /**
     * Return the matrix of this sticker.
     * @param matrix
     */
    public void setMatrix(Matrix matrix) {
        mMatrix.set(matrix);
    }

    public boolean isCanRotate() {
        return isCanRotate;
    }

    public void setCanRotate(boolean canRotate) {
        isCanRotate = canRotate;
    }

    /**
     * Fill bound points into points.
     * @param points
     */
    public void fillBoundPointsOfSticker(float[] points) {
        if (!isFlippedHorizontally) {
            if (!isFlippedVertically) {
                points[0] = 0f;
                points[1] = 0f;
                points[2] = getWidth();
                points[3] = 0f;
                points[4] = 0f;
                points[5] = getHeight();
                points[6] = getWidth();
                points[7] = getHeight();
            } else {
                points[0] = 0f;
                points[1] = getHeight();
                points[2] = getWidth();
                points[3] = getHeight();
                points[4] = 0f;
                points[5] = 0f;
                points[6] = getWidth();
                points[7] = 0f;
            }
        } else {
            if (!isFlippedVertically) {
                points[0] = getWidth();
                points[1] = 0f;
                points[2] = 0f;
                points[3] = 0f;
                points[4] = getWidth();
                points[5] = getHeight();
                points[6] = 0f;
                points[7] = getHeight();
            } else {
                points[0] = getWidth();
                points[1] = getHeight();
                points[2] = 0f;
                points[3] = getHeight();
                points[4] = getWidth();
                points[5] = 0f;
                points[6] = 0f;
                points[7] = 0f;
            }
        }
    }

    /**
     * Map points from source by Matrix.
     * @param dst
     * @param src
     */
    public void mapPointsByMatrix(float[] dst, float[] src) {
        mMatrix.mapPoints(dst, src);
    }

    /**
     * Fill original center point of this sticker which not use Matrix.
     * @param dst
     */
    public void fillOriginalCenterPointOfStickerWithoutStickerMatrix(PointF dst) {
        dst.set(getWidth() * 1f / 2, getHeight() * 1f / 2);
    }

    /**
     * Fill center point of this sticker after use Matrix.
     * @param dst
     */
    public void fillMappedCenterPointByStickerMatrix(PointF dst) {
        float[] src = new float[2];
        float[] mappedPoints = new float[2];
        fillOriginalCenterPointOfStickerWithoutStickerMatrix(dst);
        src[0] = dst.x;
        src[1] = dst.y;
        mapPointsByMatrix(mappedPoints, src);
        dst.set(mappedPoints[0], mappedPoints[1]);
    }

    private float getCurrentAngle() {
        return getMatrixAngle(mMatrix);
    }

    private float getMatrixAngle(Matrix matrix) {
        return (float) Math.toDegrees(-(Math.atan2(getMatrixValue(matrix, Matrix.MSKEW_X),
                getMatrixValue(matrix, Matrix.MSCALE_X))));
    }

    private float getMatrixValue(Matrix matrix, int valueIndex) {
        float[] matrixValues = new float[9];
        matrix.getValues(matrixValues);
        return matrixValues[valueIndex];
    }

    /**
     * Check either this sticker is in the area of these points.
     * @param point
     * @return
     */
    public boolean contains(float[] point) {
        Matrix tempMatrix = new Matrix();
        tempMatrix.setRotate(-getCurrentAngle());
        float[] boundPoints = new float[8];
        fillBoundPointsOfSticker(boundPoints);
        float[] mappedBounds = new float[8];
        mapPointsByMatrix(mappedBounds, boundPoints);
        float[] unrotatedWrapperCorner = new float[8];
        tempMatrix.mapPoints(unrotatedWrapperCorner, mappedBounds);
        float[] unrotatedPoint = new float[2];
        tempMatrix.mapPoints(unrotatedPoint, point);
        RectF trappedRect = new RectF();
        trapToRect(trappedRect, unrotatedWrapperCorner);
        return trappedRect.contains(unrotatedPoint[0], unrotatedPoint[1]);
    }

    private static void trapToRect(RectF r, float[] array) {
        r.set(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY,
                Float.NEGATIVE_INFINITY);
        for (int i = 1; i < array.length; i += 2) {
            float x = round(array[i - 1] * 10) / 10.f;
            float y = round(array[i] * 10) / 10.f;
            r.left = (x < r.left) ? x : r.left;
            r.top = (y < r.top) ? y : r.top;
            r.right = (x > r.right) ? x : r.right;
            r.bottom = (y > r.bottom) ? y : r.bottom;
        }
        r.sort();
    }

    /**
     * Draw sticker.
     * @param canvas
     */
    public void draw(Canvas canvas) {
        if (mDrawable == null) {
            return;
        }
        canvas.save();
        canvas.concat(getMatrix());
        mDrawable.setBounds(mRealBounds);
        mDrawable.draw(canvas);
        canvas.restore();
    }

    /**
     * Get the width of this sticker.
     * @return
     */
    public int getWidth() {
        if (mDrawable != null) {
            return mDrawable.getIntrinsicWidth();
        }
        return 0;
    }

    /**
     * Get the height of this sticker.
     * @return
     */
    public int getHeight() {
        if (mDrawable != null) {
            return mDrawable.getIntrinsicHeight();
        }
        return 0;
    }

    /**
     * Set alpha for this sticker.
     * @param alpha
     * @return
     */
    public abstract Sticker setAlpha(int alpha);

    public abstract boolean isShowBorder();

    public abstract boolean isShowIcon();

    public abstract boolean canFocus();
}
