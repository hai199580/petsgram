package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

/**
 * The Zoom icon sticker.
 */

public class ZoomStickerIcon extends StickerIcon {

    /**
     * Constructor.
     * @param drawable
     * @param gravity
     * @param visitor
     */
    public ZoomStickerIcon(Drawable drawable, int gravity, StickerVisitor visitor) {
        super(drawable, gravity, visitor);
    }

    @Override
    public void accept(MotionEvent event) {
        getVisitor().visit(this, event);
    }
}
