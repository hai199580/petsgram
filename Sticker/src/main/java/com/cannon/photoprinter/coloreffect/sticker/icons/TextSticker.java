package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Pair;
import android.view.MotionEvent;

import com.cannon.photoprinter.coloreffect.sticker.R;
import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

/**
 * The Text sticker which show text in front of the background.
 */
public class TextSticker extends Sticker {

    private static final int DEFAULT_MIN_TEXT_SIZE_BY_SP = 6;

    private static final int DEFAULT_MAX_TEXT_SIZE_BY_SP = 32;

    private static final String ELLIPSIS = "\u2026";

    private final Context mContext;

    private final Rect mRealBounds;

    private final Rect mTextRect;

    private final TextPaint mTextPaint;

    private StaticLayout mStaticLayout;

    private Layout.Alignment mAlignment;

    private String mText;

    private float mMaxTextSizeByPixel;

    private float mMinTextSizeByPixel;

    private float mLineSpacing = 1.0f;

    private float mLineSpacingExtra = 0.0f;

    /**
     * Constructor.
     * @param context
     * @param drawable
     * @param visitor
     */
    public TextSticker(Context context, Drawable drawable, StickerVisitor visitor) {
        super(visitor, drawable);
        mContext = context;
        mTextPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
        mRealBounds = new Rect(0, 0, getWidth(), getHeight());
        mTextRect = new Rect(0, 0, getWidth(), getHeight());
        mMinTextSizeByPixel = convertSpToPx(DEFAULT_MIN_TEXT_SIZE_BY_SP);
        mMaxTextSizeByPixel = convertSpToPx(DEFAULT_MAX_TEXT_SIZE_BY_SP);
        mAlignment = Layout.Alignment.ALIGN_CENTER;
        mTextPaint.setTextSize(mMaxTextSizeByPixel);
    }

    public TextSticker(Context context, StickerVisitor visitor) {
        this(context, context.getResources().getDrawable(R.drawable.sticker_transparent_background), visitor);
    }

    @Override
    public void draw(Canvas canvas) {
        Matrix matrix = getMatrix();
        drawDrawableBorder(matrix, canvas);
        drawText(matrix, canvas);
    }

    private void drawText(Matrix matrix, Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        if (mTextRect.width() == getWidth()) {
            int dy = getHeight() / 2 - mStaticLayout.getHeight() / 2;
            // center vertical
            canvas.translate(0, dy);
        } else {
            int dx = mTextRect.left;
            int dy = mTextRect.top + mTextRect.height() / 2 - mStaticLayout.getHeight() / 2;
            canvas.translate(dx, dy);
        }
        mStaticLayout.draw(canvas);
        canvas.restore();
    }

    private void drawDrawableBorder(Matrix matrix, Canvas canvas) {
        canvas.save();
        canvas.concat(matrix);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setBounds(mRealBounds);
            drawable.draw(canvas);
        }
        canvas.restore();
    }

    @Override
    public TextSticker setAlpha(int alpha) {
        mTextPaint.setAlpha(alpha);
        return this;
    }

    @Override
    public boolean isShowBorder() {
        return true;
    }

    @Override
    public boolean isShowIcon() {
        return true;
    }

    @Override
    public boolean canFocus() {
        return true;
    }

    /**
     * Set type face of text paint.
     * @param typeface
     * @return
     */
    public TextSticker setTypeface(Typeface typeface) {
        mTextPaint.setTypeface(typeface);
        return this;
    }

    /**
     * Set text color.
     * @param color
     * @return
     */
    public TextSticker setTextColor(int color) {
        mTextPaint.setColor(color);
        return this;
    }

    /**
     * Set alignment of this text.
     * @param alignment
     * @return
     */
    public TextSticker setTextAlign(Layout.Alignment alignment) {
        mAlignment = alignment;
        return this;
    }

    /**
     * Set max size of this text.
     * @param size
     * @return
     */
    public TextSticker setMaxTextSize(float size) {
        mTextPaint.setTextSize(convertSpToPx(size));
        mMaxTextSizeByPixel = mTextPaint.getTextSize();
        return this;
    }

    /**
     * Set min size of this text.
     *
     * @param minTextSizeBySP the minimum size to use for text in this view in scaled pixels.
     */
    public TextSticker setMinTextSize(float minTextSizeBySP) {
        mMinTextSizeByPixel = convertSpToPx(minTextSizeBySP);
        return this;
    }

    /**
     * Set line spacing of this text.
     * @param add
     * @param multiplier
     * @return
     */
    public TextSticker setLineSpacing(float add, float multiplier) {
        mLineSpacing = multiplier;
        mLineSpacingExtra = add;
        return this;
    }

    /**
     * Set text for this sticker.
     * @param text
     * @return
     */
    public TextSticker setText(String text) {
        mText = text;
        return this;
    }

    /**
     * Resize this view's text size with respect to its width and height
     */
    public TextSticker resizeText() {
        int textWidth = mTextRect.width();
        int textHeight = mTextRect.height();

        if (mText == null || mText.length() <= 0 || textHeight <= 0 || textWidth <= 0 || mMaxTextSizeByPixel <= 0) {
            return this;
        }

        Pair<Float, Integer> targetSize = calculateTargetTextSize(textWidth, textHeight);
        float targetTextSizePixels = targetSize.first;
        int targetTextHeightPixels = targetSize.second;

        if (targetTextSizePixels == mMinTextSizeByPixel
                && targetTextHeightPixels > textHeight) {
            measureTextStringAndUpdateText(targetTextSizePixels, textWidth, textHeight);
        }
        mTextPaint.setTextSize(targetTextSizePixels);
        mStaticLayout =
                new StaticLayout(mText, mTextPaint, mTextRect.width(), mAlignment, mLineSpacing,
                        mLineSpacingExtra, true);
        return this;
    }

    /**
     * Measure the text to fit the its available area.
     * @param targetTextSizePixels
     * @param textWidth
     * @param textHeight
     */
    private void measureTextStringAndUpdateText(float targetTextSizePixels, int textWidth, int textHeight) {
        TextPaint textPaintCopy = new TextPaint(mTextPaint);
        textPaintCopy.setTextSize(targetTextSizePixels);

        StaticLayout staticLayout =
                new StaticLayout(mText, textPaintCopy, textWidth, Layout.Alignment.ALIGN_NORMAL,
                        mLineSpacing, mLineSpacingExtra, false);

        if (staticLayout.getLineCount() > 0) {
            int lastLine = staticLayout.getLineForVertical(textHeight) - 1;

            if (lastLine >= 0) {
                int startOffset = staticLayout.getLineStart(lastLine);
                int endOffset = staticLayout.getLineEnd(lastLine);
                float lineWidthPixels = staticLayout.getLineWidth(lastLine);
                float ellipseWidth = textPaintCopy.measureText(ELLIPSIS);

                // Trim characters off until we have enough room to draw the ellipsis
                while (textWidth < lineWidthPixels + ellipseWidth) {
                    endOffset--;
                    lineWidthPixels =
                            textPaintCopy.measureText(mText.subSequence(startOffset, endOffset + 1).toString());
                }

                setText(mText.subSequence(0, endOffset) + ELLIPSIS);
            }
        }
    }

    /**
     * Calculate target size of this text.
     * @param textWidth
     * @param textHeight
     * @return
     */
    private Pair<Float, Integer> calculateTargetTextSize(int textWidth, int textHeight) {
        float targetTextSizePixels = mMaxTextSizeByPixel;
        int targetTextHeightPixels = calculateTextSizeHeight(mText, textWidth, targetTextSizePixels);

        while (targetTextHeightPixels > textHeight
                && targetTextSizePixels > mMinTextSizeByPixel) {
            targetTextSizePixels = Math.max(targetTextSizePixels - 2, mMinTextSizeByPixel);

            targetTextHeightPixels =
                    calculateTextSizeHeight(mText, textWidth, targetTextSizePixels);
        }
        return Pair.create(targetTextSizePixels, targetTextHeightPixels);
    }

    private int calculateTextSizeHeight(CharSequence source, int textWidth, float textSize) {
        mTextPaint.setTextSize(textSize);
        StaticLayout staticLayout =
                new StaticLayout(source, mTextPaint, textWidth, Layout.Alignment.ALIGN_NORMAL,
                        mLineSpacing, mLineSpacingExtra, true);
        return staticLayout.getHeight();
    }

    private float convertSpToPx(float scaledPixels) {
        return scaledPixels * mContext.getResources().getDisplayMetrics().scaledDensity;
    }

    @Override
    public void accept(MotionEvent event) {
        getVisitor().visit(this, event);
    }
}
