package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.view.MotionEvent;

/**
 * This interface define api to access sticker from other places.
 */

public interface StickerAcceptor {

    /**
     * Request to accept Sticker.
     * Then, Sticker will tell Visitor object to continue handle.
     * @param event
     */
    void accept(MotionEvent event);
}
