package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;

import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

/**
 * The frame sticker.
 */

public class FrameSticker extends Sticker {

    /**
     * Constructor.
     * @param visitor
     * @param drawable
     * @param width
     * @param height
     */
    public FrameSticker(StickerVisitor visitor, Drawable drawable, int width, int height) {
        super(visitor, drawable, width, height);
    }

    @Override
    public void accept(MotionEvent event) {
        // Do nothing.
    }

    @Override
    public Sticker setAlpha(int alpha) {
        return null;
    }

    @Override
    public boolean isShowBorder() {
        return false;
    }

    @Override
    public boolean isShowIcon() {
        return false;
    }

    @Override
    public boolean canFocus() {
        return false;
    }
}
