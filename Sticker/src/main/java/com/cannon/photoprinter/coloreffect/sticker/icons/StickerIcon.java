package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

/**
 * This base class is used to define icon of sticker.
 */
public abstract class StickerIcon extends Sticker implements StickerAcceptor {

    public static final float DEFAULT_ICON_RADIUS = 15f;

    public static final int LEFT_TOP = 0;

    public static final int RIGHT_TOP = 1;

    public static final int LEFT_BOTTOM = 2;

    public static final int RIGHT_BOTTOM = 3;

    private final int mGravity;

    private float mIconRadius = DEFAULT_ICON_RADIUS;

    private float mXPosition;

    private float mYPosition;

    protected final Paint mCircleIconPaint = new Paint();

    /**
     * Constructor.
     * @param drawable
     * @param gravity
     * @param visitor
     */
    public StickerIcon(Drawable drawable, int gravity, StickerVisitor visitor) {
        super(visitor, drawable);
        mGravity = gravity;
        mCircleIconPaint.setColor(Color.WHITE);
    }

    @Override
    public void draw(Canvas canvas) {
        drawCircle(canvas, mCircleIconPaint);
        super.draw(canvas);
    }

    @Override
    public Sticker setAlpha(int alpha) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            getDrawable().setAlpha(alpha);
        }
        return this;
    }

    @Override
    public boolean canFocus() {
        return true;
    }

    private void drawCircle(Canvas canvas, Paint paint) {
        canvas.drawCircle(mXPosition, mYPosition, mIconRadius, paint);
    }

    /**
     * Get the position X of this icon sticker.
     * @return
     */
    public float getXPosition() {
        return mXPosition;
    }

    /**
     * Set the position X of this icon sticker.
     * @param xPosition
     */
    public void setXPosition(float xPosition) {
        mXPosition = xPosition;
    }

    /**
     * Get the position Y of this icon sticker.
     * @return
     */
    public float getYPosition() {
        return mYPosition;
    }

    /**
     * Set the position Y of this icon sticker.
     * @param yPosition
     */
    public void setYPosition(float yPosition) {
        mYPosition = yPosition;
    }

    /**
     * Get radius of this icon.
     * @return
     */
    public float getIconRadius() {
        return mIconRadius;
    }

    /**
     * Get gravity of this icon.
     * @return
     */
    public int getGravity() {
        return mGravity;
    }

    @Override
    public boolean isShowBorder() {
        return true;
    }

    @Override
    public boolean isShowIcon() {
        return true;
    }
}
