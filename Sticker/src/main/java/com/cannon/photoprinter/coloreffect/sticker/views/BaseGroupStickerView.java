package com.cannon.photoprinter.coloreffect.sticker.views;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;

import com.cannon.photoprinter.coloreffect.sticker.icons.Sticker;
import com.cannon.photoprinter.coloreffect.sticker.icons.StickerIcon;
import com.cannon.photoprinter.coloreffect.sticker.icons.ZoomStickerIcon;
import com.cannon.photoprinter.coloreffect.sticker.utils.StickerUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is base class which can handle some other sticker.
 * Other sticker maybe icons: Delete, Zoom, Flip...
 */

public abstract class BaseGroupStickerView extends BaseStickerView {

    public final List<Sticker> mStickers = new ArrayList<>();

    protected final List<StickerIcon> mIcons = new ArrayList<>(4);

    protected Sticker mCurrentSticker;

    protected StickerIcon mCurrentIcon;

    /**
     * Constructor.
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public BaseGroupStickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDefaultIcons();
    }

    /**
     * Init sticker icons.
     */
    private void initDefaultIcons() {
        StickerIcon deleteIcon = new ZoomStickerIcon(
                null, StickerIcon.LEFT_TOP, this);
        StickerIcon zoomIcon = new ZoomStickerIcon(
                null,
                StickerIcon.RIGHT_BOTTOM, this);
        StickerIcon flipIcon = new ZoomStickerIcon(
                null,
                StickerIcon.RIGHT_TOP, this);
        StickerIcon heartIcon = new ZoomStickerIcon(
                null,
                StickerIcon.LEFT_BOTTOM, this);

        mIcons.clear();
        mIcons.add(deleteIcon);
        mIcons.add(zoomIcon);
        mIcons.add(flipIcon);
        mIcons.add(heartIcon);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        handleDrawingSticker(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);
        for (int i = 0; i < mStickers.size(); i++) {
            Sticker sticker = mStickers.get(i);
            if (sticker != null) {
                transformSticker(sticker);
            }
        }
    }

    /**
     * Find a icon in clicked area.
     * @param downX
     * @param downY
     * @return
     */
    protected StickerIcon findCurrentIconTouched(float downX, float downY) {
        for (StickerIcon icon : mIcons) {
            if (icon.canFocus() && isIconTouched(icon, downX, downY))
                return icon;
        }

        return null;
    }

    /**
     * Find a sticker in touched area.
     * @param downX
     * @param downY
     * @return
     */
    protected Sticker findHandlingSticker(float downX, float downY) {
        for (Sticker sticker : mStickers) {
            if (sticker.canFocus() && isInStickerArea(sticker, downX, downY)) {
                return sticker;
            }
        }
        return null;
    }

    private void handleDrawingSticker(Canvas canvas) {
        drawStickers(canvas);
        drawBorders(canvas);
    }


    private void drawBorders(Canvas canvas) {
        if (mCurrentSticker != null && !isIsLockIcon()) {
            boolean isShowBorder = mCurrentSticker.isShowBorder();
            boolean isShowIcon = mCurrentSticker.isShowIcon();
            if (!isShowBorder && !isShowIcon) {
                return;
            }
            float[] bitmapPoints = new float[8];
            fillStickerPoints(mCurrentSticker, bitmapPoints);

            float x1 = bitmapPoints[0];
            float y1 = bitmapPoints[1];
            float x2 = bitmapPoints[2];
            float y2 = bitmapPoints[3];
            float x3 = bitmapPoints[4];
            float y3 = bitmapPoints[5];
            float x4 = bitmapPoints[6];
            float y4 = bitmapPoints[7];

            if (isShowBorder) {
                canvas.drawLine(x1, y1, x2, y2, mBorderPaint);
                canvas.drawLine(x1, y1, x3, y3, mBorderPaint);
                canvas.drawLine(x2, y2, x4, y4, mBorderPaint);
                canvas.drawLine(x4, y4, x3, y3, mBorderPaint);
            }

            //draw mIcons
            if (isShowIcon) {
                float rotation = StickerUtil.calculateRotationBetweenTwoPoints(x4, y4, x3, y3);
                for (int i = 0; i < mIcons.size(); i++) {
                    StickerIcon icon = mIcons.get(i);
                    switch (icon.getGravity()) {
                        case StickerIcon.LEFT_TOP:
                            configIconMatrix(icon, x1, y1, rotation);
                            break;
                        case StickerIcon.RIGHT_TOP:
                            configIconMatrix(icon, x2, y2, rotation);
                            break;
                        case StickerIcon.LEFT_BOTTOM:
                            configIconMatrix(icon, x3, y3, rotation);
                            break;
                        case StickerIcon.RIGHT_BOTTOM:
                            configIconMatrix(icon, x4, y4, rotation);
                            break;
                    }
                    icon.draw(canvas);
                }
            }
        }
    }

    private void fillStickerPoints(Sticker sticker, float[] dst) {
        if (sticker == null) {
            Arrays.fill(dst, 0);
            return;
        }
        float[] bounds = new float[8];
        sticker.fillBoundPointsOfSticker(bounds);
        sticker.mapPointsByMatrix(dst, bounds);
    }

    private void drawStickers(Canvas canvas) {
        for (int i = 0; i < mStickers.size(); i++) {
            Sticker sticker = mStickers.get(i);
            if (sticker != null) {
                sticker.draw(canvas);
            }
        }
    }

    private void configIconMatrix(StickerIcon icon, float x, float y, float rotation) {
        icon.setXPosition(x);
        icon.setYPosition(y);
        icon.getMatrix().reset();

        icon.getMatrix().postRotate(rotation, icon.getWidth() / 2, icon.getHeight() / 2);
        icon.getMatrix().postTranslate(x - icon.getWidth() / 2, y - icon.getHeight() / 2);
    }

    /**
     * Add a sticker to this view.
     * @param sticker
     */
    public void addSticker(Sticker sticker) {
        addSticker(sticker, Sticker.CENTER);
    }

    /**
     * Add a sticker to this view.
     * @param sticker
     * @param position
     */
    public void addSticker(final Sticker sticker, final int position) {
        if (ViewCompat.isLaidOut(this)) {
            addStickerImmediately(sticker, position);
        } else {
            post(new Runnable() {
                @Override
                public void run() {
                    addStickerImmediately(sticker, position);
                }
            });
        }
    }

    public void addFrame(Sticker frameSticker) {
        mStickers.add(frameSticker);
        invalidate();
    }

    private void addStickerImmediately(Sticker sticker, int position) {
        setStickerPosition(sticker, position);


        float scaleFactor, widthScaleFactor, heightScaleFactor;

        widthScaleFactor = (float) getWidth() / sticker.getWidth();
        heightScaleFactor = (float) getHeight() / sticker.getHeight();
        scaleFactor = widthScaleFactor > heightScaleFactor ? heightScaleFactor : widthScaleFactor;

        sticker.getMatrix()
                .postScale(scaleFactor / 2, scaleFactor / 2, getWidth() / 2, getHeight() / 2);

        mCurrentSticker = sticker;
        mStickers.add(sticker);
        if (mStickerOperationListener != null) {
            mStickerOperationListener.onStickerAdded(sticker);
        }
        invalidate();
    }

    private void setStickerPosition(Sticker sticker, int position) {
        float width = getWidth();
        float height = getHeight();
        float offsetX = width - sticker.getWidth();
        float offsetY = height - sticker.getHeight();
        if ((position & Sticker.TOP) > 0) {
            offsetY /= 4f;
        } else if ((position & Sticker.BOTTOM) > 0) {
            offsetY *= 3f / 4f;
        } else {
            offsetY /= 2f;
        }
        if ((position & Sticker.LEFT) > 0) {
            offsetX /= 4f;
        } else if ((position & Sticker.RIGHT) > 0) {
            offsetX *= 3f / 4f;
        } else {
            offsetX /= 2f;
        }

        sticker.setmOriginalMatrix(sticker.getMatrix());
        sticker.getMatrix().postTranslate(offsetX, offsetY);
    }

    /**
     * Replace a sticker to this view.
     * @param sticker
     * @return
     */
    public boolean replace(Sticker sticker) {
        return replace(sticker, true);
    }

    private boolean replace(Sticker sticker, boolean needStayState) {
        if (mCurrentSticker != null && sticker != null) {
            float width = getWidth();
            float height = getHeight();
            if (needStayState) {
                sticker.setMatrix(mCurrentSticker.getMatrix());
                sticker.setFlippedVertically(mCurrentSticker.isFlippedVertically());
                sticker.setFlippedHorizontally(mCurrentSticker.isFlippedHorizontally());
            } else {
                mCurrentSticker.getMatrix().reset();
                // reset scale, angle, and put it in center
                float offsetX = (width - mCurrentSticker.getWidth()) / 2f;
                float offsetY = (height - mCurrentSticker.getHeight()) / 2f;
                sticker.getMatrix().postTranslate(offsetX, offsetY);

                float scaleFactor;
                if (width < height) {
                    scaleFactor = width / mCurrentSticker.getWidth();
                } else {
                    scaleFactor = height / mCurrentSticker.getHeight();
                }
                sticker.getMatrix().postScale(scaleFactor / 2f, scaleFactor / 2f, width / 2f, height / 2f);
            }
            int index = mStickers.indexOf(mCurrentSticker);
            mStickers.set(index, sticker);
            mCurrentSticker = sticker;

            invalidate();
            return true;
        } else {
            return false;
        }
    }

    private boolean remove(Sticker sticker) {
        if (mStickers.contains(sticker)) {
            mStickers.remove(sticker);
            if (mStickerOperationListener != null) {
                mStickerOperationListener.onStickerDeleted(sticker);
            }
            if (mCurrentSticker == sticker) {
                mCurrentSticker = null;
            }
            invalidate();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove sticker which focusing on screen.
     * @return
     */
    public boolean deleteCurrentSticker() {
        return remove(mCurrentSticker);
    }

    public Sticker getCurrentSticker(){
        return mCurrentSticker;
    }

    /**
     * Remove all sticker.
     */
    public void removeAllStickers() {
        mStickers.clear();
        if (mCurrentSticker != null) {
            mCurrentSticker = null;
        }
        invalidate();
    }
}
