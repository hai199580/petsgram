package com.cannon.photoprinter.coloreffect.sticker.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import com.cannon.photoprinter.coloreffect.sticker.R;
import com.cannon.photoprinter.coloreffect.sticker.icons.Sticker;
import com.cannon.photoprinter.coloreffect.sticker.icons.StickerIcon;

/**
 * This base class of sticker view.
 * It will handle common logic.
 */

public abstract class BaseStickerView extends FrameLayout implements StickerVisitor {

    /**
     * The listener of sticker view.
     */
    public interface OnStickerOperationListener {
        void onStickerAdded(Sticker sticker);

        void onStickerClicked(Sticker sticker);

        void onStickerDeleted(Sticker sticker);

        void onStickerDragFinished(Sticker sticker);

        void onStickerZoomFinished(Sticker sticker);

        void onStickerFlipped(Sticker sticker);

        void onStickerDoubleTapped(Sticker sticker);
    }

    private static final int DEFAULT_STRIKE_WIDTH = 3;

    private boolean mIsLockIcon;

    protected final boolean mBringCurrentStickerToFront;

    protected final Paint mBorderPaint = new Paint();

    private final Matrix mSizeMatrix = new Matrix();

    protected OnStickerOperationListener mStickerOperationListener;

    /**
     * Constructor.
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public BaseStickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = null;
        try {
            a = context.obtainStyledAttributes(attrs, R.styleable.StickerView);

            mBringCurrentStickerToFront =
                    a.getBoolean(R.styleable.StickerView_bringToFrontCurrentSticker, false);

            mBorderPaint.setAntiAlias(true);
            mBorderPaint.setColor(Color.WHITE);
            mBorderPaint.setStrokeWidth(DEFAULT_STRIKE_WIDTH);
        } finally {
            if (a != null) {
                a.recycle();
            }
        }
    }

    /**
     * Check either sticker locked icons or NOT.
     * @return
     */
    public boolean isIsLockIcon() {
        return mIsLockIcon;
    }

    /**
     * Set flag to specify sticker locked icons or NOT.
     * @param isLockIcon
     */
    public void setIsLockIcon(boolean isLockIcon) {
        mIsLockIcon = isLockIcon;
        invalidate();
    }

    /**
     * Sticker's drawable will be too bigger or smaller
     * This method is to transform it to fit
     * step 1：let the center of the sticker image is coincident with the center of the View.
     * step 2：Calculate the zoom and zoom
     **/
    protected void transformSticker(Sticker sticker) {
        if (sticker == null) {
            Log.e("Test", "transformSticker: the bitmapSticker is null or the bitmapSticker bitmap is null");
            return;
        }

        mSizeMatrix.reset();

        float width = getWidth();
        float height = getHeight();
        float stickerWidth = sticker.getWidth();
        float stickerHeight = sticker.getHeight();
        //step 1
        float offsetX = (width - stickerWidth) / 2;
        float offsetY = (height - stickerHeight) / 2;

        mSizeMatrix.postTranslate(offsetX, offsetY);

        //step 2
        float scaleFactor;
        if (width < height) {
            scaleFactor = width / stickerWidth;
        } else {
            scaleFactor = height / stickerHeight;
        }

        mSizeMatrix.postScale(scaleFactor / 2f, scaleFactor / 2f, width / 2f, height / 2f);

        sticker.getMatrix().reset();
        sticker.setMatrix(mSizeMatrix);

        invalidate();
    }

    /**
     * Check either this sticker is in area which clicked.
     * @param sticker
     * @param downX
     * @param downY
     * @return
     */
    protected boolean isInStickerArea(Sticker sticker, float downX, float downY) {
        float[] currentPosition = new float[2];
        currentPosition[0] = downX;
        currentPosition[1] = downY;
        return sticker.contains(currentPosition);
    }

    /**
     * Check either this icon is in area which touched.
     * @param icon
     * @param downX
     * @param downY
     * @return
     */
    protected boolean isIconTouched(StickerIcon icon, float downX, float downY) {
        float x = icon.getXPosition() - downX;
        float y = icon.getYPosition() - downY;
        float distance_pow_2 = x * x + y * y;
        if (distance_pow_2 <= Math.pow(icon.getIconRadius() + icon.getIconRadius(), 2)) {
            return true;
        }
        return false;
    }

    /**
     * Set listener of this sticker.
     * @param mStickerOperationListener
     */
    public void setStickerOperationListener(
            OnStickerOperationListener mStickerOperationListener) {
        this.mStickerOperationListener = mStickerOperationListener;
    }

    /**
     * Get listener of this sticker.
     * @return
     */
    public OnStickerOperationListener getStickerOperationListener() {
        return mStickerOperationListener;
    }
}
