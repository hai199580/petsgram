package com.cannon.photoprinter.coloreffect.sticker.icons;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;

import com.cannon.photoprinter.coloreffect.sticker.views.StickerVisitor;

/**
 * The Image icon sticker.
 */
public class ImageSticker extends Sticker {
    /**
     * Constructor.
     * @param drawable
     * @param visitor
     */
    public ImageSticker(Drawable drawable, StickerVisitor visitor) {
        super(visitor, drawable);
    }

    @Override
    public ImageSticker setAlpha(int alpha) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            getDrawable().setAlpha(alpha);
        }
        return this;
    }

    @Override
    public boolean isShowBorder() {
        return true;
    }

    @Override
    public boolean isShowIcon() {
        return true;
    }

    @Override
    public boolean canFocus() {
        return true;
    }

    @Override
    public void accept(MotionEvent event) {
        // Do nothing.
    }
}
