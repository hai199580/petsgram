package com.cannon.photoprinter.coloreffect.sticker.utils;

import android.view.MotionEvent;

/**
 * The util class is used to calculate rotation, distance...
 */

public class StickerUtil {

    /**
     * Calculate rotation in line with two fingers.
     *
     * @param event
     * @return
     */
    public static float calculateRotationBetweenTwoFingers(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            return 0f;
        }
        return calculateRotationBetweenTwoPoints(event.getX(0), event.getY(0),
                event.getX(1), event.getY(1));
    }

    /**
     * Calculate the rotation from two points.
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static float calculateRotationBetweenTwoPoints(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;
        double radians = Math.atan2(y, x);
        return (float) Math.toDegrees(radians);
    }

    /**
     * Calculate Distance in two fingers.
     *
     * @param event
     * @return
     */
    public static float calculateDistanceBetweenTwoFingers(MotionEvent event) {
        if (event == null || event.getPointerCount() < 2) {
            return 0f;
        }
        return calculateDistanceBetweenTwoPoint(event.getX(0), event.getY(0),
                event.getX(1), event.getY(1));
    }

    /**
     * Calculate distance between two points.
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static float calculateDistanceBetweenTwoPoint(float x1, float y1, float x2, float y2) {
        double x = x1 - x2;
        double y = y1 - y2;

        return (float) Math.sqrt(x * x + y * y);
    }

}
