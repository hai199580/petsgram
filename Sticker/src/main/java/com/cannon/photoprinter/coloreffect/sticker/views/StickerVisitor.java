package com.cannon.photoprinter.coloreffect.sticker.views;

import android.view.MotionEvent;

import com.cannon.photoprinter.coloreffect.sticker.icons.TextSticker;
import com.cannon.photoprinter.coloreffect.sticker.icons.ZoomStickerIcon;

/**
 * This interface define api that Sticker request sticker view to handle..
 */

public interface StickerVisitor {

    /**
     * Request {@link StickerVisitor} to continue handle {@link ZoomStickerIcon} with {@link MotionEvent}.
     * @param icon
     * @param event
     */
    void visit(ZoomStickerIcon icon, MotionEvent event);

    /**
     * Request {@link StickerVisitor} to continue handle {@link TextSticker} with {@link MotionEvent}.
     * @param icon
     * @param event
     */
    void visit(TextSticker icon, MotionEvent event);
}
